package com.cerner.vicarius.module;

/**
 * Identifies a class as one that can supply job parameters dynamically.
 * 
 * @author MS025633
 *
 */
public @interface JobParameterSet {

}
