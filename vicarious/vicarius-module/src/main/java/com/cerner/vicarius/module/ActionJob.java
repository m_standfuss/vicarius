package com.cerner.vicarius.module;

/**
 * An abstract job implementation that preforms an action.
 *
 * @author MS025633
 */
public abstract class ActionJob extends JobSpec {
	
	/**
	 * The method that runs the action.
	 */
	public abstract void run();
}
