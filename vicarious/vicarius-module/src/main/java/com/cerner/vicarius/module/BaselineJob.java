package com.cerner.vicarius.module;

/**
 * A job that needs a baseline run prior to any other runs. Useful for if conditionals need to first run to get a
 * baseline to compare future runs against. </br>
 * </br>
 * An example of this is if we are checking for a change in a count (maybe an
 * issue count for a jira queue, or number of FindBug issues while scanning a code base) the first time that conditional
 * job runs it will have nothing to compare against. Instead if that job implements {@link BaselineJob} its
 * {@link BaselineJob#runBaseline()} method will be called first before the job is invoked allowing for it to save a
 * baseline count to compare against in the future.
 * 
 * @author MS025633
 */
public interface BaselineJob {
	
	/**
	 * Runs this jobs baseline, will run prior to any other invocations of this job.
	 */
	public void runBaseline();
}
