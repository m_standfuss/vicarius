package com.cerner.vicarius.module;

/**
 * An abstract job implementation that preforms a conditional check.
 *
 * @author MS025633
 */
public abstract class ConditionalJob extends JobSpec {
	
	/**
	 * Runs the conditional check.
	 *
	 * @return Whether the conditional passed or not.
	 */
	public abstract boolean run();
}
