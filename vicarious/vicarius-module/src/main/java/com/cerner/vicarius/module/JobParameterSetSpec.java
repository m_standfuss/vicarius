package com.cerner.vicarius.module;

import java.util.List;

/**
 * A class that can supply job parameters on demand. Useful when there is a
 * specific set of parameters that valid for a job's property. Rather then
 * allowing the end user to input any String value you can supply the valid
 * values via a job parameter set for them to choose from.
 * 
 * @author MS025633
 *
 */
public interface JobParameterSetSpec {

	public List<DynamicJobParameterValue> getParameterSet();
}
