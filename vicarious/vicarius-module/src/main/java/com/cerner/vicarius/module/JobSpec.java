package com.cerner.vicarius.module;

import java.util.Date;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * An abstract job specification.
 * 
 * @author MS025633
 */
public abstract class JobSpec {
	
	private Consumer<String> saveStateConsumer;
	private Supplier<String> saveStateSupplier;
	private Supplier<Date> lastCompletedSupplier;
	
	/**
	 * Sets the consumer of the save state for this rule's job.
	 *
	 * @param consumer
	 *            The consumer.
	 */
	public void setSaveStateConsumer(Consumer<String> consumer) {
		saveStateConsumer = consumer;
	}
	
	/**
	 * Sets the supplier of the current save state for this rule's job.
	 *
	 * @param supplier
	 *            The supplier.
	 */
	public void setSaveStateSupplier(Supplier<String> supplier) {
		saveStateSupplier = supplier;
	}
	
	/**
	 * Sets the supplier of the last completed date and time for this rule.
	 *
	 * @param supplier
	 *            The supplier.
	 */
	public void setLastCompletedSupplier(Supplier<Date> supplier) {
		lastCompletedSupplier = supplier;
	}
	
	/**
	 * Saves the state of the job on completion, if the entire rule is not completed will be discarded. Can be retrieved
	 * in later invocations of the same job by {@link #getSaveState()}.
	 *
	 * @param state
	 *            The state to save.
	 */
	protected void saveState(String state) {
		if (saveStateConsumer != null) {
			saveStateConsumer.accept(state);
		}
	}
	
	/**
	 * Gets the last saved state of the job.
	 *
	 * @return The saved state.
	 */
	protected String getSaveState() {
		if (saveStateSupplier == null) {
			return null;
		}
		return saveStateSupplier.get();
	}
	
	/**
	 * Gets the last time that job was completed successfully.
	 *
	 * @return The last time the job was completed.
	 */
	protected Date getLastCompleted() {
		if (lastCompletedSupplier == null) {
			return null;
		}
		return lastCompletedSupplier.get();
	}
	
}