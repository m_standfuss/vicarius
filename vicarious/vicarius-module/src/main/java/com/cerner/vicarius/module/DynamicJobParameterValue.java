package com.cerner.vicarius.module;

/**
 * A dynamic job parameter value. Has a display and an actual value, by default
 * the display uses the {@link #toString()}, but can be overridden.
 * 
 * @author MS025633
 *
 */
public interface DynamicJobParameterValue {

	/**
	 * Creates a new dynamic job parameter value.
	 * 
	 * @param display
	 *            The display of the value.
	 * @param value
	 *            The value.
	 * @return The job parameter.
	 */
	public static DynamicJobParameterValue makeParameter(String display, Object value) {
		return new DynamicJobParameterValue() {

			@Override
			public String getDisplay() {
				return display;
			}

			@Override
			public Object getValue() {
				return value;
			}
		};
	}

	/**
	 * Gets the display of the parameter, should be front end user friendly.
	 * 
	 * @return The display.
	 */
	public default String getDisplay() {
		return toString();
	}

	/**
	 * The actual value of the parameter. The value should be able to be parsed
	 * into a primitive value.
	 * 
	 * @return the value.
	 */
	public Object getValue();
}
