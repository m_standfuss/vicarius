package com.cerner.vicarius.module;

/**
 * An exception to be thrown when a {@link ModuleSpec#startup()} runs into
 * issues and is not able to successfully startup.
 * 
 * @author MS025633
 *
 */
public class StartupException extends RuntimeException {

	private static final long serialVersionUID = -1562879375058170684L;

	public StartupException() {
		super();
	}

	public StartupException(String message) {
		super(message);
	}

	public StartupException(String message, Throwable t) {
		super(message, t);
	}
}
