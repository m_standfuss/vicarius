package com.cerner.vicarius.module;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * An annotation to help facilitate the identification of potential modules.
 * 
 * @author MS025633
 */
@Retention(RetentionPolicy.CLASS)
@Target({ ElementType.TYPE })
public @interface Module {

	/**
	 * The name of the module.
	 * 
	 * @return The name.
	 */
	String name();
}
