package com.cerner.vicarius.module;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * An annotation to help facilitate the identification of potential jobs.
 * 
 * @author MS025633
 */
@Retention(RetentionPolicy.CLASS)
@Target({ ElementType.TYPE })
public @interface Job {

	/**
	 * The name of the job.
	 * 
	 * @return The name.
	 */
	String name();

	/**
	 * Whether or not this is a conditional job.
	 * 
	 * @return If a conditional
	 */
	boolean conditional();
}
