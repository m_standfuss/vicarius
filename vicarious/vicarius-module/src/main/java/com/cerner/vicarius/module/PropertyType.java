package com.cerner.vicarius.module;

/**
 * The available property types.
 *
 * @author MS025633
 */
public enum PropertyType {
	INPUT, OUTPUT;
}