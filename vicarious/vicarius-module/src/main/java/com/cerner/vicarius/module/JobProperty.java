package com.cerner.vicarius.module;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * A property annotation to help facilitate the properties that can be set on a
 * job.
 *
 * @author MS025633
 */
@Retention(RetentionPolicy.CLASS)
@Target({ ElementType.METHOD })
public @interface JobProperty {

	/**
	 * If this is a required parameter for the job to run.
	 *
	 * @return If required to run.
	 */
	boolean required() default false;

	/**
	 * The property type in or out. For input types the property will be
	 * specified prior to running the job, for outputs the property will be
	 * queried post run of the job.
	 *
	 * @return Whether an input or output property.
	 */
	PropertyType type();

	/**
	 * The name of the property. This should be unique amongst all properties
	 * for a job.
	 *
	 * @return
	 */
	String name();
}
