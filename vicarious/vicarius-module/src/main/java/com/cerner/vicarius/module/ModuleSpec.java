package com.cerner.vicarius.module;

import java.util.function.Function;

/**
 * An abstract module specification.
 *
 * @author MS025633
 */
public abstract class ModuleSpec {

	private String display;
	private Function<String, String> propertySupplier;

	/**
	 * Starts up the module, needs to be implemented by each module to
	 * initialize any dependencies of jobs in the module.
	 */
	public abstract void startup();

	/**
	 * Shuts down the module, needs to be implemented by each module to shutdown
	 * any dependencies used while active.
	 */
	public abstract void shutdown();

	/**
	 * Gets the display of the module.
	 *
	 * @return The display.
	 */
	public String getDisplay() {
		return display;
	}

	/**
	 * Sets the display of the module.
	 *
	 * @param display
	 *            The display of the module.
	 */
	public void setDisplay(String display) {
		this.display = display;
	}

	/**
	 * Sets the property supplier for obtaining properties.
	 *
	 * @param function
	 *            The supplier function.
	 */
	public void setPropertySupplier(Function<String, String> function) {
		propertySupplier = function;
	}

	/**
	 * Gets a property for this module. If not property is defined then the
	 * default supplied will be returned.
	 *
	 * @param propertyName
	 *            The property name looking for.
	 * @param defaulted
	 *            The default value to use if not found.
	 * @return The property value.
	 */
	protected String getPropertyDefaulted(String propertyName, String defaulted) {
		String property = getProperty(propertyName);
		return property == null ? defaulted : property;
	}

	/**
	 * Gets a property for this module. If the property is not defined then a
	 * {@link StartupException} is thrown.
	 * 
	 * @param propertyName
	 *            The name of the property looking for.
	 * @return The property value.
	 */
	protected String getRequiredProperty(String propertyName) {
		String property = getProperty(propertyName);
		if (property == null) {
			throw new StartupException("The " + getDisplay() + " was missing the required field '" + propertyName
					+ "' in the parameter file.");
		}
		return property;
	}

	/**
	 * Gets a property for this module. If the property could not be found null
	 * will be returned.
	 *
	 * @param propertyName
	 *            The property name looking for.
	 * @return The property value.
	 * @see {{@link #getPropertyDefaulted(String, String)}
	 */
	protected String getProperty(String propertyName) {
		if (propertySupplier == null) {
			return null;
		}
		return propertySupplier.apply(propertyName);
	}
}
