package com.cerner.vicarius.data.access;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyCollection;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.Serializable;
import java.util.List;
import java.util.stream.IntStream;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import com.google.common.collect.Lists;

@SuppressWarnings({ "rawtypes", "unchecked", "deprecation" })
public class DaoTest {

	@Before
	public void setup() {
		Dao.setHostName("Test");
		Dao.setSessionFactory(makeSessionFactory());
	}

	@Test(expected = IllegalArgumentException.class)
	public void setHostName_NullHostName_ExceptionThrown() {
		Dao.setHostName(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void setHostName_EmptyString_ExceptionThrown() {
		Dao.setHostName("");
	}

	@Test(expected = IllegalArgumentException.class)
	public void setHostName_WhiteSpace_ExceptionThrown() {
		Dao.setHostName("   ");
	}

	@Test(expected = IllegalArgumentException.class)
	public void setUsername_NullValue_ExceptionThrown() {
		Dao.setUsername(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void setUsername_EmptyValue_ExceptionThrown() {
		Dao.setUsername("");
	}

	@Test(expected = IllegalArgumentException.class)
	public void setUsername_OnlyWhiteSpace_ExceptionThrown() {
		Dao.setUsername("   ");
	}

	@Test(expected = IllegalArgumentException.class)
	public void setPassword_NullValue_ExceptionThrown() {
		Dao.setPassword(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void setPassword_EmptyValue_ExceptionThrown() {
		Dao.setPassword(new char[] {});
	}

	@Test
	public void closeConnections_SessionFactoryClosed() {
		SessionFactory sf = makeSessionFactory();
		Dao.setSessionFactory(sf);
		Dao.closeConnections();

		verify(sf).close();
	}

	@Test
	public void testConnection_QueryRan() {
		Session session = makeSession();
		SessionFactory sf = makeSessionFactory(session);

		Dao.setSessionFactory(sf);
		Dao.testConnection();

		verify(session).createNativeQuery("select 0");
	}

	@Test
	public void testConnection_NoException_ReturnsTrue() {
		Session session = makeSession();
		SessionFactory sf = makeSessionFactory(session);

		Dao.setSessionFactory(sf);
		assertTrue(Dao.testConnection());
	}

	@Test
	public void testConnection_CreateQueryThrowsException_ReturnsFalse() {
		Session session = makeSession();
		doThrow(new HibernateException("")).when(session).createNativeQuery(anyString());
		SessionFactory sf = makeSessionFactory(session);

		Dao.setSessionFactory(sf);
		assertFalse(Dao.testConnection());
	}

	@Test
	public void testConnection_GetResultThrowsException_ReturnsFalse() {
		NativeQuery q = makeNativeQuery();
		doThrow(new HibernateException("")).when(q).getSingleResult();
		Session session = makeSession(q);
		SessionFactory sf = makeSessionFactory(session);

		Dao.setSessionFactory(sf);
		assertFalse(Dao.testConnection());
	}

	@Test(expected = IllegalArgumentException.class)
	public void setSessionFactory_NullParameter_ExceptionThrown() {
		Dao.setSessionFactory(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void partitionInParameterList_NullList_ExceptionThrown() {
		Dao.partitionInParameterList(null);
	}

	@Test
	public void partitionInParameterList_EmptyList_EmptyListReturned() {
		List<?> list = Lists.newArrayList();
		assertEquals(0, Dao.partitionInParameterList(list).size());
	}

	@Test
	public void partitionInParameterList_ListPartitionedInMaxInParameterChunks() {
		List<String> list = Lists.newArrayList();
		IntStream.generate(() -> 1).limit(Dao.MAX_IN_PARAMS).forEach(i -> list.add(""));
		assertEquals(1, Dao.partitionInParameterList(list).size());
	}

	@Test
	public void partitionInParameterList_MoreThanMax_ListPartitionedInMaxInParameterChunks() {
		List<String> list = Lists.newArrayList();
		IntStream.generate(() -> 1).limit(Dao.MAX_IN_PARAMS + 1).forEach(i -> list.add(""));
		assertEquals(2, Dao.partitionInParameterList(list).size());
	}

	@Test(expected = IllegalArgumentException.class)
	public void persist_NullEntity_ExceptionThrown() {
		DaoConcrete dao = new DaoConcrete();
		dao.persist(null);
	}

	@Test
	public void persist_EntitiesActiveIndAndDateSet() {
		EntityConcrete entity = new EntityConcrete();
		DaoConcrete dao = new DaoConcrete();
		dao.persist(entity);

		assertTrue(entity.getActiveInd());
		assertNotNull(entity.getUpdtDttm());
	}

	@Test
	public void persist_TransactionStarted() {
		Session session = makeSession();
		SessionFactory sf = makeSessionFactory(session);
		Dao.setSessionFactory(sf);

		EntityConcrete entity = new EntityConcrete();
		DaoConcrete dao = new DaoConcrete();
		dao.persist(entity);

		verify(session).beginTransaction();
	}

	@Test
	public void persist_EntityPersisted() {
		Session session = makeSession();
		SessionFactory sf = makeSessionFactory(session);
		Dao.setSessionFactory(sf);

		EntityConcrete entity = new EntityConcrete();
		DaoConcrete dao = new DaoConcrete();
		dao.persist(entity);

		verify(session).saveOrUpdate(entity);
	}

	@Test(expected = IllegalArgumentException.class)
	public void remove_NullEntity_ExceptionThrown() {
		new DaoConcrete().remove(null);
	}

	@Test
	public void remove_EntityRemoved() {
		Session session = makeSession();
		SessionFactory sf = makeSessionFactory(session);
		Dao.setSessionFactory(sf);

		EntityConcrete entity = new EntityConcrete();
		DaoConcrete dao = new DaoConcrete();
		dao.remove(entity);

		verify(session).delete(entity);
	}

	@Test
	public void remove_TransactionStarted() {
		Session session = makeSession();
		SessionFactory sf = makeSessionFactory(session);
		Dao.setSessionFactory(sf);

		EntityConcrete entity = new EntityConcrete();
		DaoConcrete dao = new DaoConcrete();
		dao.remove(entity);

		verify(session).beginTransaction();
	}

	@Test(expected = IllegalArgumentException.class)
	public void inactivateEntity_NullEntity_ExceptionThrown() {
		new DaoConcrete().inactivate((EntityConcrete) null);
	}

	@Test
	public void inactivateEntity_ActiveIndAndUpdateDateSet() {
		EntityConcrete entity = new EntityConcrete();
		entity.setActiveInd(true);

		DaoConcrete dao = new DaoConcrete();
		dao.inactivate(entity);

		assertFalse(entity.getActiveInd());
		assertNotNull(entity.getUpdtDttm());
	}

	@Test
	public void inactivateEntity_TransactionStarted() {
		Session session = makeSession();
		SessionFactory sf = makeSessionFactory(session);
		Dao.setSessionFactory(sf);

		EntityConcrete entity = new EntityConcrete();
		DaoConcrete dao = new DaoConcrete();
		dao.inactivate(entity);

		verify(session).beginTransaction();
	}

	@Test
	public void inactivateEntity_EntityPersisted() {
		Session session = makeSession();
		SessionFactory sf = makeSessionFactory(session);
		Dao.setSessionFactory(sf);

		EntityConcrete entity = new EntityConcrete();
		DaoConcrete dao = new DaoConcrete();
		dao.inactivate(entity);

		verify(session).saveOrUpdate(entity);
	}

	@Test(expected = IllegalArgumentException.class)
	public void inactivateID_NullId_ExceptionThrown() {
		new DaoConcrete().inactivate((Serializable) null);
	}

	@Test
	public void inactivateID_EntityNotFound_PersistNotCalled() {
		Session session = makeSession();

		when(session.get(any(Class.class), any(Serializable.class))).thenReturn(null);

		SessionFactory sf = makeSessionFactory(session);
		Dao.setSessionFactory(sf);

		DaoConcrete dao = new DaoConcrete();
		dao.inactivate("");

		verify(session, never()).saveOrUpdate(any(Object.class));
		verify(sf, times(1)).getCurrentSession();
	}

	@Test
	public void inactivateID_EntityFound_FieldsUpdated() {
		Session session = makeSession();

		EntityConcrete entity = new EntityConcrete();
		entity.setActiveInd(true);
		when(session.get(any(Class.class), any(Serializable.class))).thenReturn(entity);

		SessionFactory sf = makeSessionFactory(session);
		Dao.setSessionFactory(sf);

		DaoConcrete dao = new DaoConcrete();
		dao.inactivate("");

		assertNotNull(entity.getUpdtDttm());
		assertFalse(entity.getActiveInd());
	}

	@Test
	public void inactivateID_TransactionStarted() {
		Session session1 = makeSession();
		Session session2 = makeSession();

		EntityConcrete entity = new EntityConcrete();
		entity.setActiveInd(true);
		when(session1.get(any(Class.class), any(Serializable.class))).thenReturn(entity);

		SessionFactory sf = makeSessionFactory(session1, session2);
		Dao.setSessionFactory(sf);

		DaoConcrete dao = new DaoConcrete();
		dao.inactivate("");

		verify(session2).beginTransaction();
	}

	@Test
	public void inactivateID_EntityPersisted() {
		Session session = makeSession();

		EntityConcrete entity = new EntityConcrete();
		entity.setActiveInd(true);
		when(session.get(any(Class.class), any(Serializable.class))).thenReturn(entity);

		SessionFactory sf = makeSessionFactory(session);
		Dao.setSessionFactory(sf);

		DaoConcrete dao = new DaoConcrete();
		dao.inactivate("");

		verify(session).saveOrUpdate(entity);
	}

	@Test(expected = IllegalArgumentException.class)
	public void reactivateEntity_NullEntity_ExceptionThrown() {
		new DaoConcrete().reactivate((EntityConcrete) null);
	}

	@Test
	public void reactivateEntity_ActiveIndAndUpdateDateSet() {
		EntityConcrete entity = new EntityConcrete();

		DaoConcrete dao = new DaoConcrete();
		dao.reactivate(entity);

		assertTrue(entity.getActiveInd());
		assertNotNull(entity.getUpdtDttm());
	}

	@Test
	public void reactivateEntity_TransactionStarted() {
		Session session = makeSession();
		SessionFactory sf = makeSessionFactory(session);
		Dao.setSessionFactory(sf);

		EntityConcrete entity = new EntityConcrete();
		DaoConcrete dao = new DaoConcrete();
		dao.reactivate(entity);

		verify(session).beginTransaction();
	}

	@Test
	public void reactivateEntity_EntityPersisted() {
		Session session = makeSession();
		SessionFactory sf = makeSessionFactory(session);
		Dao.setSessionFactory(sf);

		EntityConcrete entity = new EntityConcrete();
		DaoConcrete dao = new DaoConcrete();
		dao.reactivate(entity);

		verify(session).saveOrUpdate(entity);
	}

	@Test(expected = IllegalArgumentException.class)
	public void reactivateID_NullId_ExceptionThrown() {
		new DaoConcrete().reactivate((Serializable) null);
	}

	@Test
	public void reactivateID_EntityNotFound_PersistNotCalled() {
		Session session = makeSession();

		when(session.get(any(Class.class), any(Serializable.class))).thenReturn(null);

		SessionFactory sf = makeSessionFactory(session);
		Dao.setSessionFactory(sf);

		DaoConcrete dao = new DaoConcrete();
		dao.reactivate("");

		verify(session, never()).saveOrUpdate(any(Object.class));
		verify(sf, times(1)).getCurrentSession();
	}

	@Test
	public void reactivateID_EntityFound_FieldsUpdated() {
		Session session = makeSession();

		EntityConcrete entity = new EntityConcrete();
		when(session.get(any(Class.class), any(Serializable.class))).thenReturn(entity);

		SessionFactory sf = makeSessionFactory(session);
		Dao.setSessionFactory(sf);

		DaoConcrete dao = new DaoConcrete();
		dao.reactivate("");

		assertNotNull(entity.getUpdtDttm());
		assertTrue(entity.getActiveInd());
	}

	@Test
	public void reactivateID_TransactionStarted() {
		Session session1 = makeSession();
		Session session2 = makeSession();

		EntityConcrete entity = new EntityConcrete();
		when(session1.get(any(Class.class), any(Serializable.class))).thenReturn(entity);

		SessionFactory sf = makeSessionFactory(session1, session2);
		Dao.setSessionFactory(sf);

		DaoConcrete dao = new DaoConcrete();
		dao.reactivate("");

		verify(session2).beginTransaction();
	}

	@Test
	public void reactivateID_EntityPersisted() {
		Session session = makeSession();

		EntityConcrete entity = new EntityConcrete();
		when(session.get(any(Class.class), any(Serializable.class))).thenReturn(entity);

		SessionFactory sf = makeSessionFactory(session);
		Dao.setSessionFactory(sf);

		DaoConcrete dao = new DaoConcrete();
		dao.reactivate("");

		verify(session).saveOrUpdate(entity);
	}

	@Test(expected = IllegalArgumentException.class)
	public void findById_NullId_ExceptionThrown() {
		new DaoConcrete().findById(null);
	}

	@Test
	public void findById_QueryCalled() {
		Session session = makeSession();
		SessionFactory sf = makeSessionFactory(session);
		Dao.setSessionFactory(sf);

		Long id = 1L;
		DaoConcrete dao = new DaoConcrete();
		dao.findById(id);

		Class<EntityConcrete> entityType = EntityConcrete.class;
		verify(session).get(entityType, id);
	}

	@Test(expected = IllegalArgumentException.class)
	public void findByIdList_NullList_ExceptionThrown() {
		new DaoConcrete().findByIds(null);
	}

	@Test
	public void findByIdList_EmptyList_EmptyListReturned() {
		List<Long> ids = Lists.newArrayList();
		assertEquals(0, new DaoConcrete().findByIds(ids).size());
	}

	@Test
	public void findByIdList_QueryUsed() {
		Session session = makeSession();
		SessionFactory sf = makeSessionFactory(session);
		Dao.setSessionFactory(sf);

		List<Long> ids = Lists.newArrayList(1L);
		DaoConcrete dao = new DaoConcrete();
		dao.findByIds(ids);

		Class<EntityConcrete> entityType = EntityConcrete.class;
		verify(session).createQuery("from com.cerner.vicarius.data.access.EntityConcrete where id IN :ID_LIST",
				entityType);
	}

	@Test
	public void findByIdList_ParameterListSet() {
		Query query = makeQuery();
		Session session = makeSession(query);
		SessionFactory sf = makeSessionFactory(session);
		Dao.setSessionFactory(sf);

		List<Long> ids = Lists.newArrayList(1L);
		DaoConcrete dao = new DaoConcrete();
		dao.findByIds(ids);

		ArgumentCaptor<List> captor = ArgumentCaptor.forClass(List.class);
		Class<EntityConcrete> entityType = EntityConcrete.class;
		verify(query).setParameterList(eq("ID_LIST"), captor.capture());

		assertEquals(ids, captor.getValue());
	}

	@Test
	public void findByIdList_OverMaxInParameters_TwoQueries() {
		Query query1 = makeQuery();
		Query query2 = makeQuery();
		Session session = makeSession(query1);
		when(session.createQuery(anyString(), any(Class.class))).thenReturn(query1).thenReturn(query2);
		SessionFactory sf = makeSessionFactory(session);
		Dao.setSessionFactory(sf);

		List<Long> ids = Lists.newArrayList();
		for (int i = 0; i < Dao.MAX_IN_PARAMS + 1; i++) {
			ids.add(Long.valueOf(i));
		}
		DaoConcrete dao = new DaoConcrete();
		dao.findByIds(ids);

		ArgumentCaptor<List> captor = ArgumentCaptor.forClass(List.class);
		verify(query1).setParameterList(eq("ID_LIST"), captor.capture());
		verify(query2).setParameterList(eq("ID_LIST"), captor.capture());

		List<List> arguments = captor.getAllValues();
		assertEquals(2, arguments.size());
		assertEquals(Dao.MAX_IN_PARAMS, arguments.get(0).size());
		assertEquals(1, arguments.get(1).size());
	}

	@Test
	public void findActive_QueryUsed() {
		Session session = makeSession();
		SessionFactory sf = makeSessionFactory(session);
		Dao.setSessionFactory(sf);

		DaoConcrete dao = new DaoConcrete();
		dao.findActive();

		Class<EntityConcrete> entityType = EntityConcrete.class;
		verify(session).createQuery("from com.cerner.vicarius.data.access.EntityConcrete where activeInd = 1",
				entityType);
	}

	private Transaction makeTransaction() {
		return mock(Transaction.class);
	}

	private Session makeSession() {
		NativeQuery q = makeNativeQuery();
		return makeSession(q);
	}

	private Session makeSession(Query q) {
		return makeSession(makeNativeQuery(), q, makeTransaction());
	}

	private Session makeSession(NativeQuery q) {
		return makeSession(q, makeQuery(), makeTransaction());
	}

	private Session makeSession(NativeQuery nativeQuery, Query query, Transaction transaction) {
		Session session = mock(Session.class);
		when(session.createNativeQuery(anyString())).thenReturn(nativeQuery);
		when(session.createQuery(anyString(), any(Class.class))).thenReturn(query);
		when(session.beginTransaction()).thenReturn(transaction);
		return session;
	}

	private NativeQuery makeNativeQuery() {
		NativeQuery q = mock(NativeQuery.class);
		when(q.setParameterList(anyString(), anyCollection())).thenReturn(q);
		return q;
	}

	private Query makeQuery() {
		Query q = mock(Query.class);
		when(q.setParameterList(anyString(), anyCollection())).thenReturn(q);
		return q;
	}

	private SessionFactory makeSessionFactory() {
		return makeSessionFactory(makeSession());
	}

	private SessionFactory makeSessionFactory(Session session) {
		SessionFactory sf = mock(SessionFactory.class);
		when(sf.getCurrentSession()).thenReturn(session);
		return sf;
	}

	private SessionFactory makeSessionFactory(Session session1, Session session2) {
		SessionFactory sf = mock(SessionFactory.class);
		when(sf.getCurrentSession()).thenReturn(session1).thenReturn(session2);
		return sf;
	}
}
