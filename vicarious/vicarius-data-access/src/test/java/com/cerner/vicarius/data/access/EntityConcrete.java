package com.cerner.vicarius.data.access;

import java.io.Serializable;
import java.util.Date;

import com.cerner.vicarius.data.model.DataEntity;

public class EntityConcrete implements DataEntity {

	private final Serializable primaryKey;
	private Date updtDttm;
	private boolean activeInd;

	public EntityConcrete() {
		this(null);
	}

	public EntityConcrete(Serializable primaryKey) {
		this.primaryKey = primaryKey;
	}

	@Override
	public Serializable getPrimaryKey() {
		return primaryKey;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}

	public boolean getActiveInd() {
		return activeInd;
	}
}