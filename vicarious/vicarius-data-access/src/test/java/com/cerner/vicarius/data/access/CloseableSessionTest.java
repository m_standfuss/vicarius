package com.cerner.vicarius.data.access;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Test;

@SuppressWarnings("resource")
public class CloseableSessionTest {

	@Test(expected = IllegalArgumentException.class)
	public void constructor_NullSession_ExceptionThrown() {
		new CloseableSession(null);
	}

	@Test
	public void getSession_ReturnsSessionSet() {
		Session s = makeSession();
		CloseableSession session = new CloseableSession(s);

		assertEquals(s, session.getSession());
	}

	@Test
	public void close_SessionCloseThrowsException_Handled() {
		Session s = makeSession();
		doThrow(new HibernateException("")).when(s).close();

		CloseableSession session = new CloseableSession(s);
		session.close();
	}

	private Session makeSession() {
		Session session = mock(Session.class);
		when(session.beginTransaction()).thenReturn(mock(Transaction.class));
		return session;
	}
}
