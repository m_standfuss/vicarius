CREATE DATABASE IF NOT EXISTS `vicarius`;
USE `vicarius`;

-- This creates the default user with the default password.
CREATE USER IF NOT EXISTS 'vicarius'@'%' IDENTIFIED BY 'vicarius';
GRANT ALL ON vicarius.* TO 'vicarius'@'%' identified by 'vicarius';
FLUSH PRIVILEGES;

--
-- Table structure for table `user`
--
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
	`user_id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`username` VARCHAR(45) NOT NULL DEFAULT '',
	`name_full_formatted` VARCHAR(255) NOT NULL,
	`updt_dttm` datetime DEFAULT NULL,
	`active_ind` tinyint(1) NOT NULL DEFAULT b'1',
	PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `module`
--

DROP TABLE IF EXISTS `module`;
CREATE TABLE `module` (
	`module_id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`display` VARCHAR(45) NOT NULL DEFAULT '',
	`jar_file` VARCHAR(255) NOT NULL,
	`class_name` VARCHAR(255) NOT NULL,
	`updt_dttm` datetime DEFAULT NULL,
	`active_ind` tinyint(1) NOT NULL DEFAULT b'1',
	PRIMARY KEY (`module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `job`
--
DROP TABLE IF EXISTS `job`;
CREATE TABLE `job` (
	`job_id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`module_id` BIGINT(20) UNSIGNED NOT NULL,
	`display` VARCHAR(45) NOT NULL DEFAULT '',
	`condition_ind` tinyint(1) NOT NULL DEFAULT b'0',
	`class_name` VARCHAR(255) NOT NULL,
	`updt_dttm` datetime DEFAULT NULL,
	`active_ind` tinyint(1) NOT NULL DEFAULT b'1',
	PRIMARY KEY (`job_id`),
	CONSTRAINT `job_ibfk_module_id` FOREIGN KEY (`module_id`) REFERENCES `module` (`module_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `job_property`
--
DROP TABLE IF EXISTS `job_property`;
CREATE TABLE `job_property` (
	`job_property_id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`job_id` BIGINT(20) UNSIGNED NOT NULL,
	`name` VARCHAR(45) NOT NULL DEFAULT '',
	`display` VARCHAR(45) NOT NULL DEFAULT '',
	`required_ind` tinyint(1) NOT NULL DEFAULT b'0',
	`method_name` VARCHAR(255) NOT NULL,
	`io_type` VARCHAR(45) NOT NULL DEFAULT '',
	`property_type` VARCHAR(45) NOT NULL DEFAULT '',
	`job_parameter_set_id` BIGINT(20) UNSIGNED NULL DEFAULT NULL,
	`updt_dttm` datetime DEFAULT NULL,
	`active_ind` tinyint(1) NOT NULL DEFAULT b'1',
	PRIMARY KEY (`job_property_id`),
	CONSTRAINT `job_property_ibfk_job_id` FOREIGN KEY (`job_id`) REFERENCES `job` (`job_id`) ON DELETE CASCADE,
	CONSTRAINT `job_property_ibfk_job_parameter_set_id` FOREIGN KEY (`job_parameter_set_id`) REFERENCES `job_property_set` (`job_parameter_set_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Table structure for table `job_parameter_set`
-- 
DROP TABLE IF EXISTS `job_parameter_set`;
CREATE TABLE `job_parameter_set` (
	`job_parameter_set_id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`module_id` BIGINT(20) UNSIGNED NOT NULL,
	`class_name` VARCHAR(255) NOT NULL,
	`updt_dttm` datetime DEFAULT NULL,
	`active_ind` tinyint(1) NOT NULL DEFAULT b'1',
	PRIMARY KEY (`job_parameter_set_id`),
	CONSTRAINT `job_property_set_ibfk_module_id` FOREIGN KEY (`module_id`) REFERENCES `module` (`module_id`) ON DELETE CASCADE,
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Table structure for table `job_parameter_set_property`
--
DROP TABLE IF EXISTS `job_parameter_set_property`;
CREATE TABLE `job_parameter_set_property` (
	`job_parameter_set_property_id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`job_parameter_set_id` BIGINT(20) UNSIGNED NOT NULL,
	`required_ind` tinyint(1) NOT NULL DEFAULT b'0',
	`method_name` VARCHAR(255) NOT NULL,
	`property_type` VARCHAR(45) NOT NULL DEFAULT '',
	`updt_dttm` datetime DEFAULT NULL,
	`active_ind` tinyint(1) NOT NULL DEFAULT b'1',
	PRIMARY KEY (`job_parameter_set_property_id`),
	CONSTRAINT `job_parameter_set_property_ibfk_job_property_set_id` FOREIGN KEY (`job_parameter_set_id`) REFERENCES `job_property_set` (`job_parameter_set_id`) ON DELETE CASCADE,
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `rule`
--
DROP TABLE IF EXISTS `rule`;
CREATE TABLE `rule` (
	`rule_id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`user_id` BIGINT(20) UNSIGNED NOT NULL,
	`created_dttm` DATETIME DEFAULT NULL,
	`priority` VARCHAR(30) DEFAULT '',
	`offset` BIGINT(20) UNSIGNED NOT NULL DEFAULT 0,
	`last_ran_dttm` datetime DEFAULT NULL,
	`last_completed_dttm` datetime DEFAULT NULL,
	`updt_dttm` datetime DEFAULT NULL,
	`active_ind` tinyint(1) NOT NULL DEFAULT b'1',
	PRIMARY KEY (`rule_id`),
	CONSTRAINT `rule_ibfk_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `rule_job`
--
DROP TABLE IF EXISTS `rule_job`;
CREATE TABLE `rule_job` (
	`rule_job_id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`rule_id` BIGINT(20) UNSIGNED NOT NULL,
	`job_id` BIGINT(20) UNSIGNED NOT NULL,
	`collation_seq` BIGINT(20) UNSIGNED NOT NULL,
	`updt_dttm` datetime DEFAULT NULL,
	`active_ind` tinyint(1) NOT NULL DEFAULT b'1',
	PRIMARY KEY (`rule_job_id`),
	CONSTRAINT `rule_job_ibfk_rule_id` FOREIGN KEY (`rule_id`) REFERENCES `rule` (`rule_id`) ON DELETE CASCADE,
	CONSTRAINT `rule_job_ibfk_job_id` FOREIGN KEY (`job_id`) REFERENCES `job` (`job_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `rule_job_save`
--
DROP TABLE IF EXISTS `rule_job_save`;
CREATE TABLE `rule_job_save` (
	`rule_job_save_id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT, 
	`rule_job_id` BIGINT(20) UNSIGNED NOT NULL,
	`content` LONGTEXT DEFAULT NULL,   
	`updt_dttm` datetime DEFAULT NULL,
	`active_ind` tinyint(1) NOT NULL DEFAULT b'1',
	PRIMARY KEY (`rule_job_save_id`),
	CONSTRAINT `rule_job_save_ibfk_rule_job_id` FOREIGN KEY (`rule_job_id`) REFERENCES `rule_job` (`rule_job_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `rule_job_parameter`
--
DROP TABLE IF EXISTS `rule_job_parameter`;
CREATE TABLE `rule_job_parameter` (
	`rule_job_parameter_id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT, 
	`rule_job_id` BIGINT(20) UNSIGNED NOT NULL,
	`property_name` VARCHAR(45) DEFAULT NULL,
	`parameter_type` VARCHAR(45) DEFAULT NULL,
	`content` VARCHAR(1000) NOT NULL DEFAULT '',
	`updt_dttm` datetime DEFAULT NULL,
	`active_ind` tinyint(1) NOT NULL DEFAULT b'1',
	PRIMARY KEY (`rule_job_parameter_id`),
	CONSTRAINT `rule_job_parameter_ibfk_rule_job_id` FOREIGN KEY (`rule_job_id`) REFERENCES `rule_job` (`rule_job_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;