package com.cerner.vicarius.data.model;

public enum PropertyIOType {
	INPUT, OUTPUT
}