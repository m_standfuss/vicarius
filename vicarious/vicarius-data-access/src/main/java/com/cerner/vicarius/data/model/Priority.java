package com.cerner.vicarius.data.model;

public enum Priority {
	LOW, MEDIUM, HIGH, IMMEDIATE, OFFSET
}