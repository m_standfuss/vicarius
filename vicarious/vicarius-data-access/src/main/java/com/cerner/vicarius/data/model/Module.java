package com.cerner.vicarius.data.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Model class representing a Module entity in the database.
 *
 * @author Venky
 */
@Entity
@Table(name = "module")
public class Module implements DataEntity {

	@Column(name = "module_id")
	@Id
	@GeneratedValue(strategy = IDENTITY)
	private long moduleId;

	@Column(name = "display")
	private String display;

	@Column(name = "class_name")
	private String className;

	@Column(name = "jar_file")
	private String jarFile;

	@Column(name = "updt_dttm")
	private Date updtDttm;

	@Column(name = "active_ind")
	private boolean activeInd;

	public long getModuleId() {
		return moduleId;
	}

	public void setModuleId(long moduleId) {
		this.moduleId = moduleId;
	}

	public String getDisplay() {
		return display;
	}

	public void setDisplay(String display) {
		this.display = display;
	}

	public String getJarFile() {
		return jarFile;
	}

	public void setJarFile(String jarFile) {
		this.jarFile = jarFile;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	public boolean getActiveInd() {
		return activeInd;
	}

	@Override
	public Serializable getPrimaryKey() {
		return moduleId;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}

	@Override
	public String toString() {
		return "Module [moduleId=" + moduleId + ", display=" + display + ", className=" + className + ", jarFile="
				+ jarFile + ", updtDttm=" + updtDttm + ", activeInd=" + activeInd + "]";
	}
}
