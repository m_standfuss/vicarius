package com.cerner.vicarius.data.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

/**
 * A property to be set in order to obtain a job parameter set.
 * 
 * @author MS025633
 *
 */
public class JobParameterSetProperty implements DataEntity {

	@Id
	@Column(name = "job_parameter_set_property_id")
	private Long jobParameterSetPropertyId;

	@Column(name = "job_parameter_set_id")
	private Long jobParameterSetId;

	@Column(name = "required_ind")
	private boolean requiredInd;

	@Column(name = "method_name")
	private String methodName;

	@Column(name = "property_type")
	@Enumerated(EnumType.STRING)
	private PropertyType propertyType;

	@Column(name = "updt_dttm")
	private Date updtDttm;

	@Column(name = "active_ind")
	private boolean activeInd;

	@Override
	public Serializable getPrimaryKey() {
		return getJobParameterSetId();
	}

	public Long getJobParameterSetPropertyId() {
		return jobParameterSetPropertyId;
	}

	public void setJobParameterSetPropertyId(Long jobParameterSetPropertyId) {
		this.jobParameterSetPropertyId = jobParameterSetPropertyId;
	}

	public Long getJobParameterSetId() {
		return jobParameterSetId;
	}

	public void setJobParameterSetId(Long jobParameterSetId) {
		this.jobParameterSetId = jobParameterSetId;
	}

	public boolean isRequiredInd() {
		return requiredInd;
	}

	public void setRequiredInd(boolean requiredInd) {
		this.requiredInd = requiredInd;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public PropertyType getPropertyType() {
		return propertyType;
	}

	public void setPropertyType(PropertyType propertyType) {
		this.propertyType = propertyType;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	public boolean isActiveInd() {
		return activeInd;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}

}
