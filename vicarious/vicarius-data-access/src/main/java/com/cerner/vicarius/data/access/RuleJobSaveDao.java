package com.cerner.vicarius.data.access;

import static com.google.common.base.Preconditions.checkArgument;

import com.cerner.vicarius.data.model.RuleJobSave;

/**
 * The {@link RuleJobSave} data access object.
 *
 * @author MS025633
 */
public class RuleJobSaveDao extends Dao<RuleJobSave> {
	
	// Queries
	private static final String FIND_RECENT_BY_RULE_JOB = "findMostRecentByRuleJob_RuleJobSave";
	private static final String DELETE_BY_RULE_JOB = "deleteByRuleJob_RuleJobSave";
	
	// Parameters
	private static final String RULE_JOB_ID = "RULE_JOB_ID";
	
	/**
	 * Deletes any saves for the rule job identifier.
	 *
	 * @param ruleJobId
	 *            The rule job identifier.
	 * @throws IllegalArgumentException
	 *             If the rule job identifier is null.
	 */
	public void deleteByRuleJob(Long ruleJobId) {
		checkArgument(ruleJobId != null, "The ruleJobId cannot be null.");
		try (CloseableSession session = getOpenSession()) {
			session.getSession().getNamedQuery(DELETE_BY_RULE_JOB).setParameter(RULE_JOB_ID, ruleJobId);
		}
	}
	
	/**
	 * Finds the most recent save for the rule job identifier supplied. Returns null if no save could be found.
	 * 
	 * @param ruleJobId
	 *            The rule job identifer.
	 * @return The save model.
	 */
	public RuleJobSave findMostRecentByRuleJobId(Long ruleJobId) {
		checkArgument(ruleJobId != null, "The ruleJobId cannot be null.");
		try (CloseableSession session = getOpenSession()) {
			return getSingleResult(session.getSession().getNamedQuery(FIND_RECENT_BY_RULE_JOB)
					.setParameter(RULE_JOB_ID, ruleJobId).getResultList());
		}
	}
	
}
