package com.cerner.vicarius.data.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Model class representing a Rule entity in the database.
 *
 * @author Venky
 */
@Entity
@Table(name = "rule")
public class Rule implements DataEntity {

	@Column(name = "rule_id")
	@Id
	@GeneratedValue(strategy = IDENTITY)
	private long ruleId;

	@Column(name = "user_id")
	private long userId;

	@Column(name = "created_dttm")
	private Date createDttm;

	@Column(name = "updt_dttm")
	private Date updtDttm;

	@Column(name = "active_ind")
	private boolean activeInd;

	@Enumerated(EnumType.STRING)
	@Column(name = "priority")
	private Priority priority;

	@Column(name = "offset")
	private long offset;

	@Column(name = "last_completed_dttm")
	private Date lastCompleted;

	@Column(name = "last_ran_dttm")
	private Date lastRan;

	public long getRuleId() {
		return ruleId;
	}

	public void setRuleId(long ruleId) {
		this.ruleId = ruleId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public Date getCreateDttm() {
		return createDttm;
	}

	public void setCreateDttm(Date createDttm) {
		this.createDttm = createDttm;
	}

	public Priority getPriority() {
		return priority;
	}

	public void setPriority(Priority priority) {
		this.priority = priority;
	}

	public long getOffset() {
		return offset;
	}

	public void setOffset(long offset) {
		this.offset = offset;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	public boolean getActiveInd() {
		return activeInd;
	}

	public Date getLastCompleted() {
		return lastCompleted;
	}

	public void setLastCompleted(Date lastCompleted) {
		this.lastCompleted = lastCompleted;
	}

	public Date getLastRan() {
		return lastRan;
	}

	public void setLastRan(Date lastRan) {
		this.lastRan = lastRan;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	@Override
	public Serializable getPrimaryKey() {
		return ruleId;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}

	@Override
	public String toString() {
		return "Rule [ruleId=" + ruleId + ", userId=" + userId + ", createDttm=" + createDttm + ", updtDttm=" + updtDttm
				+ ", activeInd=" + activeInd + ", priority=" + priority + ", offset=" + offset + ", lastCompleted="
				+ lastCompleted + ", lastRan=" + lastRan + "]";
	}
}
