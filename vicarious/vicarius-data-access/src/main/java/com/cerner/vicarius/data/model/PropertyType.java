package com.cerner.vicarius.data.model;

public enum PropertyType {
	BOOLEAN, INT, LONG, STRING, DOUBLE, FLOAT, CUSTOM;
}