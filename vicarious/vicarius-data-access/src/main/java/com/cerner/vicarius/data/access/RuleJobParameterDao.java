package com.cerner.vicarius.data.access;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.Collection;
import java.util.List;

/**
 * The {@link RuleJobParameter} data access object.
 *
 * @author MS025633
 *
 */
import com.cerner.vicarius.data.model.RuleJobParameter;
import com.google.common.collect.Lists;

public class RuleJobParameterDao extends Dao<RuleJobParameter> {

	// Queries
	private static final String FIND_BY_RULE_JOBS = "findByRuleJobIds_JobParameter";

	// Parameters
	private static final String RULE_JOB_IDS = "RULE_JOB_IDS";

	public List<RuleJobParameter> findByRuleJobs(Collection<Long> ruleJobIds) {
		checkArgument(ruleJobIds != null, "The jobIds cannot be null.");
		if (ruleJobIds.isEmpty()) {
			return Lists.newArrayList();
		}
		try (CloseableSession session = getOpenSession()) {
			List<RuleJobParameter> results = Lists.newArrayList();
			for (Collection<Long> chunk : partitionInParameterList(ruleJobIds)) {
				results.addAll(getResults(session.getSession().getNamedQuery(FIND_BY_RULE_JOBS)
						.setParameter(RULE_JOB_IDS, chunk).getResultList()));
			}
			return results;
		}
	}
}
