package com.cerner.vicarius.data.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Model class representing a association of Rule and Job.
 *
 * @author Venky
 */
@Entity
@Table(name = "rule_job")
@NamedQueries({ @NamedQuery(name = "findByRule_RuleJob", query = "SELECT rj FROM RuleJob rj WHERE "
		+ "rj.ruleId = :RULE_ID AND rj.activeInd = 1") })
public class RuleJob implements DataEntity {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "rule_job_id")
	private long ruleJobId;

	@Column(name = "rule_id")
	private long ruleId;

	@Column(name = "job_id")
	private long jobId;

	@Column(name = "collation_seq")
	private Long collationSeq;

	@Column(name = "updt_dttm")
	private Date updtDttm;

	@Column(name = "active_ind")
	private boolean activeInd;

	public long getRuleJobId() {
		return ruleJobId;
	}

	public void setRuleJobId(long ruleJobId) {
		this.ruleJobId = ruleJobId;
	}

	public long getRuleId() {
		return ruleId;
	}

	public void setRuleId(long ruleId) {
		this.ruleId = ruleId;
	}

	public long getJobId() {
		return jobId;
	}

	public void setJobId(long jobId) {
		this.jobId = jobId;
	}

	public Long getCollationSeq() {
		return collationSeq;
	}

	public void setCollationSeq(Long collationSeq) {
		this.collationSeq = collationSeq;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	@Override
	public Serializable getPrimaryKey() {
		return ruleJobId;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}

	@Override
	public String toString() {
		return "RuleJob [ruleJobId=" + ruleJobId + ", ruleId=" + ruleId + ", jobId=" + jobId + ", collationSeq="
				+ collationSeq + ", updtDttm=" + updtDttm + ", activeInd=" + activeInd + "]";
	}
}
