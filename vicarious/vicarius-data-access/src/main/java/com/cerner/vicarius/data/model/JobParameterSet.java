package com.cerner.vicarius.data.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;

/**
 * A defined set of potential parameter values for a job property.
 * 
 * @author MS025633
 *
 */
public class JobParameterSet implements DataEntity {

	@Id
	@Column(name = "job_parameter_set_id")
	private Long jobParameterSetId;

	@Column(name = "module_id")
	private Long moduleId;

	@Column(name = "class_name")
	private String className;

	@Column(name = "updt_dttm")
	private Date updtDttm;

	@Column(name = "active_ind")
	private boolean activeInd;

	@Override
	public Serializable getPrimaryKey() {
		return getJobParameterSetId();
	}

	public Long getJobParameterSetId() {
		return jobParameterSetId;
	}

	public void setJobParameterSetId(Long jobParameterSetId) {
		this.jobParameterSetId = jobParameterSetId;
	}

	public Long getModuleId() {
		return moduleId;
	}

	public void setModuleId(Long moduleId) {
		this.moduleId = moduleId;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	public boolean isActiveInd() {
		return activeInd;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((jobParameterSetId == null) ? 0 : jobParameterSetId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JobParameterSet other = (JobParameterSet) obj;
		if (jobParameterSetId == null) {
			if (other.jobParameterSetId != null)
				return false;
		} else if (!jobParameterSetId.equals(other.jobParameterSetId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "JobParameterSet [jobPropertySetId=" + jobParameterSetId + ", moduleId=" + moduleId + ", className="
				+ className + ", updtDttm=" + updtDttm + ", activeInd=" + activeInd + "]";
	}
}
