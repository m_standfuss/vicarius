package com.cerner.vicarius.data.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Model class representing a Job Property entity in the database.
 *
 * @author MS025633
 */
@Entity
@Table(name = "job_property")
@NamedQueries({ @NamedQuery(name = "findByJobIds_JobProperty", query = "SELECT jp FROM JobProperty jp WHERE "
		+ "jp.jobId IN (:JOB_IDS) AND jp.activeInd = 1") })
public class JobProperty implements DataEntity {

	@Column(name = "job_property_id")
	@Id
	@GeneratedValue(strategy = IDENTITY)
	private long jobPropertyId;

	@Column(name = "job_id")
	private long jobId;

	@Column(name = "name")
	private String name;

	@Column(name = "display")
	private String display;

	@Column(name = "required_ind")
	private boolean requiredInd;

	@Column(name = "method_name")
	private String methodName;

	@Column(name = "io_type")
	@Enumerated(EnumType.STRING)
	private PropertyIOType ioType;

	@Column(name = "property_type")
	@Enumerated(EnumType.STRING)
	private PropertyType propertyType;

	@Column(name = "job_parameter_set_id")
	private Long jobParameterSetId;

	@Column(name = "updt_dttm")
	private Date updtDttm;

	@Column(name = "active_ind")
	private boolean activeInd;

	public long getJobPropertyId() {
		return jobPropertyId;
	}

	public void setJobPropertyId(long jobPropertyId) {
		this.jobPropertyId = jobPropertyId;
	}

	public long getJobId() {
		return jobId;
	}

	public void setJobId(long jobId) {
		this.jobId = jobId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDisplay() {
		return display;
	}

	public void setDisplay(String display) {
		this.display = display;
	}

	public boolean isRequiredInd() {
		return requiredInd;
	}

	public void setRequiredInd(boolean requiredInd) {
		this.requiredInd = requiredInd;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public PropertyIOType getIOType() {
		return ioType;
	}

	public void setIOType(PropertyIOType type) {
		ioType = type;
	}

	public PropertyType getPropertyType() {
		return propertyType;
	}

	public void setPropertyType(PropertyType primitiveType) {
		propertyType = primitiveType;
	}

	public Date getUpdateDttm() {
		return updtDttm;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;

	}

	public Long getJobParameterSetId() {
		return jobParameterSetId;
	}

	public void setJobParameterSetId(Long jobParameterSetId) {
		this.jobParameterSetId = jobParameterSetId;
	}

	public boolean getActiveInd() {
		return activeInd;
	}

	@Override
	public Serializable getPrimaryKey() {
		return jobPropertyId;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}

	@Override
	public String toString() {
		return "JobProperty [jobPropertyId=" + jobPropertyId + ", jobId=" + jobId + ", name=" + name + ", display="
				+ display + ", requiredInd=" + requiredInd + ", methodName=" + methodName + ", ioType=" + ioType
				+ ", propertyType=" + propertyType + ", updtDttm=" + updtDttm + ", activeInd=" + activeInd + "]";
	}
}
