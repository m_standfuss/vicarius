package com.cerner.vicarius.data.access;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.Collection;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.cerner.vicarius.data.model.JobProperty;

/**
 * The data access object for the {@link JobProperty} data model.
 *
 * @author MS025633
 */
public class JobPropertyDao extends Dao<JobProperty> {
	
	private static final Logger logger = LogManager.getLogger(JobPropertyDao.class);

	// Queries
	private static final String FIND_BY_JOB_IDS = "findByJobIds_JobProperty";

	// Parameters
	private static final String JOB_IDS = "JOB_IDS";

	/**
	 * Finds job properties for the given job identifiers.
	 * 
	 * @param jobIds
	 *            The job identifiers to look for.
	 * @return The models that satisfy the conditions.
	 * @throws IllegalArgumentException
	 *             If jobIds is null.
	 */
	public List<JobProperty> findByJobIds(Collection<Long> jobIds) {
		logger.debug("Starting findByJobIds for jobIds=" + jobIds);
		checkArgument(jobIds != null, "The jobIds cannot be null.");

		try (CloseableSession session = getOpenSession()) {
			return getResults(
					session.getSession().getNamedQuery(FIND_BY_JOB_IDS).setParameter(JOB_IDS, jobIds).getResultList());
		}
	}
}
