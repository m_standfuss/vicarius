package com.cerner.vicarius.data.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Model class representing a Job Property entity in the database.
 *
 * @author MS025633
 */
@Entity
@Table(name = "rule_job_parameter")
@NamedQueries({
		@NamedQuery(name = "findByRuleJobIds_JobParameter", query = "SELECT rjp FROM RuleJobParameter rjp WHERE "
				+ "rjp.ruleJobId IN (:RULE_JOB_IDS) AND rjp.activeInd = 1") })
public class RuleJobParameter implements DataEntity {

	public enum ParameterType {
		PRIMITIVE, EMBEDDED_STRING, OUTPUT_PARAMETER
	}

	@Column(name = "rule_job_parameter_id")
	@Id
	@GeneratedValue(strategy = IDENTITY)
	private long jobParameterId;

	@Column(name = "rule_job_id")
	private long ruleJobId;

	@Column(name = "property_name")
	private String propertyName;

	@Column(name = "parameter_type")
	@Enumerated(EnumType.STRING)
	private ParameterType type;

	@Column(name = "content")
	private String content;

	@Column(name = "updt_dttm")
	private Date updtDttm;

	@Column(name = "active_ind")
	private boolean activeInd;

	@Override
	public Serializable getPrimaryKey() {
		return getJobParameterId();
	}

	public long getJobParameterId() {
		return jobParameterId;
	}

	public void setJobParameterId(long jobParameterId) {
		this.jobParameterId = jobParameterId;
	}

	public long getRuleJobId() {
		return ruleJobId;
	}

	public void setRuleJobId(long ruleJobId) {
		this.ruleJobId = ruleJobId;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public ParameterType getType() {
		return type;
	}

	public void setType(ParameterType type) {
		this.type = type;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	public boolean isActiveInd() {
		return activeInd;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}

	@Override
	public String toString() {
		return "RuleJobParameter [jobParameterId=" + jobParameterId + ", ruleJobId=" + ruleJobId + ", propertyName="
				+ propertyName + ", type=" + type + ", content=" + content + ", updtDttm=" + updtDttm + ", activeInd="
				+ activeInd + "]";
	}
}
