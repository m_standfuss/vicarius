package com.cerner.vicarius.data.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Model class representing a User entity in the database.
 *
 * @author Venky
 */
@Entity
@Table(name = "user")
public class User implements DataEntity {

	@Column(name = "user_id")
	@Id
	@GeneratedValue(strategy = IDENTITY)
	private String userId;

	@Column(name = "user_name")
	private String userName;

	@Column(name = "name_full_formatted")
	private String nameFullFormatted;

	@Column(name = "updt_dttm")
	private Date updtDttm;

	@Column(name = "active_ind")
	private boolean activeInd;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getNameFullFormatted() {
		return nameFullFormatted;
	}

	public void setNameFullFormatted(String nameFullFormatted) {
		this.nameFullFormatted = nameFullFormatted;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	public boolean getActiveInd() {
		return activeInd;
	}

	@Override
	public Serializable getPrimaryKey() {
		return userId;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", userName=" + userName + ", nameFullFormatted=" + nameFullFormatted
				+ ", updtDttm=" + updtDttm + ", activeInd=" + activeInd + "]";
	}
}
