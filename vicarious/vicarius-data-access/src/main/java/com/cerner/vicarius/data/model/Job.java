package com.cerner.vicarius.data.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Model class representing a Job entity in the database.
 *
 * @author Venky
 */
@Entity
@Table(name = "job")
public class Job implements DataEntity {

	@Column(name = "job_id")
	@Id
	@GeneratedValue(strategy = IDENTITY)
	private long jobId;

	@Column(name = "module_id")
	private long moduleId;

	@Column(name = "display")
	private String display;

	@Column(name = "condition_ind")
	private boolean conditionInd;

	@Column(name = "class_name")
	private String className;

	@Column(name = "updt_dttm")
	private Date updtDttm;

	@Column(name = "active_ind")
	private boolean activeInd;

	public long getJobId() {
		return jobId;
	}

	public void setJobId(long jobId) {
		this.jobId = jobId;
	}

	public long getModuleId() {
		return moduleId;
	}

	public void setModuleId(long moduleId) {
		this.moduleId = moduleId;
	}

	public String getDisplay() {
		return display;
	}

	public void setDisplay(String display) {
		this.display = display;
	}

	public boolean getConditionInd() {
		return conditionInd;
	}

	public void setConditionInd(boolean conditionInd) {
		this.conditionInd = conditionInd;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public Date getUpdateDttm() {
		return updtDttm;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;

	}

	public boolean getActiveInd() {
		return activeInd;
	}

	@Override
	public Serializable getPrimaryKey() {
		return jobId;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}

	@Override
	public String toString() {
		return "Job [jobId=" + jobId + ", moduleId=" + moduleId + ", display=" + display + ", conditionInd="
				+ conditionInd + ", className=" + className + ", updtDttm=" + updtDttm + ", activeInd=" + activeInd
				+ "]";
	}
}
