package com.cerner.vicarius.data.access;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.List;

import com.cerner.vicarius.data.model.RuleJob;

/**
 * The {@link RuleJob} data access object.
 *
 * @author MS025633
 *
 */
public class RuleJobDao extends Dao<RuleJob> {

	// Queries
	private static final String FIND_BY_RULE = "findByRule_RuleJob";

	// Parameters
	private static final String RULE_ID = "RULE_ID";

	/**
	 * Finds all rule jobs for the specified rule.
	 *
	 * @param ruleId
	 *            The identifier of the rule.
	 * @return The list of active rule jobs.
	 * @throws IllegalArgumentException
	 *             If the ruleId is null.
	 */
	public List<RuleJob> findByRule(Long ruleId) {
		checkArgument(ruleId != null, "The ruleId cannot be null.");
		try (CloseableSession session = getOpenSession()) {
			return getResults(
					session.getSession().getNamedQuery(FIND_BY_RULE).setParameter(RULE_ID, ruleId).getResultList());
		}
	}
}
