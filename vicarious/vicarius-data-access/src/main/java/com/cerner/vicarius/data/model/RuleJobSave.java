package com.cerner.vicarius.data.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Model class representing a save state for a particular rule job.
 *
 * @author MS025633
 */
@Entity
@Table(name = "rule_job_save")
@NamedQueries({
		@NamedQuery(name = "findMostRecentByRuleJob_RuleJobSave", query = "SELECT rjs FROM RuleJobSave rjs WHERE "
				+ "rjs.ruleJobId = :RULE_JOB_ID AND rjs.activeInd = 1 ORDER BY rjs.updtDttm DESC"),
		@NamedQuery(name = "deleteByRuleJob_RuleJobSave", query = "UPDATE RuleJobSave rjs SET rjs.activeInd = 0 WHERE "
				+ "rjs.ruleJobId = :RULE_JOB_ID") })
public class RuleJobSave implements DataEntity {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "rule_job_save_id")
	private long ruleJobSaveId;

	@Column(name = "rule_job_id")
	private long ruleJobId;

	@Column(name = "content")
	private String content;

	@Column(name = "updt_dttm")
	private Date updtDttm;

	@Column(name = "active_ind")
	private boolean activeInd;

	public long getRuleJobSaveId() {
		return ruleJobSaveId;
	}

	public void setRuleJobSaveId(long ruleJobSaveId) {
		this.ruleJobSaveId = ruleJobSaveId;
	}

	public long getRuleJobId() {
		return ruleJobId;
	}

	public void setRuleJobId(long ruleJobId) {
		this.ruleJobId = ruleJobId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	public boolean isActiveInd() {
		return activeInd;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}

	@Override
	public Serializable getPrimaryKey() {
		return ruleJobSaveId;
	}

	@Override
	public String toString() {
		return "RuleJobSave [ruleJobSaveId=" + ruleJobSaveId + ", ruleJobId=" + ruleJobId + ", content=" + content
				+ ", updtDttm=" + updtDttm + ", activeInd=" + activeInd + "]";
	}

}
