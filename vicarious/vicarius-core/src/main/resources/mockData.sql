SET FOREIGN_KEY_CHECKS = 0;
TRUNCATE TABLE `vicarius`.`job`;
TRUNCATE TABLE `vicarius`.`job_property`;
TRUNCATE TABLE `vicarius`.`module`;
TRUNCATE TABLE `vicarius`.`rule`;
TRUNCATE TABLE `vicarius`.`rule_job`;
TRUNCATE TABLE `vicarius`.`rule_job_parameter`;
TRUNCATE TABLE `vicarius`.`rule_job_save`;
TRUNCATE TABLE `vicarius`.`user`;
SET FOREIGN_KEY_CHECKS = 1;
--
-- Users
--
INSERT INTO `vicarius`.`user` (`user_id`, `username`, `name_full_formatted`, `updt_dttm`, `active_ind`) VALUES ('1', 'vicarius', 'vicarius', now(), '1');

--
-- Modules
--
INSERT INTO `vicarius`.`module` (`module_id`, `display`, `jar_file`, `class_name`, `updt_dttm`, `active_ind`) VALUES ('1', 'HP Fortify', 'vicarius-hpfortify.jar', 'com.cerner.vicarius.hpfortify.FortifyModule', now(), '1');
INSERT INTO `vicarius`.`module` (`module_id`, `display`, `jar_file`, `class_name`, `updt_dttm`, `active_ind`) VALUES ('2', 'Email', 'vicarius-email.jar', 'com.cerner.vicarius.email.EmailModule', now(), '1');

--
-- Jobs
--
INSERT INTO `vicarius`.`job` (`job_id`, `module_id`, `display`, `condition_ind`, `class_name`, `updt_dttm`, `active_ind`) VALUES ('1', '1', 'Birt Report Generator', '0', 'com.cerner.vicarius.hpfortify.jobs.BirtReportGenerator', now(), '1');
INSERT INTO `vicarius`.`job` (`job_id`, `module_id`, `display`, `condition_ind`, `class_name`, `updt_dttm`, `active_ind`) VALUES ('2', '1', 'Issues Increased', '1', 'com.cerner.vicarius.hpfortify.jobs.IncreasedIssues', now(), '1');
INSERT INTO `vicarius`.`job` (`job_id`, `module_id`, `display`, `condition_ind`, `class_name`, `updt_dttm`, `active_ind`) VALUES ('3', '1', 'Issues Increased Custom Filter', '1', 'com.cerner.vicarius.hpfortify.jobs.IncreasedIssuesCustomFilter', now(), '1');
INSERT INTO `vicarius`.`job` (`job_id`, `module_id`, `display`, `condition_ind`, `class_name`, `updt_dttm`, `active_ind`) VALUES ('4', '1', 'Issues Increased Filtered', '1', 'com.cerner.vicarius.hpfortify.jobs.IncreasedIssuesFiltered', now(), '1');

INSERT INTO `vicarius`.`job` (`job_id`, `module_id`, `display`, `condition_ind`, `class_name`, `updt_dttm`, `active_ind`) VALUES ('5', '2', 'Send Email', '0', 'com.cerner.vicarius.email.SendSmtpEmail', now(), '1');

--
-- Job Properties
-- 

-- Birt Report Generator
INSERT INTO `vicarius`.`job_property` (`job_property_id`, `job_id`, `name`, `display`, `required_ind`, `method_name`, `io_type`, `property_type`, `updt_dttm`, `active_ind`) VALUES 
    ('1', '1', 'template', 'Template', '1', 'setTemplate', 'INPUT', 'STRING', now(), '1');
INSERT INTO `vicarius`.`job_property` (`job_property_id`, `job_id`, `name`, `display`, `required_ind`, `method_name`, `io_type`, `property_type`, `updt_dttm`, `active_ind`) VALUES 
    ('2', '1', 'format', 'Format', '1', 'setFormat', 'INPUT', 'STRING', now(), '1');
INSERT INTO `vicarius`.`job_property` (`job_property_id`, `job_id`, `name`, `display`, `required_ind`, `method_name`, `io_type`, `property_type`, `updt_dttm`, `active_ind`) VALUES 
    ('3', '1', 'application', 'Application Name', '1', 'setApplication', 'INPUT', 'STRING', now(), '1');
INSERT INTO `vicarius`.`job_property` (`job_property_id`, `job_id`, `name`, `display`, `required_ind`, `method_name`, `io_type`, `property_type`, `updt_dttm`, `active_ind`) VALUES 
    ('4', '1', 'version', 'Application Version', '1', 'setVersion', 'INPUT', 'STRING', now(), '1');
INSERT INTO `vicarius`.`job_property` (`job_property_id`, `job_id`, `name`, `display`, `required_ind`, `method_name`, `io_type`, `property_type`, `updt_dttm`, `active_ind`) VALUES 
    ('5', '1', 'searchQuery', 'Filter', '0', 'setSearchQuery', 'INPUT', 'STRING', now(), '1');
INSERT INTO `vicarius`.`job_property` (`job_property_id`, `job_id`, `name`, `display`, `required_ind`, `method_name`, `io_type`, `property_type`, `updt_dttm`, `active_ind`) VALUES 
    ('6', '1', 'showRemoved', 'Include Removed Issues', '0', 'setShowRemoved', 'INPUT', 'BOOLEAN', now(), '1');
INSERT INTO `vicarius`.`job_property` (`job_property_id`, `job_id`, `name`, `display`, `required_ind`, `method_name`, `io_type`, `property_type`, `updt_dttm`, `active_ind`) VALUES 
    ('7', '1', 'showSuppressed', 'Include Suppressed Issues', '0', 'setShowSuppressed', 'INPUT', 'BOOLEAN', now(), '1');
INSERT INTO `vicarius`.`job_property` (`job_property_id`, `job_id`, `name`, `display`, `required_ind`, `method_name`, `io_type`, `property_type`, `updt_dttm`, `active_ind`) VALUES 
    ('8', '1', 'setShowHidden', 'Include Hidden Issues', '0', 'setShowHidden', 'INPUT', 'BOOLEAN', now(), '1');
INSERT INTO `vicarius`.`job_property` (`job_property_id`, `job_id`, `name`, `display`, `required_ind`, `method_name`, `io_type`, `property_type`, `updt_dttm`, `active_ind`) VALUES 
    ('9', '1', 'report', 'BIRT Report File', '0', 'getReportFile', 'OUTPUT', 'CUSTOM', now(), '1');
	
-- Issues Increased
INSERT INTO `vicarius`.`job_property` (`job_property_id`, `job_id`, `name`, `display`, `required_ind`, `method_name`, `io_type`, `property_type`, `updt_dttm`, `active_ind`) VALUES 
    ('10', '2', 'application', 'Application Name', '1', 'setApplication', 'INPUT', 'STRING', now(), '1');
INSERT INTO `vicarius`.`job_property` (`job_property_id`, `job_id`, `name`, `display`, `required_ind`, `method_name`, `io_type`, `property_type`, `updt_dttm`, `active_ind`) VALUES 
    ('11', '2', 'version', 'Application Version', '1', 'setVersion', 'INPUT', 'STRING', now(), '1');
INSERT INTO `vicarius`.`job_property` (`job_property_id`, `job_id`, `name`, `display`, `required_ind`, `method_name`, `io_type`, `property_type`, `updt_dttm`, `active_ind`) VALUES 
    ('12', '2', 'increaseCount', 'Issue Increase Count', '0', 'getIncreaseCount', 'OUTPUT', 'INT', now(), '1');
	
-- Issues Increased Custom Filter
INSERT INTO `vicarius`.`job_property` (`job_property_id`, `job_id`, `name`, `display`, `required_ind`, `method_name`, `io_type`, `property_type`, `updt_dttm`, `active_ind`) VALUES 
    ('13', '3', 'filter', 'Issue Filter', '1', 'setFilter', 'INPUT', 'STRING', now(), '1');
INSERT INTO `vicarius`.`job_property` (`job_property_id`, `job_id`, `name`, `display`, `required_ind`, `method_name`, `io_type`, `property_type`, `updt_dttm`, `active_ind`) VALUES 
    ('14', '3', 'application', 'Application Name', '1', 'setApplication', 'INPUT', 'STRING', now(), '1');
INSERT INTO `vicarius`.`job_property` (`job_property_id`, `job_id`, `name`, `display`, `required_ind`, `method_name`, `io_type`, `property_type`, `updt_dttm`, `active_ind`) VALUES 
    ('15', '3', 'version', 'Application Version', '1', 'setVersion', 'INPUT', 'STRING', now(), '1');
INSERT INTO `vicarius`.`job_property` (`job_property_id`, `job_id`, `name`, `display`, `required_ind`, `method_name`, `io_type`, `property_type`, `updt_dttm`, `active_ind`) VALUES 
    ('16', '3', 'increaseCount', 'Issue Increase Count', '0', 'getIncreaseCount', 'OUTPUT', 'INT', now(), '1');
	
-- Issues Increased Filtered
INSERT INTO `vicarius`.`job_property` (`job_property_id`, `job_id`, `name`, `display`, `required_ind`, `method_name`, `io_type`, `property_type`, `updt_dttm`, `active_ind`) VALUES 
    ('17', '4', 'application', 'Application Name', '1', 'setApplication', 'INPUT', 'STRING', now(), '1');
INSERT INTO `vicarius`.`job_property` (`job_property_id`, `job_id`, `name`, `display`, `required_ind`, `method_name`, `io_type`, `property_type`, `updt_dttm`, `active_ind`) VALUES 
    ('18', '4', 'version', 'Application Version', '1', 'setVersion', 'INPUT', 'STRING', now(), '1');
INSERT INTO `vicarius`.`job_property` (`job_property_id`, `job_id`, `name`, `display`, `required_ind`, `method_name`, `io_type`, `property_type`, `updt_dttm`, `active_ind`) VALUES 
    ('19', '4', 'increaseCount', 'Issue Increase Count', '0', 'getIncreaseCount', 'OUTPUT', 'INT', now(), '1');
	
-- Send Email
INSERT INTO `vicarius`.`job_property` (`job_property_id`, `job_id`, `name`, `display`, `required_ind`, `method_name`, `io_type`, `property_type`, `updt_dttm`, `active_ind`) VALUES 
    ('20', '5', 'recipient', 'Recipient', '1', 'setRecipient', 'INPUT', 'STRING', now(), '1');
INSERT INTO `vicarius`.`job_property` (`job_property_id`, `job_id`, `name`, `display`, `required_ind`, `method_name`, `io_type`, `property_type`, `updt_dttm`, `active_ind`) VALUES 
    ('21', '5', 'messageText', 'Message', '1', 'setMessageText', 'INPUT', 'STRING', now(), '1');
INSERT INTO `vicarius`.`job_property` (`job_property_id`, `job_id`, `name`, `display`, `required_ind`, `method_name`, `io_type`, `property_type`, `updt_dttm`, `active_ind`) VALUES 
    ('22', '5', 'attachment', 'Attachment', '0', 'setAttachment', 'INPUT', 'CUSTOM', now(), '1');
INSERT INTO `vicarius`.`job_property` (`job_property_id`, `job_id`, `name`, `display`, `required_ind`, `method_name`, `io_type`, `property_type`, `updt_dttm`, `active_ind`) VALUES 
    ('23', '5', 'attachmentName', 'Attachment Name', '0', 'setAttachmentName', 'INPUT', 'STRING', now(), '1');

--
-- Rule
--
INSERT INTO `vicarius`.`rule` (`rule_id`, `user_id`, `created_dttm`, `priority`, `offset`, `last_ran_dttm`, `last_completed_dttm`, `updt_dttm`, `active_ind`) VALUES 
	('1', '1', now(), 'HIGH', 0, NULL, NULL, now(), '1');

--
-- Rule Jobs
--
INSERT INTO `vicarius`.`rule_job` (`rule_job_id`, `rule_id`, `job_id`, `collation_seq`, `updt_dttm`, `active_ind`) VALUES ('1', '1', '2', '1', now(), '1');
INSERT INTO `vicarius`.`rule_job` (`rule_job_id`, `rule_id`, `job_id`, `collation_seq`, `updt_dttm`, `active_ind`) VALUES ('2', '1', '1', '1', now(), '1');
INSERT INTO `vicarius`.`rule_job` (`rule_job_id`, `rule_id`, `job_id`, `collation_seq`, `updt_dttm`, `active_ind`) VALUES ('3', '1', '5', '1', now(), '1');

--
-- Rule Job Parameter
--

-- Issue Count Increase
INSERT INTO `vicarius`.`rule_job_parameter` (`rule_job_parameter_id`, `rule_job_id`, `property_name`, `parameter_type`, `content`, `updt_dttm`, `active_ind`) VALUES 
	('1', '1', 'application', 'PRIMITIVE', '724Access.DowntimeViewer', now(), '1');
INSERT INTO `vicarius`.`rule_job_parameter` (`rule_job_parameter_id`, `rule_job_id`, `property_name`, `parameter_type`, `content`, `updt_dttm`, `active_ind`) VALUES 
	('2', '1', 'version', 'PRIMITIVE', '5.8.x', now(), '1');

-- Generate Birt Report
INSERT INTO `vicarius`.`rule_job_parameter` (`rule_job_parameter_id`, `rule_job_id`, `property_name`, `parameter_type`, `content`, `updt_dttm`, `active_ind`) VALUES 
	('3', '2', 'template', 'PRIMITIVE', 'DISA STIG', now(), '1');
INSERT INTO `vicarius`.`rule_job_parameter` (`rule_job_parameter_id`, `rule_job_id`, `property_name`, `parameter_type`, `content`, `updt_dttm`, `active_ind`) VALUES 
	('4', '2', 'format', 'PRIMITIVE', 'HTML', now(), '1');
INSERT INTO `vicarius`.`rule_job_parameter` (`rule_job_parameter_id`, `rule_job_id`, `property_name`, `parameter_type`, `content`, `updt_dttm`, `active_ind`) VALUES 
	('5', '2', 'application', 'PRIMITIVE', '724Access.DowntimeViewer', now(), '1');
INSERT INTO `vicarius`.`rule_job_parameter` (`rule_job_parameter_id`, `rule_job_id`, `property_name`, `parameter_type`, `content`, `updt_dttm`, `active_ind`) VALUES 
	('6', '2', 'version', 'PRIMITIVE', '5.8.x', now(), '1');

-- Send Email
INSERT INTO `vicarius`.`rule_job_parameter` (`rule_job_parameter_id`, `rule_job_id`, `property_name`, `parameter_type`, `content`, `updt_dttm`, `active_ind`) VALUES 
	('7', '3', 'recipient', 'PRIMITIVE', 'michael.standfuss@cerner.com', now(), '1');
INSERT INTO `vicarius`.`rule_job_parameter` (`rule_job_parameter_id`, `rule_job_id`, `property_name`, `parameter_type`, `content`, `updt_dttm`, `active_ind`) VALUES 
	('8', '3', 'messageText', 'EMBEDDED_STRING', '{"value":"The issue count for the Downtime Viewer 5.8.x application has increased by @embeddedParameter1 ","properties":[{"embeddedPropertyName":"@embeddedParameter1","ruleJobId":1,"parameterName":"increaseCount"}]}', now(), '1');
INSERT INTO `vicarius`.`rule_job_parameter` (`rule_job_parameter_id`, `rule_job_id`, `property_name`, `parameter_type`, `content`, `updt_dttm`, `active_ind`) VALUES 
	('9', '3', 'attachment', 'OUTPUT_PARAMETER', '{"ruleJobId":2, "parameterName":"report"}', now(), '1');
INSERT INTO `vicarius`.`rule_job_parameter` (`rule_job_parameter_id`, `rule_job_id`, `property_name`, `parameter_type`, `content`, `updt_dttm`, `active_ind`) VALUES 
	('10', '3', 'attachmentName', 'PRIMITIVE', 'ReportFile.html', now(), '1');
