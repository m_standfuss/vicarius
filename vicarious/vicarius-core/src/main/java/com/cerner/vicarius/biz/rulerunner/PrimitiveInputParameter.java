package com.cerner.vicarius.biz.rulerunner;

import static com.google.common.base.Preconditions.checkArgument;

import java.io.InvalidObjectException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.cerner.vicarius.data.model.PropertyType;

/**
 * A primitive input parameter. All primitive parameters can be converted from a
 * string representation of that object. Current supported primitive types are:
 * <ul>
 * <li>{@link PropertyType#BOOLEAN}</li>
 * <li>{@link PropertyType#DOUBLE}</li>
 * <li>{@link PropertyType#FLOAT}</li>
 * <li>{@link PropertyType#INT}</li>
 * <li>{@link PropertyType#LONG}</li>
 * <li>{@link PropertyType#STRING}</li>
 * </ul>
 * 
 * @author MS025633
 *
 */
public class PrimitiveInputParameter implements InputParameter {

	private static final Logger logger = LogManager.getLogger(PrimitiveInputParameter.class);

	private static final List<PropertyType> PRIMITIVE_TYPES;

	static {
		PRIMITIVE_TYPES = new ArrayList<>();
		PRIMITIVE_TYPES.add(PropertyType.BOOLEAN);
		PRIMITIVE_TYPES.add(PropertyType.DOUBLE);
		PRIMITIVE_TYPES.add(PropertyType.FLOAT);
		PRIMITIVE_TYPES.add(PropertyType.INT);
		PRIMITIVE_TYPES.add(PropertyType.LONG);
		PRIMITIVE_TYPES.add(PropertyType.STRING);
	}

	private final Class<?> primitiveClass;
	private final Object primitiveValue;

	/**
	 * Determines if the type provided is a valid primitive property type or
	 * not.
	 * 
	 * @param type
	 *            The type to check.
	 * @return If a primitive type.
	 */
	public static final boolean isPrimitivePropertyType(PropertyType type) {
		checkArgument(type != null, "The primitive type cannot be null.");
		return PRIMITIVE_TYPES.contains(type);
	}

	/**
	 * Creates a new input primitive parameter based on the supplied values.
	 * 
	 * @param type
	 *            The primitive type
	 * @param parameterString
	 *            The parameter string value.
	 * @throws InvalidObjectException
	 *             If the parameter string cannot be converted into its
	 *             corresponding type.
	 * @throws IllegalArgumentException
	 *             If type is null, or not supported for primitive conversion.
	 */
	public PrimitiveInputParameter(PropertyType type, String parameterString) throws InvalidObjectException {
		checkArgument(type != null, "The primitive type cannot be null.");
		checkArgument(isPrimitivePropertyType(type),
				"The property type '" + type + "' provided is not supported for primitive conversion.");
		logger.debug("Creating new primitive parameter with type '" + type + "' and value '" + parameterString + "'.");
		primitiveClass = getClassType(type);
		primitiveValue = toObject(type, parameterString);
	}

	@Override
	public Class<?> getParameterClass() {
		return primitiveClass;
	}

	@Override
	public Object getParameterValue() {
		return primitiveValue;
	}

	private Object toObject(PropertyType type, String value) throws InvalidObjectException {
		if (value == null) {
			return null;
		}
		switch (type) {
		case BOOLEAN:
			return Boolean.valueOf(value);
		case INT:
			try {
				return Integer.valueOf(value);
			} catch (NumberFormatException e) {
				throw new InvalidObjectException("Unable to convert '" + value + "' to an Integer.");
			}
		case DOUBLE:
			try {
				return Double.valueOf(value);
			} catch (NumberFormatException e) {
				throw new InvalidObjectException("Unable to convert '" + value + "' to a Double.");
			}
		case FLOAT:
			try {
				return Float.valueOf(value);
			} catch (NumberFormatException e) {
				throw new InvalidObjectException("Unable to convert '" + value + "' to a Float.");
			}
		case LONG:
			try {
				return Long.valueOf(value);
			} catch (NumberFormatException e) {
				throw new InvalidObjectException("Unable to convert '" + value + "' to a Long.");
			}
		case STRING:
			return value;
		default:
			throw new IllegalStateException("The parameter type '" + type + "' cannot be mapped to a primitive class.");
		}
	}

	private Class<?> getClassType(PropertyType type) {
		switch (type) {
		case BOOLEAN:
			return Boolean.TYPE;
		case INT:
			return Integer.TYPE;
		case DOUBLE:
			return Double.TYPE;
		case FLOAT:
			return Float.TYPE;
		case LONG:
			return Long.TYPE;
		case STRING:
			return String.class;
		default:
			throw new IllegalStateException("The parameter type '" + type + "' cannot be mapped to a primitive class.");
		}
	}

}
