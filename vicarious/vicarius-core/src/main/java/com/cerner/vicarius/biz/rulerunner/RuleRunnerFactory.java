package com.cerner.vicarius.biz.rulerunner;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.List;
import java.util.Map;

import com.cerner.vicarius.data.model.Rule;
import com.google.common.collect.Maps;

/**
 * A factory class to generate a {@link RuleRunner} provided a {@link Rule}.
 *
 * @author MS025633
 *
 */
public class RuleRunnerFactory {

	private final Map<Long, RuleRunner> ruleRunnerMap;

	/**
	 * Creates a new instance of an empty factory.
	 */
	public RuleRunnerFactory() {
		ruleRunnerMap = Maps.newHashMap();
	}

	/**
	 * Creates a new factory and prepopulates it with a set of already made rule
	 * runners.
	 *
	 * @param ruleRunners
	 *            The rule runners to populate the factory with.
	 * @throws IllegalArgumentException
	 *             If the list of runners is null or contains a null.
	 */
	public RuleRunnerFactory(List<RuleRunner> ruleRunners) {
		this();
		checkArgument(ruleRunners != null, "The rule runners list cannot be null.");
		checkArgument(!ruleRunners.contains(null), "The rule runners list cannot contain a null.");
		for (RuleRunner runner : ruleRunners) {
			ruleRunnerMap.put(runner.getRuleId(), runner);
		}
	}

	/**
	 * Gets a runner for a corresponding rule.
	 *
	 * @param rule
	 *            The rule.
	 * @return The runner.
	 * @throws IllegalArgumentException
	 *             If the rule is null.
	 */
	public RuleRunner getRuleRunner(Long ruleId) {
		checkArgument(ruleId != null, "The rule cannot be null.");
		RuleRunner runner = ruleRunnerMap.get(ruleId);
		if (runner == null) {
			synchronized (ruleRunnerMap) {
				if (runner == null) {
					runner = new RuleRunner(ruleId);
					ruleRunnerMap.put(ruleId, runner);
				}
			}
		}
		return runner;
	}
}
