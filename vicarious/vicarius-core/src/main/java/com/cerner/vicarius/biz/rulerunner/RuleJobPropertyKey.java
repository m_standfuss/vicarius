package com.cerner.vicarius.biz.rulerunner;

/**
 * A unique key that can be mapped to a unique job property based on the rule
 * job identifier of the rule job and the property name.
 * 
 * @author MS025633
 *
 */
public class RuleJobPropertyKey {

	private final Long ruleJobId;
	private final String propertyName;

	/**
	 * Creates a new rule job property key.
	 * 
	 * @param ruleJobId
	 *            The rule job identifier.
	 * @param propertyName
	 *            The property name.
	 */
	public RuleJobPropertyKey(Long ruleJobId, String propertyName) {
		this.ruleJobId = ruleJobId;
		this.propertyName = propertyName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((propertyName == null) ? 0 : propertyName.hashCode());
		result = prime * result + ((ruleJobId == null) ? 0 : ruleJobId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		RuleJobPropertyKey other = (RuleJobPropertyKey) obj;
		if (propertyName == null) {
			if (other.propertyName != null) {
				return false;
			}
		} else if (!propertyName.equals(other.propertyName)) {
			return false;
		}
		if (ruleJobId == null) {
			if (other.ruleJobId != null) {
				return false;
			}
		} else if (!ruleJobId.equals(other.ruleJobId)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "RuleJobPropertyKey [ruleJobId=" + ruleJobId + ", propertyName=" + propertyName + "]";
	}
}
