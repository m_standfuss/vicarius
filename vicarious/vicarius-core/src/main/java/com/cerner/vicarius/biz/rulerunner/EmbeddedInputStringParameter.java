package com.cerner.vicarius.biz.rulerunner;

import static com.google.common.base.Preconditions.checkArgument;

import java.io.InvalidObjectException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * An input parameter String instance that has other parameters embedded into
 * its value.</br>
 * </br>
 * The expected JSON format of an embedded input string parameter is:
 * 
 * <pre>
 *{
 *  "value":"The full string with @embeddedParameter1  embedded tags",
 *  "properties":[
 *    {
 *      "embeddedPropertyName":"embeddedParameter1",
 *      "ruleJobId":123,
 *      "parameterName":"jobsParamName"
 *    }
 *  ]
 *}
 * </pre>
 * 
 * @author MS025633
 *
 */
public class EmbeddedInputStringParameter implements InputParameter {

	private static final Logger logger = LogManager.getLogger(EmbeddedInputStringParameter.class);

	private static final char ESCAPE_CHAR = '/';
	private static final String PROPERTY_ANNOTATION = "@";
	private static final String REGEX_PROPERTY = "((?<!" + ESCAPE_CHAR + ")" + PROPERTY_ANNOTATION + "\\w*) ";

	private static final Pattern PATTERN_PROPERTY = Pattern.compile(REGEX_PROPERTY);

	private static final String PROP_VALUE = "value";
	private static final String PROP_PROPERTIES = "properties";
	private static final String PROP_EMBEDDED_PROP_NAME = "embeddedPropertyName";
	private static final String PROP_RULE_JOB_ID = "ruleJobId";
	private static final String PROP_PARAMETER_NAME = "parameterName";

	private final String value;

	/**
	 * Creates a new embedded input parameter based on the json object, and
	 * using the output map as values to inject.
	 * 
	 * @param content
	 *            The json content of the parameter.
	 * @param outputMap
	 *            The output map.
	 * @throws InvalidObjectException
	 *             If the supplied parameters cannot be parsed into a parameter.
	 * @throws IllegalArgumentException
	 *             If the json object or output map is null.
	 */
	public EmbeddedInputStringParameter(String content, Map<RuleJobPropertyKey, Object> outputMap)
			throws InvalidObjectException {
		checkArgument(content != null, "The json object cannot be null.");
		checkArgument(outputMap != null, "The output map cannot be null.");
		value = parseParameter(content, outputMap);
	}

	@Override
	public Class<?> getParameterClass() {
		return String.class;
	}

	@Override
	public Object getParameterValue() {
		return value;
	}

	private String parseParameter(String content, Map<RuleJobPropertyKey, Object> outputMap)
			throws InvalidObjectException {
		logger.debug("Starting parse of json '" + content + "'.");

		JsonObject jsonObj = new JsonParser().parse(content).getAsJsonObject();
		validateMemberExists(jsonObj, PROP_VALUE);
		validateMemberExists(jsonObj, PROP_PROPERTIES);

		String value = jsonObj.get(PROP_VALUE).getAsString();
		JsonArray propArray = jsonObj.getAsJsonArray(PROP_PROPERTIES);

		Map<String, Object> parameterNameToInstance = new HashMap<>();
		for (JsonElement embeddedProp : propArray) {

			logger.debug("Creating rule job property key for json '" + embeddedProp + "'.");
			if (!embeddedProp.isJsonObject()) {
				logger.error("Expecting the embedded property to be a json object.");
				continue;
			}

			JsonObject embeddedPropObj = embeddedProp.getAsJsonObject();
			if (!embeddedPropObj.has(PROP_EMBEDDED_PROP_NAME) || !embeddedPropObj.has(PROP_RULE_JOB_ID)
					|| !embeddedPropObj.has(PROP_PARAMETER_NAME)) {
				continue;
			}

			Long ruleJobId = embeddedPropObj.get(PROP_RULE_JOB_ID).getAsLong();
			String propertyName = embeddedPropObj.get(PROP_PARAMETER_NAME).getAsString();
			RuleJobPropertyKey key = new RuleJobPropertyKey(ruleJobId, propertyName);

			String embeddedName = embeddedPropObj.get(PROP_EMBEDDED_PROP_NAME).getAsString();

			Object outputParameter = outputMap.get(key);
			parameterNameToInstance.put(embeddedName, outputParameter);
		}

		Matcher matcher;
		while ((matcher = PATTERN_PROPERTY.matcher(value)).find()) {
			int beginIndex = matcher.start();
			int endIndex = matcher.end();
			String parameterName = matcher.group(1);
			Object parameterInstance = parameterNameToInstance.get(parameterName);
			String embeddedParameterValue = parameterInstance == null ? null : parameterInstance.toString();

			logger.debug("Replacing '" + parameterName + "' with '" + embeddedParameterValue + "'.");
			value = value.substring(0, beginIndex) + embeddedParameterValue + value.substring(endIndex);
		}

		value = value.replaceAll("/@", "@");
		logger.debug("New string value after replacement '" + value + "'.");
		return value;
	}

	private void validateMemberExists(JsonObject obj, String memberName) {
		if (!obj.has(memberName)) {
			throw new IllegalStateException(
					"The json provided '" + obj + "', is missing the '" + memberName + "' member.");
		}
	}

}
