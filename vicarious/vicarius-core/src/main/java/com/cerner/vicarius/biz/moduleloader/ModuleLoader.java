package com.cerner.vicarius.biz.moduleloader;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.cerner.vicarius.Application;
import com.cerner.vicarius.data.access.ModuleDao;
import com.cerner.vicarius.data.model.Module;
import com.cerner.vicarius.module.ModuleSpec;
import com.google.common.collect.Maps;

/**
 * A module loader, can handle starting and stopping modules.
 *
 * @author MS025633
 */
public class ModuleLoader {
	
	private static final Logger logger = LogManager.getLogger(ModuleLoader.class);
	
	private static final String DEFAULT_MODULE_DIR = "modules";
	
	private final ModuleDao moduleDao;
	
	private final Map<Long, ModuleSpec> moduleMap;
	
	private String moduleDirectory = DEFAULT_MODULE_DIR;
	
	/**
	 * Creates a new module loader.
	 */
	public ModuleLoader() {
		this(new ModuleDao());
	}
	
	/**
	 * Creates a new module loader with the supplied dependencies.
	 *
	 * @param moduleDao
	 *            The module data access object to use.
	 * @throws IllegalArgumentException
	 *             If the module data access is null.
	 */
	public ModuleLoader(ModuleDao moduleDao) {
		checkArgument(moduleDao != null, "The module data access object cannot be null.");
		this.moduleDao = moduleDao;
		moduleMap = Maps.newHashMap();
	}
	
	/**
	 * Sets the location to look for modules to load, can a relative path or absolute.
	 * 
	 * @param directory
	 *            The directory to look for.
	 */
	public void setModuleDirectory(String directory) {
		moduleDirectory = directory;
	}
	
	/**
	 * Loads the active modules into a map and returns it. The instances will be held in an internal map for later
	 * stopping. A module loaded from a {@link ModuleLoader} should never be stopped manually, instead the
	 * {@link #stopModule(Long)} or {@link #stopAllModules()} should be used.
	 *
	 * @return A map from module identifier to loaded module instance.
	 * @throws IllegalStateException
	 *             If modules have already previously been loaded.
	 */
	public Map<Long, ModuleSpec> loadActiveModules() {
		logger.debug("Starting all active modules.");
		checkState(moduleMap.keySet().isEmpty(),
				"Previous modules have been loaded, must be shutdown prior to starting.");
		List<Module> activeModules = moduleDao.findActive();
		activeModules.forEach(m -> loadModule(m));
		return Maps.newHashMap(moduleMap);
	}
	
	/**
	 * Loads a specific module and returns it. The instance will be held internally as well for later stopping. A module
	 * loaded from a {@link ModuleLoader} should never be stopped
	 * manually, instead the {@link #stopModule(Long)} or {@link #stopAllModules()} should be used.
	 *
	 * @param moduleId
	 *            The identifier of the module to load.
	 * @return The loaded instance of the module. Null if the module could not be loaded.
	 * @throws IllegalArgumentException
	 *             If the module id is null.
	 */
	public ModuleSpec loadModule(Long moduleId) {
		logger.debug("Starting module with id " + moduleId);
		checkState(!moduleMap.containsKey(moduleId), "The module has already been started.");
		Module module = moduleDao.findById(moduleId);
		if (module == null) {
			return null;
		}
		return loadModule(module);
	}
	
	/**
	 * Stops a module and removes its instance from the loader. This will only stop modules that were successfully
	 * started via the {@link #loadModule(Long)} or {@link #loadActiveModules()} methods.
	 */
	public void stopAllModules() {
		logger.debug("Stopping all modules.");
		moduleMap.values().forEach(ms -> stopModule(ms));
		moduleMap.clear();
	}
	
	/**
	 * Stops a module and removes its instance from the loader. This will only stop a module if it was successfully
	 * loaded view the {@link #loadModule(Long)} or {@link #loadActiveModules()} methods.
	 *
	 * @param moduleId
	 *            The module's identifier to stop.
	 * @throw IllegalArgumentException If the module identifier supplied was not recognized.
	 */
	public void stopModule(Long moduleId) {
		logger.debug("Stopping module with id = " + moduleId);
		checkArgument(moduleMap.containsKey(moduleId), "The module identifier was not recognized.");
		
		stopModule(moduleMap.get(moduleId));
		moduleMap.remove(moduleId);
	}
	
	private ModuleSpec loadModule(Module module) {
		ClassLoader classLoader;
		try {
			classLoader = getClassLoader(module);
		} catch (FileNotFoundException e) {
			logger.error("The module's jar file could not be found. Unable to start the '" + module.getDisplay()
					+ "' module.", e);
			return null;
		}
		
		ModuleSpec spec;
		try {
			spec = createModuleSpec(classLoader, module);
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
			logger.error("Unable to instantiate the module '" + module.getDisplay() + "'.", e);
			return null;
		}
		
		try {
			spec.startup();
		} catch (Throwable t) {
			logger.error("Exception caught while attempting to startup module '" + spec.getDisplay() + "'.", t);
			return null;
		}
		moduleMap.put(module.getModuleId(), spec);
		return spec;
	}
	
	private void stopModule(ModuleSpec module) {
		try {
			module.shutdown();
		} catch (Throwable t) {
			logger.error("An exception was caught while attempting to shutdown module '" + module.getDisplay(), t);
		}
	}
	
	private ClassLoader getClassLoader(Module module) throws FileNotFoundException {
		/*
		 * Each module will have a one to one relationship with a class loader. This will ensure that dependency
		 * collision
		 * does not occur between modules.
		 */
		ClassLoader loader;
		if (module.getJarFile() == null) {
			/*
			 * If no external jar file is specified assume the system's default class loader.
			 */
			loader = ClassLoader.getSystemClassLoader();
		} else {
			File jarFile = new File(moduleDirectory + File.separatorChar + module.getJarFile());
			if (!jarFile.exists()) {
				throw new FileNotFoundException(
						"Unable to find external module's jar file '" + jarFile.getAbsolutePath() + "'.");
			}
			URL url;
			try {
				url = jarFile.toURI().toURL();
			} catch (MalformedURLException e) {
				throw new RuntimeException("Unable to load external module's jar file", e);
			}
			loader = new URLClassLoader(new URL[] { url });
		}
		return loader;
	}
	
	private ModuleSpec createModuleSpec(ClassLoader cl, Module m)
			throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		Class<?> clazz = cl.loadClass(m.getClassName());
		if (!ModuleSpec.class.isAssignableFrom(clazz)) {
			throw new InstantiationException("The '" + m.getClassName() + "' class does not extend the Module class.");
		}
		ModuleSpec spec = (ModuleSpec) clazz.newInstance();
		spec.setDisplay(m.getDisplay());
		spec.setPropertySupplier((propName) -> Application.getProperty(spec.getClass(), propName));
		return spec;
	}
}
