package com.cerner.vicarius.biz.rules;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.cerner.vicarius.data.access.RuleDao;
import com.cerner.vicarius.data.model.Rule;
import com.google.common.collect.Lists;

/**
 * Delegating class to handle rule logic.
 *
 * @author MS025633
 */
public class RuleDelegate {

	private static final Logger logger = LogManager.getLogger(RuleDelegate.class);

	private final RuleDao ruleDao;

	/**
	 * Creates a new instance.
	 */
	public RuleDelegate() {
		this(new RuleDao());
	}

	/**
	 * Creates a new instance with the supplied dependencies.
	 *
	 * @param ruleDao
	 *            The rule data access object.
	 * @throws IllegalArgumentException
	 *             If any parameter is null.
	 */
	public RuleDelegate(RuleDao ruleDao) {
		checkArgument(ruleDao != null, "The rule data access object cannot be null.");
		this.ruleDao = ruleDao;
	}

	/**
	 * Gets all currently active rules.
	 * 
	 * @return The list of rules.
	 */
	public List<RuleRecord> getActiveRules() {
		logger.debug("Starting getActiveRules.");
		List<Rule> rule = ruleDao.findActive();
		List<RuleRecord> records = Lists.newArrayList();
		rule.forEach(r -> records.add(makeRuleRecord(r)));
		return records;
	}

	private RuleRecord makeRuleRecord(Rule rule) {
		RuleRecord record = new RuleRecord();
		record.setOffset(rule.getOffset());
		record.setPriority(rule.getPriority());
		record.setRuleId(rule.getRuleId());
		record.setLastCompletedOn(rule.getLastCompleted());
		return record;
	}
}
