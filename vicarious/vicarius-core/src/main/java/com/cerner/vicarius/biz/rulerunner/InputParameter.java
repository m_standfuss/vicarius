package com.cerner.vicarius.biz.rulerunner;

/**
 * Defines a input parameter
 * 
 * @author MS025633
 *
 */
public interface InputParameter {

	/**
	 * Gets the class of the parameter.
	 * 
	 * @return The class
	 */
	public Class<?> getParameterClass();

	/**
	 * Gets the value of the parameter.
	 * 
	 * @return The parameter
	 */
	public Object getParameterValue();
}
