package com.cerner.vicarius.biz.rules;

import java.util.Date;

import com.cerner.vicarius.data.model.Priority;

public class RuleRecord {

	private Long ruleId;
	private Date lastCompletedOn;
	private Priority priority;
	private Long offset;
	
	public Long getRuleId() {
		return ruleId;
	}
	
	public void setRuleId(Long ruleId) {
		this.ruleId = ruleId;
	}
	
	public Date getLastCompletedOn() {
		return lastCompletedOn;
	}
	
	public void setLastCompletedOn(Date lastCompletedOn) {
		this.lastCompletedOn = lastCompletedOn;
	}
	
	public Priority getPriority() {
		return priority;
	}
	
	public void setPriority(Priority priority) {
		this.priority = priority;
	}
	
	public Long getOffset() {
		return offset;
	}
	
	public void setOffset(Long offset) {
		this.offset = offset;
	}

}
