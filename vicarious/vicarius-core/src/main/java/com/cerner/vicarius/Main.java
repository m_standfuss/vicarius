package com.cerner.vicarius;

import java.io.IOException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * The main class that handles command line arguments and delegating actions.
 *
 * @author MS025633
 */
public class Main {

	private static final Logger logger = LogManager.getLogger(Main.class);

	private static final int EXIT_SUCCESSFUL_TERMINATION = 0;
	private static final int EXIT_COMMAND_ARGS = 1;
	private static final int EXIT_EXCEPTION = 2;

	private static final String CMD_LINE_SYNTAX = "java -jar support-bot.jar";

	private static final String OPT_HELP = "help";
	private static final String OPT_START = "start";
	private static final String OPT_STOP = "stop";

	private static final String OPT_START_DESC = "To start the application.";
	private static final String OPT_STOP_DESC = "To stop the application.";
	private static final String OPT_HELP_DESC = "Shows help menu.";

	private boolean showHelp;
	private boolean startApplication;
	private boolean stopApplication;

	public static void main(String[] args) {
		new Main().run(args);
		System.exit(EXIT_SUCCESSFUL_TERMINATION);
	}

	private void run(String[] args) {
		logger.debug("Starting run.");
		Options options = getCommandLineOptions();

		try {
			parseArguments(args, options);
		} catch (ParseException e) {
			logger.error("Unable to parse the command line arguments terminating.", e);
			System.exit(EXIT_COMMAND_ARGS);
			return;
		}

		if (!validateArguments()) {
			System.exit(EXIT_COMMAND_ARGS);
			return;
		}

		if (showHelp) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(CMD_LINE_SYNTAX, options);
			return;
		}

		Application application = new Application();
		if (startApplication) {
			try {
				application.start();
			} catch (IOException e) {
				logger.error("Unable to start the application.", e);
				System.exit(EXIT_EXCEPTION);
				return;
			}
		} else if (stopApplication) {
			try {
				application.stop();
			} catch (IOException | InterruptedException e) {
				logger.error("Unable to stop the application.", e);
				System.exit(EXIT_EXCEPTION);
				return;
			}
		}
	}

	private Options getCommandLineOptions() {
		Options options = new Options();
		options.addOption(OPT_START, false, OPT_START_DESC);
		options.addOption(OPT_STOP, false, OPT_STOP_DESC);
		options.addOption(OPT_HELP, false, OPT_HELP_DESC);
		return options;
	}

	private void parseArguments(String[] args, Options options) throws ParseException {
		CommandLineParser parser = new DefaultParser();
		CommandLine line = parser.parse(options, args);
		startApplication = line.hasOption(OPT_START);
		stopApplication = line.hasOption(OPT_STOP);
		showHelp = line.hasOption(OPT_HELP);
	}

	private boolean validateArguments() {
		int argCnt = 0;
		argCnt += startApplication ? 1 : 0;
		argCnt += stopApplication ? 1 : 0;
		argCnt += showHelp ? 1 : 0;

		if (argCnt == 0) {
			logger.error("No command options were specified.");
			return false;
		}
		if (argCnt > 1) {
			logger.error("Multiple command options were specified.");
			return false;
		}
		return true;
	}

}
