package com.cerner.vicarius.biz.rulerunner;

import static com.google.common.base.Preconditions.checkArgument;

import java.io.InvalidObjectException;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.cerner.vicarius.data.model.PropertyType;
import com.cerner.vicarius.data.model.RuleJobParameter;

/**
 * A input parameter factory. Takes in the rule job parameter (as well as any
 * extra context needed) of the input parameter and returns the corresponding
 * {@link InputParameter}.
 * 
 * @author MS025633
 *
 */
public class InputParameterFactory {

	private static final Logger logger = LogManager.getLogger(InputParameterFactory.class);

	/**
	 * Parses the content into the corresponding {@link InputParameter}.
	 * 
	 * @param type
	 *            The property type of the parameter.
	 * @param element
	 *            The json element representing the parameter.
	 * @param outputMap
	 *            The current content of the output map.
	 * @return The corresponding input parameter.
	 * @throws IllegalArgumentException
	 *             If any of the parameter supplied was null.
	 * @throws InvalidObjectException
	 *             If the object cannot be created based on the supplied
	 *             parameters.
	 */
	public InputParameter parseInputParameter(PropertyType type, RuleJobParameter parameter,
			Map<RuleJobPropertyKey, Object> outputMap) throws InvalidObjectException {
		checkArgument(type != null, "The property type cannot be null.");
		checkArgument(parameter != null, "The rule job parameter cannot be null.");
		checkArgument(outputMap != null, "The output map cannot be null.");
		logger.debug("Parsing input parameter " + parameter + ".");

		switch (parameter.getType()) {
		case PRIMITIVE:
			return new PrimitiveInputParameter(type, parameter.getContent());
		case OUTPUT_PARAMETER:
			return new InputFromOutputParameter(parameter.getContent(), outputMap);
		case EMBEDDED_STRING:
			return new EmbeddedInputStringParameter(parameter.getContent(), outputMap);
		default:
			return new EmptyInputParameter();
		}
	}
}
