package com.cerner.vicarius.runtime;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.Lists;

public class CommandHandler {
	private static final Logger logger = LogManager.getLogger(CommandHandler.class);

	private static final Charset ENCODING = StandardCharsets.UTF_8;

	private final Runtime runtime;
	private List<String> outputStrings;
	private List<String> errorStrings;

	/**
	 * Initializes a new CommandHandler object. The Runtime will be defaulted to
	 * the {@link Runtime#getRuntime()}
	 */
	public CommandHandler() {
		runtime = Runtime.getRuntime();
	}

	/**
	 * Initializes a new CommandHandler with a specific Runtime object.
	 *
	 * @param runtime
	 *            The Runtime to run commands against.
	 */
	public CommandHandler(Runtime runtime) {
		this.runtime = runtime;
	}

	/**
	 * Gets the output of the last command as a list of strings for each line.
	 *
	 * @return The list of output strings from the command's input stream. Will
	 *         be empty list if command has not been ran.
	 */
	public List<String> getOutputStrings() {
		if (outputStrings == null) {
			return Lists.newArrayList();
		}
		return outputStrings;
	}

	/**
	 * Gets the errors of the last command as a list of strings for each line.
	 *
	 * @return The list of error strings from the command's error stream. Will
	 *         be empty list if command has not been ran.
	 */
	public List<String> getErrorStrings() {
		if (errorStrings == null) {
			return Lists.newArrayList();
		}
		return errorStrings;
	}

	/**
	 * Runs the given command against the runtime saving off the input and error
	 * streams for use later.
	 *
	 * @param command
	 *            The command to run.
	 * @throws IllegalArgumentException
	 *             If the command string is null.
	 * @throws IOException
	 *             If there is a problem running the command.
	 * @throws InterruptedException
	 *             If the process is terminated early before an exit code can be
	 *             returned.
	 */
	public int runCommand(String command) throws IOException, InterruptedException {
		if (command == null) {
			throw new IllegalArgumentException("The command cannot be null.");
		}

		logger.debug("Running the command '" + command + "'.");

		Process p = runtime.exec(command);

		outputStrings = IOUtils.readLines(p.getInputStream(), ENCODING);
		errorStrings = IOUtils.readLines(p.getErrorStream(), ENCODING);

		p.waitFor();
		return p.exitValue();
	}
}
