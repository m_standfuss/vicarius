package com.cerner.vicarius.biz.rulerunner;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * An input parameter that gets its value assigned from a previous output.</br>
 * The expected JSON format of an embedded input string parameter is:
 * 
 * <pre>
 *{
 *  "ruleJobId":123,
 *  "parameterName":"jobsParamName"
 *}
 * </pre>
 * 
 * @author MS025633
 *
 */
public class InputFromOutputParameter implements InputParameter {

	private static final Logger logger = LogManager.getLogger(InputFromOutputParameter.class);

	private static final String RULE_JOB_ID = "ruleJobId";
	private static final String PARAMETER_NAME = "parameterName";

	private final Object property;

	/**
	 * Creates new new input parameter based on an output parameter from a
	 * previous job execution.
	 * 
	 * @param obj
	 *            The json object defining this parameter.
	 * @param outputMap
	 *            The current output parameter map.
	 */
	public InputFromOutputParameter(String content, Map<RuleJobPropertyKey, Object> outputMap) {
		checkArgument(content != null, "The json content cannot be null.");
		checkArgument(outputMap != null, "The output map cannot be null.");
		property = parseParameter(content, outputMap);
	}

	@Override
	public Class<?> getParameterClass() {
		return property == null ? null : property.getClass();
	}

	@Override
	public Object getParameterValue() {
		return property;
	}

	private Object parseParameter(String content, Map<RuleJobPropertyKey, Object> outputMap) {
		logger.debug("Starting parse of parameter '" + content + "'.");
		JsonObject obj = new JsonParser().parse(content).getAsJsonObject();

		validateMemberExists(obj, RULE_JOB_ID);
		Long ruleJobId = obj.get(RULE_JOB_ID).getAsLong();

		validateMemberExists(obj, PARAMETER_NAME);
		String propertyName = obj.get(PARAMETER_NAME).getAsString();

		RuleJobPropertyKey key = new RuleJobPropertyKey(ruleJobId, propertyName);
		logger.debug("Getting value of output for property key " + key);
		return outputMap.get(key);
	}

	private void validateMemberExists(JsonObject obj, String memberName) {
		if (!obj.has(memberName)) {
			throw new IllegalStateException(
					"The json provided '" + obj + "', is missing the '" + memberName + "' member.");
		}
	}
}
