package com.cerner.vicarius.runtime;

public enum OSType {
	WIN, MAC, UNIX;

	/**
	 * Gets the operating system type for the device running on. This makes use
	 * of the {@link System#getProperty(String)} for the os.name.
	 *
	 * @return The os type of the device.
	 */
	public static OSType getType() {
		String osName = System.getProperty("os.name").toUpperCase();
		if (osName.contains("NIX") || osName.contains("NUX") || osName.contains("AIX")) {
			return UNIX;
		} else if (osName.contains("MAC")) {
			return MAC;
		} else if (osName.contains("WIN")) {
			return WIN;
		} else {
			throw new IllegalStateException("The operating system was not recognized.");
		}
	}
}
