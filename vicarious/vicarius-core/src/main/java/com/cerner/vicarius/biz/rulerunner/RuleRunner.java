package com.cerner.vicarius.biz.rulerunner;

import static com.google.common.base.Preconditions.checkArgument;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.cerner.vicarius.biz.EntityMap;
import com.cerner.vicarius.data.access.JobDao;
import com.cerner.vicarius.data.access.JobPropertyDao;
import com.cerner.vicarius.data.access.RuleDao;
import com.cerner.vicarius.data.access.RuleJobDao;
import com.cerner.vicarius.data.access.RuleJobParameterDao;
import com.cerner.vicarius.data.access.RuleJobSaveDao;
import com.cerner.vicarius.data.model.Job;
import com.cerner.vicarius.data.model.JobProperty;
import com.cerner.vicarius.data.model.PropertyIOType;
import com.cerner.vicarius.data.model.Rule;
import com.cerner.vicarius.data.model.RuleJob;
import com.cerner.vicarius.data.model.RuleJobParameter;
import com.cerner.vicarius.data.model.RuleJobSave;
import com.cerner.vicarius.module.ActionJob;
import com.cerner.vicarius.module.BaselineJob;
import com.cerner.vicarius.module.ConditionalJob;
import com.cerner.vicarius.module.JobSpec;
import com.cerner.vicarius.module.ModuleSpec;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;

/**
 * A rule runner that accepts a rule and attempts to run the jobs assigned to
 * it. If a conditional job returns false then the rule will be terminated prior
 * to running any actions.
 *
 * @author MS025633
 */
public class RuleRunner {

	private static final Logger logger = LogManager.getLogger(RuleRunner.class);

	private static Map<Long, ModuleSpec> moduleSpecMap = Maps.newHashMap();

	private final JobDao jobDao;
	private final JobPropertyDao jobPropertyDao;
	private final RuleDao ruleDao;
	private final RuleJobDao ruleJobDao;
	private final RuleJobParameterDao ruleJobParameterDao;
	private final RuleJobSaveDao ruleJobSaveDao;

	private final long ruleId;

	private final InputParameterFactory inputParser;

	public static void setModuleSpecMap(Map<Long, ModuleSpec> moduleSpecMap) {
		RuleRunner.moduleSpecMap = moduleSpecMap == null ? Maps.newHashMap() : moduleSpecMap;
	}

	/**
	 * Creates a new rule runner for the specified rule.
	 *
	 * @param rule
	 *            The rule to run.
	 * @throws IllegalArgumentException
	 *             If the rule is null.
	 */
	public RuleRunner(long ruleId) {
		this(ruleId, new JobDao(), new JobPropertyDao(), new RuleDao(), new RuleJobDao(), new RuleJobSaveDao(),
				new RuleJobParameterDao());
	}

	/**
	 * Creates a new rule runner for the specified rule and to use the specified
	 * data access objects.
	 *
	 * @param ruleId
	 *            The id of the rule to run.
	 * @param jobDao
	 *            The job data access object.
	 * @param jobPropertyDao
	 *            The job property data access object.
	 * @param ruleDao
	 *            The rule data access object.
	 * @param ruleJobDao
	 *            The rule job data access object.
	 * @param ruleJobSaveDao
	 *            The rule job save data access object.
	 * @param ruleJobParameterDao
	 *            The rule job parameter data access object.
	 * 
	 * @throws IllegalArgumentException
	 *             If any of the parameters are null.
	 */
	public RuleRunner(long ruleId, JobDao jobDao, JobPropertyDao jobPropertyDao, RuleDao ruleDao, RuleJobDao ruleJobDao,
			RuleJobSaveDao ruleJobSaveDao, RuleJobParameterDao ruleJobParameterDao) {
		logger.debug("Creating a new rule runner for the rule " + ruleId);
		checkArgument(jobDao != null, "The job dao cannot be null.");
		checkArgument(jobPropertyDao != null, "The job property dao cannot be null.");
		checkArgument(ruleJobDao != null, "The rule job dao cannot be null.");
		checkArgument(ruleJobSaveDao != null, "the rule job save dao cannot be null.");
		checkArgument(ruleJobParameterDao != null, "the rule job parameter dao cannot be null.");

		this.ruleId = ruleId;
		this.jobDao = jobDao;
		this.jobPropertyDao = jobPropertyDao;
		this.ruleDao = ruleDao;
		this.ruleJobDao = ruleJobDao;
		this.ruleJobSaveDao = ruleJobSaveDao;
		this.ruleJobParameterDao = ruleJobParameterDao;
		inputParser = new InputParameterFactory();
	}

	/**
	 * Gets the rule id assigned to this rule runner.
	 *
	 * @return The rule id.
	 */
	public Long getRuleId() {
		return ruleId;
	}

	/**
	 * Runs the rule.
	 *
	 * @return If the rule is run to completion.
	 * @throws IllegalStateException
	 *             If anything goes wrong with the configuration of the job or
	 *             rules.
	 */
	public boolean run() {
		logger.debug("Starting run.");

		Rule rule = ruleDao.findById(ruleId);

		List<RuleJob> ruleJobs = getRuleJobs();

		Set<Long> ruleJobIds = Sets.newHashSet();
		Set<Long> jobIds = Sets.newHashSet();
		ruleJobs.forEach(rj -> {
			ruleJobIds.add(rj.getRuleJobId());
			jobIds.add(rj.getJobId());
		});

		List<JobProperty> jobProperties = getJobProperties(jobIds);
		EntityMap<Job> jobMap = getJobs(jobIds);
		Multimap<Long, RuleJobParameter> ruleJobIdToParameters = getRuleJobParameters(ruleJobIds);

		Multimap<Long, JobProperty> jobIdToInputProperties = makePropertyMap(PropertyIOType.INPUT, jobProperties);
		Multimap<Long, JobProperty> jobIdToOutputProperties = makePropertyMap(PropertyIOType.OUTPUT, jobProperties);

		validateRowsExist(rule, ruleJobs, jobMap);

		ruleJobs.sort(new RuleJobComparator(jobMap));

		/*
		 * Allow for baseline jobs to run their baseline if we have never ran
		 * this rule and at least one of its jobs implements the baseline job
		 * interface.
		 */
		boolean runBaselines = rule.getLastRan() == null && hasBaselineJobs(jobMap.convertToCollection());

		boolean completed = true;
		Map<Long, String> saveState = new HashMap<>();
		Map<RuleJobPropertyKey, Object> outputParametersMap = new HashMap<>();
		for (RuleJob ruleJob : ruleJobs) {
			Job job = jobMap.get(ruleJob.getJobId());
			JobSpec jobSpec = makeJobInstance(job);

			validateJob(job, jobSpec);

			if (runBaselines && !(jobSpec instanceof BaselineJob)) {
				// If we are on a baseline run and this job doesnt implement
				// baseline job skip it.
				continue;
			}

			jobSpec.setSaveStateSupplier(() -> getState(ruleJob.getRuleJobId()));
			jobSpec.setSaveStateConsumer(state -> saveState.put(ruleJob.getRuleJobId(), state));
			jobSpec.setLastCompletedSupplier(() -> rule.getLastCompleted());

			Collection<JobProperty> jobInputProperties = jobIdToInputProperties.get(job.getJobId());
			Collection<RuleJobParameter> ruleJobParameters = ruleJobIdToParameters.get(ruleJob.getRuleJobId());
			setInputJobProperties(jobSpec, ruleJobParameters, jobInputProperties, outputParametersMap);

			if (runBaselines) {
				((BaselineJob) jobSpec).runBaseline();
			} else if (job.getConditionInd()) {
				if (!((ConditionalJob) jobSpec).run()) {
					completed = false;
					break;
				}
			} else {
				((ActionJob) jobSpec).run();
			}

			Collection<JobProperty> jobOutputProps = jobIdToOutputProperties.get(job.getJobId());
			outputParametersMap.putAll(makeOutputProperties(ruleJob.getRuleJobId(), jobSpec, jobOutputProps));
		}

		Date now = new Date();
		rule.setLastRan(now);
		if (completed) {
			rule.setLastCompleted(now);
		}
		ruleDao.persist(rule);

		if (completed || runBaselines) {
			/*
			 * Allow for rule jobs to update their save status is the rule is
			 * ran to completion, or if running baselines for the first time.
			 */
			saveState.entrySet().forEach(e -> saveState(e.getValue(), e.getKey()));
		}
		return completed;
	}

	private void validateJob(Job job, JobSpec jobSpec) {
		if (job.getConditionInd() && !(jobSpec instanceof ConditionalJob)) {
			throw new RuntimeException(
					"The job class was marked as a conditional, but does not extend the ConditionalJob class.");
		} else if (!job.getConditionInd() && !(jobSpec instanceof ActionJob)) {
			throw new RuntimeException(
					"The job class was marked as an action, but does not extend the ActionJob class.");
		}
	}

	private void setInputJobProperties(JobSpec jobSpec, Collection<RuleJobParameter> ruleJobParameters,
			Collection<JobProperty> jobInputProps, Map<RuleJobPropertyKey, Object> outputPropertyMap) {
		if (jobInputProps.isEmpty()) {
			return;
		}

		Map<String, RuleJobParameter> propertyNameToParameter = Maps.newHashMap();
		for (RuleJobParameter ruleJobParameter : ruleJobParameters) {
			propertyNameToParameter.put(ruleJobParameter.getPropertyName(), ruleJobParameter);
		}

		for (JobProperty jp : jobInputProps) {
			RuleJobParameter ruleJobParameter = propertyNameToParameter.get(jp.getName());
			if (jp.isRequiredInd() && ruleJobParameter == null) {
				throw new RuntimeException("A required property '" + jp.getName() + "' is missing from the rule job.");
			}

			if (ruleJobParameter == null) {
				continue;
			}

			try {
				InputParameter inputParameter = inputParser.parseInputParameter(jp.getPropertyType(), ruleJobParameter,
						outputPropertyMap);
				Class<?> propertyClass = inputParameter.getParameterClass();
				Object propertyInstance = inputParameter.getParameterValue();
				Method method = getMethod(jobSpec.getClass(), jp.getMethodName(), propertyClass);
				method.invoke(jobSpec, propertyInstance);
			} catch (Throwable t) {
				if (jp.isRequiredInd()) {
					throw new RuntimeException("The job property '" + jp.getName()
							+ " was required but an exception occured when setting.", t);
				} else {
					logger.error("Unable to successfully set job property '" + jp.getName()
							+ "', but not a required field so continuing.", t);
				}
			}
		}
	}

	private Method getMethod(Class<?> clazz, String methodName, Class<?> parameterClass) throws NoSuchMethodException {
		if (parameterClass != null) {
			return clazz.getMethod(methodName, parameterClass);
		}
		/*
		 * If the parameter is null we still want to give the individual job a
		 * chance to accept/handle the null value, so we have to search for the
		 * method manually
		 */
		List<Method> methodPotentials = new ArrayList<>();
		for (Method method : clazz.getDeclaredMethods()) {
			if (method.getName().equals(methodName) && method.getParameterCount() == 1) {
				methodPotentials.add(method);
			}
		}
		if (methodPotentials.isEmpty()) {
			throw new NoSuchMethodException("The method '" + methodName + "' was not found in the job.");
		}
		if (methodPotentials.size() == 1) {
			return methodPotentials.get(0);
		}
		throw new NoSuchMethodException("Multiple methods for '" + methodName
				+ "' were found in the job when calling with a null parameter. If this job accepts a null parameter it's setting method name be unique.");
	}

	private Map<RuleJobPropertyKey, Object> makeOutputProperties(Long ruleJobId, JobSpec jobSpec,
			Collection<JobProperty> jobOutputProps) {
		Map<RuleJobPropertyKey, Object> outputMap = new HashMap<>();
		for (JobProperty jp : jobOutputProps) {
			RuleJobPropertyKey key = new RuleJobPropertyKey(ruleJobId, jp.getName());
			try {
				Method method = jobSpec.getClass().getMethod(jp.getMethodName());
				Object prop = method.invoke(jobSpec);
				outputMap.put(key, prop);
			} catch (Throwable t) {
				logger.error(
						"Unable to obtain the output parameter from the job. If later jobs depend on this value they will fail to execute.",
						t);
			}
		}
		return outputMap;
	}

	private void validateRowsExist(Rule rule, List<RuleJob> ruleJobs, EntityMap<Job> jobMap) {
		/*
		 * Validates that all the expected rule/module/job rows were
		 * successfully loaded from the database. Allows for checking once
		 * rather then in each place along the way.
		 */
		if (rule == null) {
			throw new RuntimeException(
					"The rule with rule id = '" + ruleId + " could not be found unable to run rule.");
		}
		for (RuleJob ruleJob : ruleJobs) {
			Job job = jobMap.get(ruleJob.getJobId());
			if (job == null) {
				throw new RuntimeException("Missing job with id=" + ruleJob.getJobId() + ", unable to run rule.");
			}

			ModuleSpec module = moduleSpecMap.get(job.getModuleId());
			if (module == null) {
				throw new RuntimeException(
						"Missing module with id=" + job.getModuleId() + ", check that it was successfully started up.");
			}
		}
	}

	private JobSpec makeJobInstance(Job job) {
		ModuleSpec module = moduleSpecMap.get(job.getModuleId());
		try {
			Class<?> jobClass = module.getClass().getClassLoader().loadClass(job.getClassName());
			return (JobSpec) jobClass.newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			throw new RuntimeException("Unable to instantiate job class.", e);
		}
	}

	private boolean hasBaselineJobs(Collection<Job> jobs) {
		for (Job job : jobs) {
			ModuleSpec module = moduleSpecMap.get(job.getModuleId());
			Class<?> jobClass;
			try {
				jobClass = module.getClass().getClassLoader().loadClass(job.getClassName());
			} catch (ClassNotFoundException e) {
				throw new RuntimeException("Unable to load class '" + job.getClassName() + "'.");
			}
			if (BaselineJob.class.isAssignableFrom(jobClass)) {
				return true;
			}
		}
		return false;
	}

	private List<RuleJob> getRuleJobs() {
		logger.debug("Starting getRuleJobs.");
		return ruleJobDao.findByRule(ruleId);
	}

	private EntityMap<Job> getJobs(Collection<Long> jobIds) {
		logger.debug("Starting getJobs.");
		List<Job> jobs = jobDao.findByIds(jobIds);
		return EntityMap.newEntityMap(jobs);
	}

	private Multimap<Long, RuleJobParameter> getRuleJobParameters(Set<Long> ruleJobIds) {
		List<RuleJobParameter> ruleJobParameters = ruleJobParameterDao.findByRuleJobs(ruleJobIds);
		Multimap<Long, RuleJobParameter> ruleJobIdToParameters = ArrayListMultimap.create();
		for (RuleJobParameter rjp : ruleJobParameters) {
			ruleJobIdToParameters.put(rjp.getRuleJobId(), rjp);
		}
		return ruleJobIdToParameters;
	}

	private List<JobProperty> getJobProperties(Collection<Long> jobIds) {
		logger.debug("Starting getJobProperty.");
		List<JobProperty> jobProperties = jobPropertyDao.findByJobIds(jobIds);
		return jobProperties;
	}

	private Multimap<Long, JobProperty> makePropertyMap(PropertyIOType type, List<JobProperty> properties) {
		logger.debug("Starting makePropertyMap for type=" + type);
		Multimap<Long, JobProperty> map = ArrayListMultimap.create();
		properties.forEach(jp -> {
			if (jp.getIOType() == type) {
				map.put(jp.getJobId(), jp);
			}
		});
		return map;
	}

	private String getState(long ruleJobId) {
		RuleJobSave state = ruleJobSaveDao.findMostRecentByRuleJobId(ruleJobId);
		if (state == null) {
			return null;
		}
		return state.getContent();
	}

	private void saveState(String state, long ruleJobId) {
		ruleJobSaveDao.deleteByRuleJob(ruleJobId);
		RuleJobSave ruleJobSave = new RuleJobSave();
		ruleJobSave.setRuleJobId(ruleJobId);
		ruleJobSave.setContent(state);
		ruleJobSaveDao.persist(ruleJobSave);
	}
}
