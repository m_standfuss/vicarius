package com.cerner.vicarius.biz.rulerunner;

/**
 * An empty input parameter, will return null for the parameter class and name.
 * 
 * @author MS025633
 *
 */
public class EmptyInputParameter implements InputParameter {
	@Override
	public Class<?> getParameterClass() {
		return null;
	}

	@Override
	public Object getParameterValue() {
		return null;
	}
}
