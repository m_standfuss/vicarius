package com.cerner.vicarius.runtime;

import static com.google.common.base.Preconditions.checkArgument;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;

public class ProcessHandler {

	private static final String KILL_PROCESS_UNIX = "kill -9 %d";
	private static final String KILL_PROCESS_WIN = "taskkill /PID %d /F";

	private static final String QUERY_PROCESS_UNIX = "ps -p %d";
	private static final String QUERY_PROCESS_WIN = "tasklist /fi \"pid eq %d\" | findstr %d";

	private final OSType osType;
	private final CommandHandler commandHandler;
	private final RuntimeMXBean runtimeBean;

	/**
	 * Creates a new instance of a process handler using default parameters.
	 */
	public ProcessHandler() {
		this(OSType.getType(), new CommandHandler(), ManagementFactory.getRuntimeMXBean());
	}

	/**
	 * Creates a new instance of a process handler with supplied dependencies.
	 *
	 * @param osType
	 *            The operating system to run the processes on.
	 * @param commandHandler
	 *            The command handler to use to run processes.
	 * @param runtimeBean
	 *            The runtime bean to use (to get the current java processes
	 *            id).
	 *
	 * @throws IllegalArgumentException
	 *             If any parameter is null.
	 */
	public ProcessHandler(OSType osType, CommandHandler commandHandler, RuntimeMXBean runtimeBean) {
		checkArgument(osType != null, "The osType cannot be null.");
		checkArgument(commandHandler != null, "The commandHandler cannot be null.");
		checkArgument(runtimeBean != null, "The runtimeBean cannot be null.");

		this.osType = osType;
		this.commandHandler = commandHandler;
		this.runtimeBean = runtimeBean;
	}

	/**
	 * Gets the process id of the current java process running.
	 *
	 * @return The process id.
	 */
	public int getCurrentProcessId() {
		String processString = runtimeBean.getName();
		if (!processString.contains("@")) {
			throw new IllegalStateException(
					"Expected the process name to be in a different format than '" + processString + "'.");
		}
		String[] split = processString.split("@");
		return Integer.valueOf(split[0]);
	}

	/**
	 * Determines if the process for the id is currently running.
	 *
	 * @param processId
	 *            The process id to check for.
	 * @return If the process is active.
	 * @throws IOException
	 *             While running the command.
	 * @throws InterruptedException
	 *             While running the command.
	 * @throws IllegalStateException
	 *             If the os type supplied is not expected.
	 */
	public boolean isActiveProcess(int processId) throws IOException, InterruptedException {
		String command;
		if (osType == OSType.MAC || osType == OSType.UNIX) {
			command = String.format(QUERY_PROCESS_UNIX, processId);
		} else if (osType == OSType.WIN) {
			command = String.format(QUERY_PROCESS_WIN, processId, processId);
		} else {
			throw new IllegalStateException("Unexpected operating system, unable to continue.");
		}
		int exitCode = commandHandler.runCommand(command);
		return exitCode == 0;
	}

	/**
	 * Kills the process for the supplied process id.
	 *
	 * @param processId
	 *            The process id to kill.
	 * @throws IOException
	 *             While running the command.
	 * @throws InterruptedException
	 *             While running the command.
	 * @throws IllegalStateException
	 *             If the os type supplied is not expected.
	 */
	public void killProcess(int processId) throws IOException, InterruptedException {
		String command;
		if (osType == OSType.MAC || osType == OSType.UNIX) {
			command = String.format(KILL_PROCESS_UNIX, processId);
		} else if (osType == OSType.WIN) {
			command = String.format(KILL_PROCESS_WIN, processId);
		} else {
			throw new IllegalStateException("Unexpected operating system, unable to continue.");
		}
		int exitCode = commandHandler.runCommand(command);
		if (exitCode != 0) {
			throw new IllegalStateException("Unable to kill process with pid " + processId);
		}
	}
}
