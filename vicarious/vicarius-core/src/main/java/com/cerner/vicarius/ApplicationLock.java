package com.cerner.vicarius;

import static com.google.common.base.Preconditions.checkArgument;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.cerner.vicarius.runtime.ProcessHandler;

/**
 * The application lock, handles obtaining the application lock, releasing the
 * application lock and terminating the current process that has the application
 * lock.</br>
 * </br>
 * This lock transcends instances of the application running to help ensure that
 * only a single instance of the application is running at a time.
 *
 * @author MS025633
 */
public class ApplicationLock {

	public static final String LOCK_FILE_LOCATION = System.getProperty("user.dir") + File.separator + "vicarius.loc";
	public static final File LOCK_FILE = new File(LOCK_FILE_LOCATION);
	
	/**
	 * The default amount of time (1 minute) to wait for a graceful shutdown to
	 * occur can be overwritten via the {@link #setGracefulShutdownPeriod(int)}
	 * method.
	 */
	public static final int DEFAULT_GRACEFUL_SHUTDOWN = 60000;
	
	private static final Logger logger = LogManager.getLogger(ApplicationLock.class);
	
	private static int gracefulShutdownPeriod = DEFAULT_GRACEFUL_SHUTDOWN;
	
	private final ProcessHandler processHandler;
	
	/**
	 * Cache the current process id since it never changes. This should only be
	 * accessed through its corresponding {@link #getCurrentProcessId()} method.
	 */
	private Integer processId;
	
	/**
	 * Sets how long this application should wait for any other instance of the
	 * application to gracefully shutdown before manually killing the process.
	 *
	 * @param gracefulShutdownPeriod
	 *            The amount of time in milliseconds to wait.
	 */
	public static void setGracefulShutdownPeriod(int gracefulShutdownPeriod) {
		ApplicationLock.gracefulShutdownPeriod = gracefulShutdownPeriod;
	}
	
	public static int getGracefulShutdownPeriod() {
		return ApplicationLock.gracefulShutdownPeriod;
	}
	
	public ApplicationLock() {
		processHandler = new ProcessHandler();
	}

	public ApplicationLock(ProcessHandler processHandler) {
		checkArgument(processHandler != null, "The processHandler should not be null.");
		this.processHandler = processHandler;
	}

	/**
	 * Attempts to obtain the application lock. Will return if the lock was
	 * successfully obtained or not.
	 *
	 * @return If the lock was obtained or not.
	 * @throws IOException
	 *             If something goes wrong dealing with the application lock
	 *             file.
	 */
	public boolean obtainLock() throws IOException {
		logger.debug("Starting obtain lock.");

		if (!LOCK_FILE.exists()) {
			logger.info("Attempting to claim the lock file for this application.");
			createLockFile();
			return true;
		}

		logger.info("A existing lock file already exists, checking its validity.");
		int processId = getLockedProcessId();

		boolean activeProcess;
		try {
			activeProcess = processHandler.isActiveProcess(processId);
		} catch (InterruptedException e) {
			logger.error("Unable to determine if current pid in lock file is active or not.", e);
			return false;
		}

		if (!activeProcess) {
			logger.info(
					"A terminated instance of the application currently holds the lock file, attempting to delete and reclaim.");
			deleteLockFile();
			createLockFile();
		}
		return !activeProcess;
	}

	/**
	 * Determines if the current process currently has the rights the the lock
	 * file on disk.
	 *
	 * @return If the application holds the lock.
	 * @throws IOException
	 *             If anything happens reading the lock file.
	 */
	public boolean isLocked() throws IOException {
		logger.debug("Starting is locked.");
		if (!LOCK_FILE.exists()) {
			logger.info("The lock file no longer exists.");
			return false;
		}
		
		return getLockedProcessId() == getCurrentProcessId();
	}
	
	/**
	 * Attempts to terminate the instance of the application that currently
	 * holds the application lock.
	 *
	 * @throws IOException
	 *             If anything goes wrong with the dealing of the application
	 *             lock file.
	 * @throws InterruptedException
	 *             If anything goes wrong when trying to terminate the
	 *             application.
	 */
	public void terminateLockedApplication() throws IOException, InterruptedException {
		logger.debug("Starting releaseLock.");
		if (!LOCK_FILE.exists()) {
			logger.info("No lock file could be found.");
			return;
		}
		int processId = getLockedProcessId();
		logger.info("Lock file found with pid of '" + processId + "'.");
		
		deleteLockFile();
		long startTime = new Date().getTime();
		while (processHandler.isActiveProcess(processId)) {
			long waitTime = new Date().getTime() - startTime;
			if (waitTime >= gracefulShutdownPeriod) {
				logger.info("Existing process was not terminated gracefully, killing process manually.");
				processHandler.killProcess(processId);
				break;
			}
		}
	}

	/**
	 * Deletes the application lock. This should only be called by the
	 * application that currently holds the lock right before termination.
	 */
	public void releaseLock() {
		deleteLockFile();
	}

	private int getLockedProcessId() throws IOException {
		String fileContents = FileUtils.readFileToString(LOCK_FILE, StandardCharsets.UTF_8);
		return Integer.parseInt(fileContents);
	}

	private void deleteLockFile() {
		LOCK_FILE.delete();
	}

	private void createLockFile() throws IOException {
		int currentPID = getCurrentProcessId();
		FileUtils.writeStringToFile(LOCK_FILE, String.valueOf(currentPID), StandardCharsets.UTF_8);
	}
	
	private int getCurrentProcessId() {
		if (processId == null) {
			processId = processHandler.getCurrentProcessId();
		}
		return processId;
	}
}
