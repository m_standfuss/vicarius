package com.cerner.vicarius;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.cerner.vicarius.biz.rulerunner.RuleRunner;
import com.cerner.vicarius.biz.rulerunner.RuleRunnerFactory;
import com.cerner.vicarius.biz.rules.RuleDelegate;
import com.cerner.vicarius.biz.rules.RuleRecord;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

/**
 * The scheduler is in charge of maintaining the schedule of active rule jobs to run at given priority intervals.
 *
 * @author MS025633
 */
public class Scheduler {
	
	public static final int DEFAULT_POOL_SIZE = 10;
	public static final int DEFAULT_UPDATE_SCHEDULER = 60000;
	public static final int DEFAULT_LOW_PRIORITY_SCHEDULE = 120000;
	public static final int DEFAULT_MEDIUM_PRIORITY_SCHEDULE = 60000;
	public static final int DEFAULT_HIGH_PRIORITY_SCHEDULE = 30000;
	public static final int DEFAULT_IMMEDIATE_PRIORITY_SCHEDULE = 15000;
	public static final int DEFAULT_UNKNOWN_PRIORITY_SCHEDULE = 3600000;
	
	private static final Logger logger = LogManager.getLogger(Scheduler.class);
	
	private final RuleRunnerFactory runnerFactory;
	private final RuleDelegate ruleDelegate;
	
	private final Map<Long, RuleRunnable> runnableMap;
	
	/*
	 * The amount of concurrent threads running jobs at a time.
	 */
	private int poolSize = DEFAULT_POOL_SIZE;
	
	/*
	 * How often the scheduler will update its rules to be ran.
	 */
	private long updateSchedule = DEFAULT_UPDATE_SCHEDULER;
	
	/*
	 * Scheduling frequencies for the different priorities these are in seconds.
	 */
	private long lowPrioritySchedule = DEFAULT_LOW_PRIORITY_SCHEDULE;
	private long mediumPrioritySchedule = DEFAULT_MEDIUM_PRIORITY_SCHEDULE;
	private long highPrioritySchedule = DEFAULT_HIGH_PRIORITY_SCHEDULE;
	private long immediatePrioritySchedule = DEFAULT_IMMEDIATE_PRIORITY_SCHEDULE;
	private long unknownPrioritySchedule = DEFAULT_UNKNOWN_PRIORITY_SCHEDULE;
	
	private Thread schedulerThread;
	private ScheduledThreadPoolExecutor executor;
	private boolean running;
	
	/**
	 * Sets how often (in milliseconds) that a low priority rule will be ran.
	 *
	 * @param lowPrioritySchedule
	 *            The frequency in milliseconds.
	 * @throws IllegalArgumentException
	 *             If zero or negative.
	 */
	public void setLowPrioritySchedule(long lowPrioritySchedule) {
		checkArgument(lowPrioritySchedule > 0);
		this.lowPrioritySchedule = lowPrioritySchedule;
	}
	
	/**
	 * Sets how often (in milliseconds) that a medium priority rule will be ran.
	 *
	 * @param mediumPrioritySchedule
	 *            The frequency in milliseconds.
	 * @throws IllegalArgumentException
	 *             If zero or negative.
	 */
	public void setMediumPrioritySchedule(long mediumPrioritySchedule) {
		checkArgument(mediumPrioritySchedule > 0);
		this.mediumPrioritySchedule = mediumPrioritySchedule;
	}
	
	/**
	 * Sets how often (in milliseconds) that a high priority rule will be ran.
	 *
	 * @param highPrioritySchedule
	 *            The frequency in milliseconds.
	 * @throws IllegalArgumentException
	 *             If zero or negative.
	 */
	public void setHighPrioritySchedule(long highPrioritySchedule) {
		checkArgument(highPrioritySchedule > 0);
		this.highPrioritySchedule = highPrioritySchedule;
	}
	
	/**
	 * Sets how often (in milliseconds) that a immediate priority rule will be
	 * ran.
	 *
	 * @param immediatePrioritySchedule
	 *            The frequency in milliseconds.
	 * @throws IllegalArgumentException
	 *             If zero or negative.
	 */
	public void setImmediatePrioritySchedule(long immediatePrioritySchedule) {
		checkArgument(immediatePrioritySchedule > 0);
		this.immediatePrioritySchedule = immediatePrioritySchedule;
	}
	
	/**
	 * Sets how often (in milliseconds) that a unknown priority rule will be
	 * ran.
	 *
	 * @param unknownPrioritySchedule
	 *            The frequency in milliseconds.
	 * @throws IllegalArgumentException
	 *             If zero or negative.
	 */
	public void setUnknownPrioritySchedule(long unknownPrioritySchedule) {
		checkArgument(unknownPrioritySchedule > 0);
		this.unknownPrioritySchedule = unknownPrioritySchedule;
	}
	
	/**
	 * Sets how often (in milliseconds) that scheduler will update its jobs
	 * being ran.
	 *
	 * @param updateSchedule
	 *            The update frequency in milliseconds.
	 * @throws IllegalArgumentException
	 *             If zero or negative.
	 */
	public void setUpdateSchedule(long updateSchedule) {
		checkArgument(updateSchedule > 0);
		this.updateSchedule = updateSchedule;
	}
	
	/**
	 * Sets how many concurrent rule jobs can be executing at a time.
	 *
	 * @param ruleJobPoolSize
	 *            The size of the pool of threads executing the rule jobs.
	 * @throws IllegalArgumentException
	 *             If zero or negative.
	 */
	public void setRuleJobPoolSize(int ruleJobPoolSize) {
		checkArgument(ruleJobPoolSize > 0);
		this.poolSize = ruleJobPoolSize;
	}
	
	/**
	 * Creates a new scheduler.
	 */
	public Scheduler() {
		this(new RuleRunnerFactory(), new RuleDelegate());
	}
	
	/**
	 * Creates a new scheduler with dependencies provided.
	 *
	 * @param runnerFactory
	 *            The rule runner factory to use.
	 * @param ruleDao
	 *            The rule data access object.
	 * @throws IllegalArgumentException
	 *             If any argument is null.
	 */
	public Scheduler(RuleRunnerFactory runnerFactory, RuleDelegate ruleDelegate) {
		checkArgument(runnerFactory != null, "The rule runner factory cannot be null.");
		checkArgument(ruleDelegate != null, "The rule delegate cannot be null.");
		this.runnerFactory = runnerFactory;
		this.ruleDelegate = ruleDelegate;
		
		runnableMap = Maps.newHashMap();
		running = false;
	}
	
	/**
	 * Starts the scheduler as a process in the background, meaning that control
	 * will return to caller. The scheduler can be terminated via the #link
	 * {@link #stop()} method.
	 *
	 * @throws IllegalStateException
	 *             If this scheduler has be started previously without first
	 *             stopping it.
	 */
	public void start() {
		checkState(schedulerThread == null || !schedulerThread.isAlive(),
				"This scheduler thread is already running and must be stopped before a new one can be started.");
		
		executor = new ScheduledThreadPoolExecutor(poolSize);
		
		/*
		 * If we remove when we cancel then when a job is inactivated we can
		 * cancel it's future which will remove it from the executor.
		 */
		executor.setRemoveOnCancelPolicy(true);
		
		/*
		 * When we call shutdown on the executor any rule runners that are
		 * currently delayed (sleeping) will not be executed. Allows for a
		 * faster graceful shutdown.
		 */
		executor.setExecuteExistingDelayedTasksAfterShutdownPolicy(false);
		
		schedulerThread = new Thread(() -> run());
		schedulerThread.setUncaughtExceptionHandler(
				(Thread t, Throwable e) -> logger.error("Uncaught exception thrown during scheduler thread.", e));
		
		running = true;
		schedulerThread.start();
	}
	
	/**
	 * Stops the scheduler thread. Any rules that are currently being executed
	 * will finish, but no future rules will be ran.
	 */
	public void stop() {
		checkState(running, "The scheduler is not currently running.");
		running = false;
		executor.shutdown();
	}
	
	/**
	 * The run method for the scheduler. Updates then sleeps on repeat until a
	 * signal to stop is issued.
	 */
	private void run() {
		while (running) {
			updateRules();
			try {
				Thread.sleep(updateSchedule);
			} catch (InterruptedException e) {
			}
		}
	}
	
	/**
	 * Updates the rule runners currently active based on what is set in the
	 * database.
	 */
	private void updateRules() {
		logger.debug("Starting updateRules.");
		List<RuleRecord> activeRules = ruleDelegate.getActiveRules();
		logger.debug(activeRules.size() + " active rules found.");
		
		synchronized (runnableMap) {
			/*
			 * Create new rule runners or update the schedule of existing
			 * runners.
			 */
			Set<Long> activeIds = Sets.newHashSet();
			for (RuleRecord rule : activeRules) {
				activeIds.add(rule.getRuleId());
				
				RuleRunnable runnable = runnableMap.get(rule.getRuleId());
				if (runnable == null) {
					runnable = new RuleRunnable(rule.getRuleId());
					runnable.schedule(getDelay(rule));
					runnableMap.put(rule.getRuleId(), runnable);
				} else {
					runnable.update(getDelay(rule));
				}
			}
			
			/*
			 * Remove any existing rule runners that were created but are now
			 * inactivated.
			 */
			for (Map.Entry<Long, RuleRunnable> entry : runnableMap.entrySet()) {
				if (!activeIds.contains(entry.getKey())) {
					entry.getValue().cancel();
				}
			}
			runnableMap.keySet().retainAll(activeIds);
		}
	}
	
	/**
	 * Gets the delay between runs of a rule.
	 *
	 * @param rule
	 *            The rule to run.
	 * @return The amount of time in milliseconds between executions of rule.
	 */
	private long getDelay(RuleRecord rule) {
		if (rule.getPriority() != null) {
			switch (rule.getPriority()) {
				case HIGH:
					return highPrioritySchedule;
				case MEDIUM:
					return mediumPrioritySchedule;
				case LOW:
					return lowPrioritySchedule;
				case IMMEDIATE:
					return immediatePrioritySchedule;
				case OFFSET:
					return rule.getOffset();
			}
		}
		return unknownPrioritySchedule;
	}
	
	/**
	 * Inner class that handles the running of rules. Basically a runnable
	 * wrapper around the {@link RuleRunner} class that also handles keeping
	 * track of current scheduling on the executor.
	 */
	private class RuleRunnable implements Runnable {
		
		private final RuleRunner ruleRunner;
		
		private long delay;
		private ScheduledFuture<?> future;
		
		/**
		 * Creates a new rule runnable for the given id.
		 *
		 * @param ruleId
		 *            The rule id.
		 */
		public RuleRunnable(Long ruleId) {
			ruleRunner = runnerFactory.getRuleRunner(ruleId);
		}
		
		/**
		 * Schedules this rule to run starting in the next [1, delay)
		 * milliseconds then will repeat after delay amount of seconds.
		 *
		 * @param delay
		 *            The amount of milliseconds in delay between executions of
		 *            the runner.
		 * @throws IllegalArgumentException
		 *             If the delay is not greater than 0.
		 */
		public void schedule(long delay) {
			logger.debug("Scheduling a rule runner with a delay of '" + delay + "' milliseconds.");
			checkArgument(delay > 0, "The delay must be greater than 0 seconds.");
			this.delay = delay;
			future = executor.scheduleAtFixedRate(this, getInitialDelay(delay), delay, TimeUnit.MILLISECONDS);
		}
		
		/**
		 * Updates the scheduled delay of this runner. If it has changed then
		 * the current runner is stopped before its next execution and is
		 * re-scheduled on the new delay. At most this will cause the span in
		 * execution time to be oldTime + newTime, if it would be about to run
		 * now before the canceling, then on the new schedule it waits the full
		 * amount of delay before its new execution.
		 *
		 * @param delay
		 *            The new delay of the runner.
		 * @throws IllegalArgumentException
		 *             If the delay is not greater than 0.
		 * @throw IllegalStateException If the runner has never been scheduled.
		 */
		public void update(long delay) {
			checkArgument(delay > 0, "The delay must be greater than 0 seconds.");
			checkState(future != null, "The runnable must be scheduled prior to calling update.");
			if (this.delay != delay) {
				logger.debug(
						"Updating the schedule of existing rule runner. Existing=" + this.delay + ", New=" + delay);
				cancel();
				schedule(delay);
			}
		}
		
		@Override
		public void run() {
			logger.debug("Running rule.");
			try {
				ruleRunner.run();
			} catch (Throwable t) {
				logger.error("Unexpected exception when running rule '" + ruleRunner.getRuleId() + "'.", t);
			}
		}
		
		/**
		 * Cancels any future executions of the runner.
		 *
		 * @throw IllegalStateException If the runner has never been scheduled.
		 */
		private void cancel() {
			checkState(future != null, "The runnable must be scheduled prior to calling cancel.");
			if (!future.cancel(true)) {
				throw new IllegalStateException("Unable to successfully cancel the rule runner task.");
			}
		}
		
		/**
		 * Gets the initial time to delay before first execution. This will
		 * ensure that the jobs have a staggered distribution rather then all
		 * kicking off at the same time then all waiting at the same time.
		 *
		 * @param delay
		 *            The delay time of the job.
		 * @return The initial delay to wait. Will be between [1, delay]
		 */
		private long getInitialDelay(long delay) {
			return ThreadLocalRandom.current().nextLong(delay) + 1;
		}
	}
}
