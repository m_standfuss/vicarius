package com.cerner.vicarius.biz.rulerunner;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.Comparator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.cerner.vicarius.biz.EntityMap;
import com.cerner.vicarius.data.model.Job;
import com.cerner.vicarius.data.model.RuleJob;

/**
 * A comparator that orders rule jobs that are conditionals first then actions. Within each job type the collating
 * sequence is used to order.
 *
 * @author MS025633
 */
public class RuleJobComparator implements Comparator<RuleJob> {

	private static final Logger logger = LogManager.getLogger(RuleJobComparator.class);

	private final EntityMap<Job> jobMap;

	/**
	 * Creates a new rule job to use to compare.
	 *
	 * @param jobMap
	 *            The job map. This should contain all the corresponding jobs
	 *            for the rule jobs to sort.
	 */
	public RuleJobComparator(EntityMap<Job> jobMap) {
		checkArgument(jobMap != null, "The jobMap cannot be null.");
		this.jobMap = jobMap;
	}

	@Override
	public int compare(RuleJob ruleJob1, RuleJob ruleJob2) {
		if (ruleJob1 == null) {
			return ruleJob2 == null ? 0 : 1;
		}
		if (ruleJob2 == null) {
			return -1;
		}

		Job job1 = jobMap.get(ruleJob1.getJobId());
		Job job2 = jobMap.get(ruleJob2.getJobId());

		if (job1 == null) {
			logger.error("Missing Job entity for ruleJob with id = " + ruleJob1.getRuleJobId());
			return 1;
		}
		if (job2 == null) {
			logger.error("Missing Job entity for ruleJob with id = " + ruleJob2.getRuleJobId());
			return -1;
		}

		if (job1.getConditionInd() == job2.getConditionInd()) {
			/*
			 * If both are conditions or both actions use the collating sequence to determine sorting.
			 */
			Long collation1 = ruleJob1.getCollationSeq();
			Long collation2 = ruleJob2.getCollationSeq();
			if (collation1 == null) {
				return collation2 == null ? 0 : 1;
			}
			return collation2 == null ? -1 : Long.compare(collation1, collation2);
		}
		return job1.getConditionInd() ? -1 : 1;
	}

}
