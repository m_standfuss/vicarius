package com.cerner.vicarius;

import static com.google.common.base.Preconditions.checkArgument;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;
import java.util.function.Consumer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jasypt.util.text.BasicTextEncryptor;

import com.cerner.vicarius.biz.moduleloader.ModuleLoader;
import com.cerner.vicarius.biz.rulerunner.RuleRunner;
import com.cerner.vicarius.data.access.Dao;
import com.cerner.vicarius.module.ModuleSpec;

/**
 * The application
 *
 * @author MS025633
 */
public class Application {

	public static final char[] DEFAULT_ENCRYPTION_KEY = new char[] { 'V', '!', 'C', '@', 'r', 'i', 'U', '$' };

	private static final Logger logger = LogManager.getLogger(Application.class);

	private static final String PROPERTIES_FILE = "vicarius.properties";

	private static final String PROPERTIES_FILE_ENCRYPTED = "vicarius.encrypted.properties";

	private static final String PROP_RULE_JOB_POOL = "RULE_JOB_POOL_SIZE";
	private static final String PROP_UPDATE_SCHEDULER = "UPDATE_RULE_JOBS_SCHEDULE";
	private static final String PROP_LOW_PRIORITY_SCHEDULE = "LOW_PRIORITY_SCHEDULE";
	private static final String PROP_MEDIUM_PRIORITY_SCHEDULE = "MEDIUM_PRIORITY_SCHEDULE";
	private static final String PROP_HIGH_PRIORITY_SCHEDULE = "HIGH_PRIORITY_SCHEDULE";
	private static final String PROP_IMMEDIATE_PRIORITY_SCHEDULE = "IMMEDIATE_PRIORITY_SCHEDULE";
	private static final String PROP_UNKNOWN_PRIORITY_SCHEDULE = "UNKNOWN_PRIORITY_SCHEDULE";
	private static final String PROP_GRACEFUL_SHUTDOWN = "GRACEFUL_SHUTDOWN";
	private static final String PROP_MODULE_DIR = "MODULE_DIR";

	private static final String PROP_DB_HOSTNAME = "DATABASE_HOST";
	private static final String PROP_DB_USERNAME = "DATABASE_USERNAME";
	private static final String PROP_DB_PASSWORD = "DATABASE_PASSWORD";

	private static Properties appProperties;
	private static Properties encryptedProperties;

	private final ApplicationLock lock;
	private final Scheduler scheduler;
	private final ModuleLoader moduleLoader;

	static {
		/*
		 * Load properties when first referenced. Can be re-loaded at anytime by
		 * calling the method directly.
		 */
		loadProperties();
	}

	/**
	 * Sets the unencrypted properties to use for the application.
	 * 
	 * @param properties
	 *            The properties
	 */
	public static void setProperties(Properties properties) {
		Application.appProperties = properties;
	}

	/**
	 * Sets the encrypted properties to use for the application.
	 * 
	 * @param properties
	 *            The properties.
	 */
	public static void setEncryptedProperties(Properties properties) {
		Application.encryptedProperties = properties;
	}

	/**
	 * Loads the properties into memory for the application. These properties
	 * can queried or gotten from modules that are started from this
	 * application.
	 */
	public static void loadProperties() {
		appProperties = loadProperties(PROPERTIES_FILE);
		encryptedProperties = loadProperties(PROPERTIES_FILE_ENCRYPTED);
	}

	/**
	 * Gets a property from the application properties file.
	 *
	 * @param module
	 *            The class of the module using the property.
	 * @param propName
	 *            The property name.
	 * @return The property value, null if not found.
	 * @see #getProperty(Class, String, String)
	 */
	public static String getProperty(Class<? extends ModuleSpec> module, String propName) {
		return getProperty(module, propName, null);
	}

	/**
	 * Gets a property from the application properties file, with a default
	 * value if not found.
	 *
	 * @param module
	 *            The class of the module using the property.
	 * @param propName
	 *            The property name.
	 * @param defaultValue
	 *            The value to use if not found.
	 * @return The property value, defaultValue if not found.
	 * @throws IllegalArgumentException
	 *             If the module class is null or does not have a canonical
	 *             name.
	 * @throws IllegalArgumentException
	 *             If the property name is null or empty.
	 * @see #getProperty(Class, String)
	 */
	public static String getProperty(Class<? extends ModuleSpec> module, String propName, String defaultValue) {
		checkArgument(module != null, "The module class cannot be null.");
		checkArgument(module.getCanonicalName() != null, "The module must be a proper class.");
		checkArgument(propName != null && !propName.trim().isEmpty(), "The property name cannot be null or empty.");

		String fullPropName = module.getCanonicalName() + '.' + propName;
		String property = getProperty(fullPropName);

		return property == null ? defaultValue : property;
	}

	/**
	 * Gets if a property exists in the application properties file.
	 * 
	 * @param module
	 *            The class of the module using the property.
	 * @param propName
	 *            The property name.
	 * @return If the property exists or not.
	 */
	public static boolean hasProperty(Class<? extends ModuleSpec> module, String propName) {
		checkArgument(module != null, "The module class cannot be null.");
		checkArgument(module.getCanonicalName() != null, "The module must be a proper class.");
		checkArgument(propName != null && !propName.trim().isEmpty(), "The property name cannot be null or empty.");

		return hasProperty(getFullPropertyName(module, propName));
	}

	private static String getFullPropertyName(Class<? extends ModuleSpec> module, String propName) {
		return module.getCanonicalName() + '.' + propName;
	}

	private static boolean hasProperty(String propName) {
		if (appProperties == null) {
			return false;
		}
		return appProperties.containsKey(propName);
	}

	private static Properties loadProperties(String fileName) {
		File propFile = new File(fileName);
		if (!propFile.exists()) {
			logger.warn("No properties file found at '" + propFile.getAbsolutePath() + "'.");
			return null;
		}

		try {
			InputStream input = new FileInputStream(propFile);
			Properties props = new Properties();
			props.load(input);
			return props;
		} catch (IOException e) {
			logger.error("An error occured while attempting to read the properties file.", e);
			return null;
		}
	}

	private static String getProperty(String propName) {
		if (appProperties != null) {
			logger.debug("Checking application properties.");
			String property = appProperties.getProperty(propName);
			if (property != null) {
				return property;
			}
		}
		if (encryptedProperties != null) {
			logger.debug("Checking encrypted properties.");
			String encryptedProperty = encryptedProperties.getProperty(propName);
			if (encryptedProperty != null) {
				return decrypt(encryptedProperty);
			}
		}
		logger.warn("Missing requested property '" + propName + "'.");
		return null;
	}

	private static String decrypt(String value) {
		BasicTextEncryptor encryptor = new BasicTextEncryptor();
		encryptor.setPasswordCharArray(DEFAULT_ENCRYPTION_KEY);
		return encryptor.decrypt(value);
	}

	/**
	 * Creates a new instance of the application.
	 */
	public Application() {
		this(new ApplicationLock(), new Scheduler(), new ModuleLoader());
	}

	/**
	 * Create a new instance of the application with the provided lock.
	 *
	 * @param lock
	 *            The application lock.
	 * @throws IllegalArgumentException
	 *             If the lock is null.
	 * @throws IllegalArgumentException
	 *             If any argument is null.
	 */
	public Application(ApplicationLock lock, Scheduler scheduler, ModuleLoader moduleLoader) {
		checkArgument(lock != null, "The lock cannot be null.");
		checkArgument(scheduler != null, "The scheduler cannot be null.");
		checkArgument(moduleLoader != null, "The module loader cannot be null.");
		this.lock = lock;
		this.scheduler = scheduler;
		this.moduleLoader = moduleLoader;
	}

	/**
	 * Starts the application. Checks if any other instances currently hold the
	 * application lock prior to starting. If not other instances are running
	 * then it creates its own lock on the application.
	 *
	 * @throws IOException
	 *             With any complications of locking the application.
	 */
	public void start() throws IOException {
		logger.debug("Starting start.");
		if (!setRequiredString(PROP_DB_HOSTNAME, s -> Dao.setHostName(s))
				|| !setRequiredString(PROP_DB_USERNAME, s -> Dao.setHostName(s))
				|| !setRequiredString(PROP_DB_PASSWORD, s -> Dao.setPassword(s.toCharArray()))) {
			logger.error("Missing database required parameter, unable to start application.");
			return;
		}
		boolean locked = lock.obtainLock();

		if (!locked) {
			logger.debug("Unable to obtain the application lock exiting.");
			return;
		}
		// The application loop.
		logger.debug("Starting application loop.");
		setProperties();
		initializeModules();
		scheduler.start();
		while (continueApplication()) {
			try {
				/*
				 * Sleep for a quarter of the application lock timeout to give
				 * ourselves ample time to shutdown gracefully.
				 */
				Thread.sleep(ApplicationLock.getGracefulShutdownPeriod() / 4);
			} catch (InterruptedException e) {
				break;
			}
		}
		logger.debug("Application loop ended.");
		scheduler.stop();
		shutdownModules();
		lock.releaseLock();
	}

	/**
	 * Attempts to shutdown any other instances of the application that are
	 * running based on what currently holds the application lock.
	 *
	 * @throws InterruptedException
	 *             If we are unable to terminate the application.
	 * @throws IOException
	 *             With any complications with unlocking the lock.
	 */
	public void stop() throws IOException, InterruptedException {
		logger.debug("Starting stop.");
		lock.terminateLockedApplication();
	}

	private boolean continueApplication() throws IOException {
		return lock.isLocked();
	}

	private void setProperties() {
		setLongIfPresent(PROP_HIGH_PRIORITY_SCHEDULE, l -> scheduler.setHighPrioritySchedule(l));
		setLongIfPresent(PROP_IMMEDIATE_PRIORITY_SCHEDULE, l -> scheduler.setImmediatePrioritySchedule(l));
		setLongIfPresent(PROP_LOW_PRIORITY_SCHEDULE, l -> scheduler.setLowPrioritySchedule(l));
		setLongIfPresent(PROP_MEDIUM_PRIORITY_SCHEDULE, l -> scheduler.setMediumPrioritySchedule(l));
		setLongIfPresent(PROP_UNKNOWN_PRIORITY_SCHEDULE, l -> scheduler.setUnknownPrioritySchedule(l));
		setLongIfPresent(PROP_UPDATE_SCHEDULER, l -> scheduler.setUpdateSchedule(l));
		setIntIfPresent(PROP_RULE_JOB_POOL, i -> scheduler.setRuleJobPoolSize(i));

		setStringIfPresent(PROP_MODULE_DIR, s -> moduleLoader.setModuleDirectory(s));

		setIntIfPresent(PROP_GRACEFUL_SHUTDOWN, i -> ApplicationLock.setGracefulShutdownPeriod(i));
	}

	private boolean setRequiredString(String key, Consumer<String> consumer) {
		if (!hasProperty(key)) {
			logger.error("Missing a required property '" + key + "'.");
			return false;
		}
		consumer.accept(getProperty(key));
		return true;
	}

	private void setIntIfPresent(String key, Consumer<Integer> consumer) {
		String value = getProperty(key);
		if (value != null) {
			try {
				int i = Integer.parseInt(value);
				consumer.accept(i);
			} catch (NumberFormatException e) {
				logger.error("Unable to parse property '" + key + "' into an integer value.", e);
			}
		}
	}

	private void setLongIfPresent(String key, Consumer<Long> consumer) {
		String value = getProperty(key);
		if (value != null) {
			try {
				long l = Long.parseLong(value);
				consumer.accept(l);
			} catch (NumberFormatException e) {
				logger.error("Unable to parse property '" + key + "' into a long value.", e);
			}
		}
	}

	private void setStringIfPresent(String key, Consumer<String> consumer) {
		String value = getProperty(key);
		if (value != null) {
			consumer.accept(value);
		}
	}

	private void initializeModules() {
		Map<Long, ModuleSpec> moduleMap = moduleLoader.loadActiveModules();
		RuleRunner.setModuleSpecMap(moduleMap);
	}

	private void shutdownModules() {
		moduleLoader.stopAllModules();
	}
}
