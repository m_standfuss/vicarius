package com.cerner.vicarius.biz.rulerunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class JobPropertyKeyTest {

	private static final Long RULE_JOB_ID_1 = 1L;
	private static final Long RULE_JOB_ID_2 = 2L;

	private static final String PROP_NAME_1 = "PROP_NAME_1";
	private static final String PROP_NAME_2 = "PROP_NAME_2";

	@Test
	public void hashCode_SameFields_Equals() {
		RuleJobPropertyKey key1 = new RuleJobPropertyKey(RULE_JOB_ID_1, PROP_NAME_1);
		RuleJobPropertyKey key2 = new RuleJobPropertyKey(RULE_JOB_ID_1, PROP_NAME_1);

		assertEquals(key1.hashCode(), key2.hashCode());
	}

	@Test
	public void hashCode_NullFields_Equals() {
		RuleJobPropertyKey key1 = new RuleJobPropertyKey(null, null);
		RuleJobPropertyKey key2 = new RuleJobPropertyKey(null, null);

		assertEquals(key1.hashCode(), key2.hashCode());
	}

	@Test
	public void hashCode_SameRuleJobDifferentPropertyName_DoesNotEqual() {
		RuleJobPropertyKey key1 = new RuleJobPropertyKey(RULE_JOB_ID_1, PROP_NAME_1);
		RuleJobPropertyKey key2 = new RuleJobPropertyKey(RULE_JOB_ID_1, PROP_NAME_2);

		assertTrue("The hashcodes should not equal.", key1.hashCode() != key2.hashCode());
	}

	@Test
	public void hashCode_SamePropertyNameDifferentRuleJob_DoesNotEqual() {
		RuleJobPropertyKey key1 = new RuleJobPropertyKey(RULE_JOB_ID_1, PROP_NAME_1);
		RuleJobPropertyKey key2 = new RuleJobPropertyKey(RULE_JOB_ID_2, PROP_NAME_1);

		assertTrue("The hashcodes should not equal.", key1.hashCode() != key2.hashCode());
	}

	@Test
	public void equals_SameFields_Equals() {
		RuleJobPropertyKey key1 = new RuleJobPropertyKey(RULE_JOB_ID_1, PROP_NAME_1);
		RuleJobPropertyKey key2 = new RuleJobPropertyKey(RULE_JOB_ID_1, PROP_NAME_1);
		assertTrue("The equals should return true.", key1.equals(key2));
		assertTrue("The equals should return true.", key2.equals(key1));
	}

	@Test
	public void equals_NullFields_Equals() {
		RuleJobPropertyKey key1 = new RuleJobPropertyKey(null, null);
		RuleJobPropertyKey key2 = new RuleJobPropertyKey(null, null);

		assertTrue("The equals should return true.", key1.equals(key2));
		assertTrue("The equals should return true.", key2.equals(key1));
	}

	@Test
	public void equals_OneRuleIdNull_DoesNotEqual() {
		RuleJobPropertyKey key1 = new RuleJobPropertyKey(RULE_JOB_ID_1, PROP_NAME_1);
		RuleJobPropertyKey key2 = new RuleJobPropertyKey(null, PROP_NAME_1);

		assertFalse("The equals should return false.", key1.equals(key2));
		assertFalse("The equals should return false.", key2.equals(key1));
	}

	@Test
	public void equals_OnePropNameNull_DoesNotEqual() {
		RuleJobPropertyKey key1 = new RuleJobPropertyKey(RULE_JOB_ID_1, PROP_NAME_1);
		RuleJobPropertyKey key2 = new RuleJobPropertyKey(RULE_JOB_ID_1, null);

		assertFalse("The equals should return false.", key1.equals(key2));
		assertFalse("The equals should return false.", key2.equals(key1));
	}

	@Test
	public void equals_SameRuleJobDifferentPropertyName_DoesNotEqual() {
		RuleJobPropertyKey key1 = new RuleJobPropertyKey(RULE_JOB_ID_1, PROP_NAME_1);
		RuleJobPropertyKey key2 = new RuleJobPropertyKey(RULE_JOB_ID_1, PROP_NAME_2);

		assertFalse("The equals should return false.", key1.equals(key2));
		assertFalse("The equals should return false.", key2.equals(key1));
	}

	@Test
	public void equals_SamePropertyNameDifferentRuleJob_DoesNotEqual() {
		RuleJobPropertyKey key1 = new RuleJobPropertyKey(RULE_JOB_ID_1, PROP_NAME_1);
		RuleJobPropertyKey key2 = new RuleJobPropertyKey(RULE_JOB_ID_2, PROP_NAME_1);

		assertFalse("The equals should return false.", key1.equals(key2));
		assertFalse("The equals should return false.", key2.equals(key1));
	}

	@Test
	public void equals_SameInstance_ReturnsTrue() {
		RuleJobPropertyKey key1 = new RuleJobPropertyKey(null, null);
		RuleJobPropertyKey key2 = key1;

		assertTrue("The equals should return true.", key1.equals(key2));
		assertTrue("The equals should return true.", key2.equals(key1));
	}

	@Test
	public void equals_OtherIsNull_DoesNotEqual() {
		RuleJobPropertyKey key1 = new RuleJobPropertyKey(RULE_JOB_ID_1, PROP_NAME_1);
		RuleJobPropertyKey key2 = null;

		assertFalse("The equals should return false.", key1.equals(key2));
	}

	@Test
	public void equals_OtherDifferentClass_DoesNotEqual() {
		RuleJobPropertyKey key1 = new RuleJobPropertyKey(RULE_JOB_ID_1, PROP_NAME_1);
		Long key2 = 1L;

		assertFalse("The equals should return false.", key1.equals(key2));
	}
}
