package com.cerner.vicarius.dummymodules;

import com.cerner.vicarius.module.ConditionalJob;

public class DummyConditionalException extends ConditionalJob {
	
	private static int invokeCount = 0;

	public static int getInvokeCount() {
		return invokeCount;
	}
	
	public static void resetInvokeCount() {
		invokeCount = 0;
	}

	@Override
	public boolean run() {
		invokeCount++;
		throw new RuntimeException("");
	}
}
