package com.cerner.vicarius.biz;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.List;

import org.junit.Test;

import com.google.common.collect.Lists;

public class TestEntityMap {

	private static final Long PRIMARY_KEY_1 = 1L;
	private static final Long PRIMARY_KEY_2 = 2L;

	@Test
	public void newEntityMap_NoCollection_EmptyMap() {
		assertEquals(0, EntityMap.newEntityMap().size());
	}

	@Test(expected = IllegalArgumentException.class)
	public void newEntityMap_NullCollection_ExceptionThrown() {
		EntityMap.newEntityMap(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void newEntityMap_NullOnCollection_ExceptionThrown() {
		List<DummyEntity> list = Lists.newArrayList((DummyEntity) null);
		EntityMap.newEntityMap(list);
	}

	@Test
	public void newEntityMap_CollectionPassedIn_AddedToMap() {
		EntityMap<DummyEntity> map = EntityMap.newEntityMap(makeEntities(PRIMARY_KEY_1));

		assertEquals(1, map.size());
		assertNotNull(map.get(PRIMARY_KEY_1));
	}

	@Test
	public void constructor_CreatesEmptyMap() {
		assertEquals(0, new EntityMap<DummyEntity>().size());
	}

	@Test
	public void constructor_CollectionSupplied_CollectionLoaded() {
		EntityMap<DummyEntity> map = new EntityMap<>(makeEntities(PRIMARY_KEY_1));

		assertEquals(1, map.size());
		assertNotNull(map.get(PRIMARY_KEY_1));
	}

	@Test(expected = IllegalArgumentException.class)
	public void add_NullEntity_ThrowsException() {
		EntityMap<DummyEntity> map = new EntityMap<>();
		map.add(null);
	}

	@Test
	public void add_AddingEntities_SizeIncrements() {
		EntityMap<DummyEntity> map = new EntityMap<>();
		map.add(makeEntity(PRIMARY_KEY_1));
		assertEquals(1, map.size());
		map.add(makeEntity(PRIMARY_KEY_2));
		assertEquals(2, map.size());
	}

	@Test(expected = IllegalArgumentException.class)
	public void load_NullCollection_ExceptionThrown() {
		new EntityMap<DummyEntity>().load(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void load_NullOnCollection_ExceptionThrown() {
		List<DummyEntity> list = Lists.newArrayList((DummyEntity) null);
		EntityMap<DummyEntity> map = new EntityMap<>();
		map.load(list);
	}

	@Test
	public void load_CollectionPassedIn_AddedToMap() {
		EntityMap<DummyEntity> map = new EntityMap<>();
		map.load(makeEntities(PRIMARY_KEY_1));

		assertEquals(1, map.size());
		assertNotNull(map.get(PRIMARY_KEY_1));
	}

	@Test
	public void get_NoEntity_NullReturned() {
		EntityMap<DummyEntity> map = EntityMap.newEntityMap();
		assertNull(map.get(PRIMARY_KEY_1));
	}

	@Test
	public void get_EntityExists_EntityReturns() {
		EntityMap<DummyEntity> map = EntityMap.newEntityMap();
		map.add(makeEntity(PRIMARY_KEY_1));

		assertNotNull(map.get(PRIMARY_KEY_1));
	}

	@Test
	public void clear_SizeResetToZero() {
		EntityMap<DummyEntity> map = EntityMap.newEntityMap();
		map.add(makeEntity(PRIMARY_KEY_1));

		map.clear();
		assertEquals(0, map.size());
	}

	private List<DummyEntity> makeEntities(Long... primaryKeys) {
		List<DummyEntity> entities = Lists.newArrayList();
		for (Long primaryKey : primaryKeys) {
			entities.add(makeEntity(primaryKey));
		}
		return entities;
	}

	private DummyEntity makeEntity(Long primaryKey) {
		return new DummyEntity(primaryKey);
	}
}
