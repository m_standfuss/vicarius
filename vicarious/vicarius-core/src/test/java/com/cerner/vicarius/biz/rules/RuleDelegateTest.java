package com.cerner.vicarius.biz.rules;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;

import com.cerner.vicarius.data.access.RuleDao;
import com.cerner.vicarius.data.model.Rule;
import com.google.common.collect.Lists;

public class RuleDelegateTest {

	private static final Long RULE_ID_1 = 1L;
	private static final Date LAST_COMPLETED_DTTM_1 = new Date(100l);
	
	@Test(expected = IllegalArgumentException.class)
	public void constructor_NullRuleDao_ExceptionThrown() {
		makeDelegate(null);
	}

	@Test
	public void getActiveRules_QueryCalled() {
		RuleDao ruleDao = makeRuleDao();
		RuleDelegate delegate = makeDelegate(ruleDao);
		delegate.getActiveRules();

		verify(ruleDao, times(1)).findActive();
	}

	@Test
	public void getActiveRules_QueryEmpty_ReturnsEmpty() {
		RuleDao ruleDao = makeRuleDao();
		when(ruleDao.findActive()).thenReturn(new ArrayList<>());
		RuleDelegate delegate = makeDelegate(ruleDao);

		assertEquals(0, delegate.getActiveRules().size());
	}

	@Test
	public void getActiveRules_ResultsFromQuery_PopulateRecord() {
		Rule rule = makeRule(RULE_ID_1, LAST_COMPLETED_DTTM_1);
		
		RuleDao ruleDao = makeRuleDao();
		when(ruleDao.findActive()).thenReturn(Lists.newArrayList(rule));
		RuleDelegate delegate = makeDelegate(ruleDao);

		List<RuleRecord> records = delegate.getActiveRules();
		assertEquals(1, records.size());

		RuleRecord record = records.get(0);
		assertEquals(RULE_ID_1, record.getRuleId());
		assertEquals(LAST_COMPLETED_DTTM_1, record.getLastCompletedOn());
	}
	
	private RuleDelegate makeDelegate(RuleDao ruleDao) {
		return new RuleDelegate(ruleDao);
	}
	
	private RuleDao makeRuleDao() {
		RuleDao ruleDao = mock(RuleDao.class);
		return ruleDao;
	}
	
	private Rule makeRule(Long ruleId, Date lastCompleted) {
		Rule rule = new Rule();
		rule.setRuleId(ruleId);
		rule.setLastCompleted(lastCompleted);
		return rule;
	}

}
