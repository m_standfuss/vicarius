package com.cerner.vicarius.dummymodules;

import com.cerner.vicarius.module.ModuleSpec;

public class DummyModuleNoConstructor extends ModuleSpec {
	
	public DummyModuleNoConstructor(Long longValue) {
	}

	@Override
	public void startup() {
	}

	@Override
	public void shutdown() {
	}
}
