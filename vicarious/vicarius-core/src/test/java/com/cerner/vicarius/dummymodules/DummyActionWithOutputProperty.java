package com.cerner.vicarius.dummymodules;

import com.cerner.vicarius.module.ActionJob;

public class DummyActionWithOutputProperty extends ActionJob {

	private static Object output;

	public static void setOutput(Object object) {
		output = object;
	}

	public static void clearOutput() {
		output = null;
	}

	public DummyActionWithOutputProperty() {

	}

	public Object getOutput() {
		return output;
	}

	@Override
	public void run() {
	}
}
