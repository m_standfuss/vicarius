package com.cerner.vicarius.dummymodules;

import com.cerner.vicarius.module.ActionJob;

public class DummyAction extends ActionJob {
	
	private static int invokeCount = 0;
	
	public static int getInvokeCount() {
		return invokeCount;
	}

	public static void resetInvokeCount() {
		invokeCount = 0;
	}
	
	@Override
	public void run() {
		invokeCount++;
	}
}
