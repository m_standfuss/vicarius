package com.cerner.vicarius.biz.rulerunner;

import static org.junit.Assert.assertEquals;

import java.io.InvalidObjectException;
import java.util.HashMap;

import org.junit.Test;

import com.cerner.vicarius.data.model.PropertyType;
import com.cerner.vicarius.data.model.RuleJobParameter;
import com.cerner.vicarius.data.model.RuleJobParameter.ParameterType;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

public class InputParameterFactoryTest {

	@Test(expected = IllegalArgumentException.class)
	public void parseInputParameter_NullType_ExceptionThrown() throws InvalidObjectException {
		new InputParameterFactory().parseInputParameter(null, makeRuleJobParameter(), new HashMap<>());
	}

	@Test(expected = IllegalArgumentException.class)
	public void parseInputParameter_NullJson_ExceptionThrown() throws InvalidObjectException {
		new InputParameterFactory().parseInputParameter(PropertyType.BOOLEAN, null, new HashMap<>());
	}

	@Test(expected = IllegalArgumentException.class)
	public void parseInputParameter_NullMap_ExceptionThrown() throws InvalidObjectException {
		new InputParameterFactory().parseInputParameter(PropertyType.BOOLEAN, makeRuleJobParameter(), null);
	}

	@Test
	public void parseInputParameter_JsonPrimitivie_InputPrimitiveTypeReturned() throws InvalidObjectException {
		JsonPrimitive prim = new JsonPrimitive(true);
		InputParameter parameter = new InputParameterFactory().parseInputParameter(PropertyType.BOOLEAN,
				makeRuleJobParameter(ParameterType.PRIMITIVE, "true"), new HashMap<>());

		assertEquals(PrimitiveInputParameter.class, parameter.getClass());
	}

	@Test
	public void parseInputParameter_OutputProperty_InputFromOutputReturned() throws InvalidObjectException {
		JsonObject obj = new JsonObject();
		obj.addProperty("ruleJobId", 213L);
		obj.addProperty("parameterName", "asdf");
		RuleJobParameter rjp = makeRuleJobParameter(ParameterType.OUTPUT_PARAMETER, obj.toString());
		InputParameter parameter = new InputParameterFactory().parseInputParameter(PropertyType.STRING, rjp,
				new HashMap<>());
		assertEquals(InputFromOutputParameter.class, parameter.getClass());
	}

	@Test
	public void parseInputParameter_EmbeddedString_EmbeddedInputStringReturned() throws InvalidObjectException {
		JsonObject obj = new JsonObject();
		obj.addProperty("value", "");
		obj.add("properties", new JsonArray());
		RuleJobParameter rjp = makeRuleJobParameter(ParameterType.EMBEDDED_STRING, obj.toString());
		InputParameter parameter = new InputParameterFactory().parseInputParameter(PropertyType.STRING, rjp,
				new HashMap<>());
		assertEquals(EmbeddedInputStringParameter.class, parameter.getClass());
	}

	private RuleJobParameter makeRuleJobParameter() {
		return makeRuleJobParameter(ParameterType.PRIMITIVE, "");
	}

	private RuleJobParameter makeRuleJobParameter(ParameterType type, String content) {
		RuleJobParameter rjp = new RuleJobParameter();
		rjp.setContent(content);
		rjp.setType(type);
		return rjp;
	}
}
