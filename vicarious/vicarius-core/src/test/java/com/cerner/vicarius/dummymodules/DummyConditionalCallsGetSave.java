package com.cerner.vicarius.dummymodules;

import com.cerner.vicarius.module.ConditionalJob;

public class DummyConditionalCallsGetSave extends ConditionalJob {

	private static String lastSaveState;

	public static String getLastSaveStateSet() {
		return lastSaveState;
	}
	
	public static void clearLastSaveState() {
		lastSaveState = null;
	}

	@Override
	public boolean run() {
		lastSaveState = getSaveState();
		return true;
	}
	
}
