package com.cerner.vicarius.biz.rulerunner;

import static org.junit.Assert.assertNull;

import org.junit.Test;

public class EmptyInputParameterTest {

	@Test
	public void getParameterClass_ReturnsNull() {
		assertNull(new EmptyInputParameter().getParameterClass());
	}

	@Test
	public void getParameterValue_ReturnsNull() {
		assertNull(new EmptyInputParameter().getParameterValue());
	}
}
