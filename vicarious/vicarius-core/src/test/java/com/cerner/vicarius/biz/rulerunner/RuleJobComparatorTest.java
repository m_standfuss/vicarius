package com.cerner.vicarius.biz.rulerunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.cerner.vicarius.biz.EntityMap;
import com.cerner.vicarius.data.model.Job;
import com.cerner.vicarius.data.model.RuleJob;
import com.google.common.collect.Lists;

public class RuleJobComparatorTest {

	private static final Long RULE_JOB_ID_1 = 1L;
	private static final Long RULE_JOB_ID_2 = 2L;
	private static final Long RULE_JOB_ID_3 = 3L;

	private static final Long JOB_ID_1 = 1L;
	private static final Long JOB_ID_2 = 2L;
	private static final Long JOB_ID_3 = 3L;

	private EntityMap<Job> jobMap;
	private RuleJobComparator comparator;

	@Before
	public void setup() {
		jobMap = EntityMap.newEntityMap();
		jobMap.add(makeJob(JOB_ID_1, true));
		jobMap.add(makeJob(JOB_ID_2, false));
		jobMap.add(makeJob(JOB_ID_3, true));

		comparator = new RuleJobComparator(jobMap);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructor_nullMap_ExceptionThrown() {
		new RuleJobComparator(null);
	}

	@Test
	public void compare_BothConditionalAndNot_ConditionalsOrderedFirst() {
		List<RuleJob> ruleJobs = Lists.newArrayList();
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_1, JOB_ID_1));
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_2, JOB_ID_2));
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_3, JOB_ID_3));

		RuleJobComparator comparator = new RuleJobComparator(jobMap);
		ruleJobs.sort(comparator);

		assertTrue(jobMap.get(ruleJobs.get(0).getJobId()).getConditionInd());
		assertTrue(jobMap.get(ruleJobs.get(1).getJobId()).getConditionInd());
		assertFalse(jobMap.get(ruleJobs.get(2).getJobId()).getConditionInd());
	}

	@Test
	public void compare_NullsOnList_NullsMovedToEnd() {
		List<RuleJob> ruleJobs = Lists.newArrayList();
		ruleJobs.add(null);
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_1, JOB_ID_1));
		ruleJobs.add(null);

		ruleJobs.sort(comparator);

		assertNull(ruleJobs.get(1));
		assertNull(ruleJobs.get(2));
	}

	@Test
	public void compare_MissingJobs_MovedToEnd() {
		jobMap = EntityMap.newEntityMap();
		jobMap.add(makeJob(RULE_JOB_ID_2, true));

		comparator = new RuleJobComparator(jobMap);

		List<RuleJob> ruleJobs = Lists.newArrayList();
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_1, JOB_ID_1));
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_2, JOB_ID_2));
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_3, JOB_ID_3));

		ruleJobs.sort(comparator);

		assertNotNull(jobMap.get(ruleJobs.get(0).getJobId()));
		assertNull(jobMap.get(ruleJobs.get(1).getJobId()));
		assertNull(jobMap.get(ruleJobs.get(2).getJobId()));
	}
	
	@Test
	public void compare_CollationSequenceSetAllConditions_OrderedByCollation() {
		jobMap = EntityMap.newEntityMap();
		jobMap.add(makeJob(JOB_ID_1, true));
		jobMap.add(makeJob(JOB_ID_2, true));
		jobMap.add(makeJob(JOB_ID_3, true));

		comparator = new RuleJobComparator(jobMap);

		List<RuleJob> ruleJobs = Lists.newArrayList();
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_1, JOB_ID_1, 2L));
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_2, JOB_ID_2, 0L));
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_3, JOB_ID_3, 1L));

		ruleJobs.sort(comparator);

		assertEquals(RULE_JOB_ID_2, (Long) ruleJobs.get(0).getJobId());
		assertEquals(RULE_JOB_ID_3, (Long) ruleJobs.get(1).getJobId());
		assertEquals(RULE_JOB_ID_1, (Long) ruleJobs.get(2).getJobId());
	}
	
	@Test
	public void compare_CollationSequenceSetAllActions_OrderedByCollation() {
		jobMap = EntityMap.newEntityMap();
		jobMap.add(makeJob(JOB_ID_1, false));
		jobMap.add(makeJob(JOB_ID_2, false));
		jobMap.add(makeJob(JOB_ID_3, false));

		comparator = new RuleJobComparator(jobMap);

		List<RuleJob> ruleJobs = Lists.newArrayList();
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_1, JOB_ID_1, 2L));
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_2, JOB_ID_2, 0L));
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_3, JOB_ID_3, 1L));

		ruleJobs.sort(comparator);

		assertEquals(RULE_JOB_ID_2, (Long) ruleJobs.get(0).getJobId());
		assertEquals(RULE_JOB_ID_3, (Long) ruleJobs.get(1).getJobId());
		assertEquals(RULE_JOB_ID_1, (Long) ruleJobs.get(2).getJobId());
	}
	
	@Test
	public void compare_CollationSequenceActionsAndConditions_ConditionsStillPulledFirst() {
		jobMap = EntityMap.newEntityMap();
		jobMap.add(makeJob(JOB_ID_1, true));
		jobMap.add(makeJob(JOB_ID_2, false));
		jobMap.add(makeJob(JOB_ID_3, true));

		comparator = new RuleJobComparator(jobMap);

		List<RuleJob> ruleJobs = Lists.newArrayList();
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_1, JOB_ID_1, 2L));
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_2, JOB_ID_2, 0L));
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_3, JOB_ID_3, 1L));

		ruleJobs.sort(comparator);
		
		assertEquals(RULE_JOB_ID_3, (Long) ruleJobs.get(0).getJobId());
		assertEquals(RULE_JOB_ID_1, (Long) ruleJobs.get(1).getJobId());
		assertEquals(RULE_JOB_ID_2, (Long) ruleJobs.get(2).getJobId());
	}
	
	@Test
	public void compare_NullCollationSequence_NullsOrderedLast() {
		jobMap = EntityMap.newEntityMap();
		jobMap.add(makeJob(JOB_ID_1, true));
		jobMap.add(makeJob(JOB_ID_2, true));
		jobMap.add(makeJob(JOB_ID_3, true));

		comparator = new RuleJobComparator(jobMap);

		List<RuleJob> ruleJobs = Lists.newArrayList();
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_1, JOB_ID_1, null));
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_2, JOB_ID_2, 0L));
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_3, JOB_ID_3, null));

		ruleJobs.sort(comparator);
		
		assertEquals(RULE_JOB_ID_2, (Long) ruleJobs.get(0).getJobId());
		assertEquals(RULE_JOB_ID_1, (Long) ruleJobs.get(1).getJobId());
		assertEquals(RULE_JOB_ID_3, (Long) ruleJobs.get(2).getJobId());
	}

	private RuleJob makeRuleJob(Long ruleJobId, Long jobId) {
		return makeRuleJob(ruleJobId, jobId, null);
	}
	
	private RuleJob makeRuleJob(Long ruleJobId, Long jobId, Long collationSeq) {
		RuleJob ruleJob = new RuleJob();
		ruleJob.setRuleJobId(ruleJobId);
		ruleJob.setJobId(jobId);
		ruleJob.setCollationSeq(collationSeq);
		return ruleJob;
	}

	private Job makeJob(Long jobId, boolean conditionInd) {
		Job job = new Job();
		job.setJobId(jobId);
		job.setConditionInd(conditionInd);
		return job;
	}
}
