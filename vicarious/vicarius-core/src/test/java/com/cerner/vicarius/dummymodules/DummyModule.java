package com.cerner.vicarius.dummymodules;

import com.cerner.vicarius.module.ModuleSpec;

public class DummyModule extends ModuleSpec {
	
	private static int invokeStartupCount = 0;
	private static int invokeShutdownCount = 0;
	
	public static int getStartupInvokeCount() {
		return invokeStartupCount;
	}
	
	public static int getShutdownInvokeCount() {
		return invokeShutdownCount;
	}

	public static void resetStartupInvokeCount() {
		invokeStartupCount = 0;
	}
	
	public static void resetShutdownInvokeCount() {
		invokeShutdownCount = 0;
	}
	
	public String getPropertyWrap(String propertyName) {
		return getProperty(propertyName);
	}
	
	@Override
	public void startup() {
		invokeStartupCount++;
	}

	@Override
	public void shutdown() {
		invokeShutdownCount++;
	}
}
