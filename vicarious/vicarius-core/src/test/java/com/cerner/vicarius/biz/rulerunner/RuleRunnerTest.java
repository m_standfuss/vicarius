package com.cerner.vicarius.biz.rulerunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyCollectionOf;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import com.cerner.vicarius.data.access.JobDao;
import com.cerner.vicarius.data.access.JobPropertyDao;
import com.cerner.vicarius.data.access.RuleDao;
import com.cerner.vicarius.data.access.RuleJobDao;
import com.cerner.vicarius.data.access.RuleJobParameterDao;
import com.cerner.vicarius.data.access.RuleJobSaveDao;
import com.cerner.vicarius.data.model.Job;
import com.cerner.vicarius.data.model.JobProperty;
import com.cerner.vicarius.data.model.PropertyIOType;
import com.cerner.vicarius.data.model.PropertyType;
import com.cerner.vicarius.data.model.Rule;
import com.cerner.vicarius.data.model.RuleJob;
import com.cerner.vicarius.data.model.RuleJobParameter;
import com.cerner.vicarius.data.model.RuleJobParameter.ParameterType;
import com.cerner.vicarius.data.model.RuleJobSave;
import com.cerner.vicarius.dummymodules.DummyAction;
import com.cerner.vicarius.dummymodules.DummyActionException;
import com.cerner.vicarius.dummymodules.DummyActionWithInputProperty;
import com.cerner.vicarius.dummymodules.DummyActionWithOutputProperty;
import com.cerner.vicarius.dummymodules.DummyActionWithPrimitiveProperties;
import com.cerner.vicarius.dummymodules.DummyBaselineJob;
import com.cerner.vicarius.dummymodules.DummyConditionalCallsGetLastCompleted;
import com.cerner.vicarius.dummymodules.DummyConditionalCallsGetSave;
import com.cerner.vicarius.dummymodules.DummyConditionalCallsSaveState;
import com.cerner.vicarius.dummymodules.DummyConditionalException;
import com.cerner.vicarius.dummymodules.DummyConditionalFail;
import com.cerner.vicarius.dummymodules.DummyConditionalPass;
import com.cerner.vicarius.dummymodules.DummyConditionalWithPrimitiveProperties;
import com.cerner.vicarius.dummymodules.DummyModule;
import com.cerner.vicarius.module.ModuleSpec;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class RuleRunnerTest {

	private static final Long RULE_ID = 1L;
	private static final Date RULE_LAST_COMPLETED = new Date(100);
	private static final Date RULE_LAST_RAN = new Date(1000);

	private static final Long JOB_ACTION_ID = 10L;
	private static final Long JOB_ACTION_EXCEPTION_ID = 11L;
	private static final Long JOB_ACTION_WITH_PRIMITIVE_PROPS_ID = 12L;

	private static final Long JOB_CONDITIONAL_PASS_ID = 13L;
	private static final Long JOB_CONDITIONAL_FAIL_ID = 14L;
	private static final Long JOB_CONDITIONAL_EXCEPTION_ID = 15L;
	private static final Long JOB_CONDITIONAL_WITH_PRIMITIVE_PROPS_ID = 16L;

	private static final Long JOB_CONDITIONAL_CALLS_GET_LAST_COMPLETED_ID = 17L;
	private static final Long JOB_CONDITIONAL_CALLS_SAVE_STATE_ID = 18L;
	private static final Long JOB_CONDITIONAL_CALLS_GET_SAVE_STATE_ID = 19L;

	private static final Long JOB_BASELINE_ID = 20L;
	private static final Long JOB_BASELINE_CALLS_SAVE_ID = 21L;

	private static final Long JOB_CLASSDOESNTEXIST_ID = 22L;

	private static final Long JOB_WITH_OUTPUT_ID = 23L;
	private static final Long JOB_WITH_INPUT_ID = 24L;

	private static final Long JOB_WITH_REQUIRED_INPUT_PROP = 25L;

	private static final Long MODULE_ID_1 = 100L;

	private static final String JOB_ACTION_CLASSNAME = "com.cerner.vicarius.dummymodules.DummyAction";
	private static final String JOB_ACTION_EXCEPTION_CLASSNAME = "com.cerner.vicarius.dummymodules.DummyActionException";
	private static final String JOB_ACTION_WITH_PROPS_CLASSNAME = "com.cerner.vicarius.dummymodules.DummyActionWithPrimitiveProperties";

	private static final String JOB_CONDITIONAL_PASS_CLASSNAME = "com.cerner.vicarius.dummymodules.DummyConditionalPass";
	private static final String JOB_CONDITIONAL_FAIL_CLASSNAME = "com.cerner.vicarius.dummymodules.DummyConditionalFail";
	private static final String JOB_CONDITIONAL_EXCEPTION_CLASSNAME = "com.cerner.vicarius.dummymodules.DummyConditionalException";
	private static final String JOB_CONDITIONAL_WITH_PROPS_CLASSNAME = "com.cerner.vicarius.dummymodules.DummyConditionalWithPrimitiveProperties";

	private static final String JOB_CONDITIONAL_CALLS_GET_LAST_COMPLETED_CLASSNAME = "com.cerner.vicarius.dummymodules.DummyConditionalCallsGetLastCompleted";
	private static final String JOB_CONDITIONAL_CALLS_SAVE_STATE_CLASSNAME = "com.cerner.vicarius.dummymodules.DummyConditionalCallsSaveState";
	private static final String JOB_CONDITIONAL_CALLS_GET_SAVE_STATE_CLASSNAME = "com.cerner.vicarius.dummymodules.DummyConditionalCallsGetSave";

	private static final String JOB_BASELINE_IMPLEMENTATION_CLASSNAME = "com.cerner.vicarius.dummymodules.DummyBaselineJob";
	private static final String JOB_BASELINE_CALLS_SAVE_CLASSNAME = "com.cerner.vicarius.dummymodules.DummyBaselineCallsSaveJob";

	private static final String JOB_DOESNTEXIST_CLASSNAME = "ClassNameDoesntExist";

	private static final String JOB_WITH_OUTPUT_CLASSNAME = "com.cerner.vicarius.dummymodules.DummyActionWithOutputProperty";
	private static final String JOB_WITH_INPUT_CLASSNAME = "com.cerner.vicarius.dummymodules.DummyActionWithInputProperty";

	private static final String JOB_PROPERTY_NAME_BOOLEAN = "boolean_";
	private static final String JOB_PROPERTY_NAME_INTEGER = "integer_";
	private static final String JOB_PROPERTY_NAME_LONG = "long_";
	private static final String JOB_PROPERTY_NAME_STRING = "string_";
	private static final String JOB_PROPERTY_NAME_DOUBLE = "double_";
	private static final String JOB_PROPERTY_NAME_FLOAT = "float_";
	private static final String JOB_PROPERTY_NAME_INPUT = "input";
	private static final String JOB_PROPERTY_NAME_OUTPUT = "output";

	private static final String JOB_PROPERTY_METHOD_BOOLEAN = "setBoolean";
	private static final String JOB_PROPERTY_METHOD_INTEGER = "setInteger";
	private static final String JOB_PROPERTY_METHOD_LONG = "setLong";
	private static final String JOB_PROPERTY_METHOD_STRING = "setString";
	private static final String JOB_PROPERTY_METHOD_DOUBLE = "setDouble";
	private static final String JOB_PROPERTY_METHOD_FLOAT = "setFloat";
	private static final String JOB_PROPERTY_METHOD_INPUT = "setInput";
	private static final String JOB_PROPERTY_METHOD_OUTPUT = "getOutput";

	private static final Long RULE_JOB_ID_1 = 1L;
	private static final Long RULE_JOB_ID_2 = 2L;
	private static final Long RULE_JOB_ID_3 = 3L;
	private static final Long RULE_JOB_ID_4 = 4L;

	private static final String RULE_JOB_PARAMETER_INPUT_FROM_OUTPUT = "{\"ruleJobId\":" + RULE_JOB_ID_1
			+ ",\"parameterName\":\"output\"}";
	private static final String INVALID_JSON = "{thisIsNotJson]";

	private static final String RULE_JOB_SAVED_STATE = "RULE_JOB_SAVED_STATE";

	@Before
	public void setup() {
		RuleRunner.setModuleSpecMap(makeModuleSpecMap());

		DummyConditionalCallsSaveState.setSaveState(RULE_JOB_SAVED_STATE);
		DummyConditionalCallsGetSave.clearLastSaveState();
		DummyConditionalCallsGetLastCompleted.clearLastCompletedSet();

		DummyAction.resetInvokeCount();
		DummyActionException.resetInvokeCount();
		DummyActionWithPrimitiveProperties.resetInvokeCount();
		DummyConditionalException.resetInvokeCount();
		DummyConditionalFail.resetInvokeCount();
		DummyConditionalPass.resetInvokeCount();
		DummyConditionalWithPrimitiveProperties.resetInvokeCount();

		DummyBaselineJob.resetInvokeCounts();

		DummyActionWithInputProperty.clearInput();
		DummyActionWithOutputProperty.clearOutput();
	}

	@AfterClass
	public static void tearDownClass() {
		RuleRunner.setModuleSpecMap(null);
		DummyConditionalCallsSaveState.setSaveState(null);
		DummyConditionalCallsGetSave.clearLastSaveState();
		DummyConditionalCallsGetLastCompleted.clearLastCompletedSet();

		DummyAction.resetInvokeCount();
		DummyActionException.resetInvokeCount();
		DummyActionWithPrimitiveProperties.resetInvokeCount();
		DummyConditionalException.resetInvokeCount();
		DummyConditionalFail.resetInvokeCount();
		DummyConditionalPass.resetInvokeCount();
		DummyConditionalWithPrimitiveProperties.resetInvokeCount();

		DummyBaselineJob.resetInvokeCounts();
	}

	@Test(expected = RuntimeException.class)
	public void run_NoRuleFound_ExceptionThrown() {
		RuleDao ruleDao = makeRuleDao(null);

		RuleRunner runner = makeRuleRunner(ruleDao);
		runner.run();
	}

	@Test
	public void run_NoRuleJobs_ReturnedSuccess() {
		RuleJobDao ruleJobDao = makeRuleJobDao();
		when(ruleJobDao.findByRule(RULE_ID)).thenReturn(new ArrayList<>());

		RuleRunner runner = makeRuleRunner(ruleJobDao);
		assertTrue(runner.run());
	}

	@Test
	public void run_HappyPath_MethodsCalled() {
		RuleRunner runner = makeRuleRunner();
		runner.run();

		assertEquals(1, DummyAction.getInvokeCount());
		assertEquals(1, DummyConditionalPass.getInvokeCount());
		assertEquals(1, DummyActionWithPrimitiveProperties.getInvokeCount());
		assertEquals(1, DummyConditionalWithPrimitiveProperties.getInvokeCount());
	}

	@Test
	public void run_HappyPath_SuccessReturned() {
		RuleRunner runner = makeRuleRunner();
		assertTrue(runner.run());
	}

	@Test
	public void run_OneActionRuleJob_RanOnce() {
		List<RuleJob> ruleJobs = Lists.newArrayList();
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_1, RULE_ID, JOB_ACTION_ID));

		RuleJobDao ruleJobDao = makeRuleJobDao();
		when(ruleJobDao.findByRule(RULE_ID)).thenReturn(ruleJobs);

		RuleRunner runner = makeRuleRunner(ruleJobDao);
		runner.run();

		assertEquals(1, DummyAction.getInvokeCount());
	}

	@Test
	public void run_ConditionalRuleJobReturnsFalse_FalseReturned() {
		List<RuleJob> ruleJobs = Lists.newArrayList();
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_1, RULE_ID, JOB_CONDITIONAL_FAIL_ID));

		RuleJobDao ruleJobDao = makeRuleJobDao();
		when(ruleJobDao.findByRule(RULE_ID)).thenReturn(ruleJobs);

		RuleRunner runner = makeRuleRunner(ruleJobDao);
		assertFalse(runner.run());
	}

	@Test
	public void run_ActionsAfterConditionalFails_NotRan() {
		List<RuleJob> ruleJobs = Lists.newArrayList();
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_1, RULE_ID, JOB_CONDITIONAL_FAIL_ID));
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_2, RULE_ID, JOB_ACTION_ID));

		RuleJobDao ruleJobDao = makeRuleJobDao();
		when(ruleJobDao.findByRule(RULE_ID)).thenReturn(ruleJobs);

		RuleRunner runner = makeRuleRunner(ruleJobDao);
		runner.run();

		assertEquals(1, DummyConditionalFail.getInvokeCount());
		assertEquals(0, DummyAction.getInvokeCount());
	}

	@Test
	public void run_ConditionsAfterConditionFails_NotRan() {
		List<RuleJob> ruleJobs = Lists.newArrayList();
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_1, RULE_ID, JOB_CONDITIONAL_FAIL_ID, 0l));
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_2, RULE_ID, JOB_CONDITIONAL_PASS_ID, 1l));

		RuleJobDao ruleJobDao = makeRuleJobDao();
		when(ruleJobDao.findByRule(RULE_ID)).thenReturn(ruleJobs);

		RuleRunner runner = makeRuleRunner(ruleJobDao);
		runner.run();

		assertEquals(1, DummyConditionalFail.getInvokeCount());
		assertEquals(0, DummyConditionalPass.getInvokeCount());
	}

	@Test
	public void run_ConditionInvokesSaveState_ExistingRowsDeleted() {
		List<RuleJob> ruleJobs = Lists.newArrayList();
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_1, RULE_ID, JOB_CONDITIONAL_CALLS_SAVE_STATE_ID));

		RuleJobDao ruleJobDao = makeRuleJobDao();
		when(ruleJobDao.findByRule(RULE_ID)).thenReturn(ruleJobs);

		RuleJobSaveDao ruleJobSaveDao = makeRuleJobSaveDao();

		RuleRunner runner = makeRuleRunner(ruleJobDao, ruleJobSaveDao);
		runner.run();

		verify(ruleJobSaveDao, times(1)).deleteByRuleJob(RULE_JOB_ID_1);
	}

	@Test
	public void run_ConditionInvokesSaveStateRuleCompletes_NewRowInserted() {
		List<RuleJob> ruleJobs = Lists.newArrayList();
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_1, RULE_ID, JOB_CONDITIONAL_CALLS_SAVE_STATE_ID));

		RuleJobDao ruleJobDao = makeRuleJobDao();
		when(ruleJobDao.findByRule(RULE_ID)).thenReturn(ruleJobs);

		RuleJobSaveDao ruleJobSaveDao = makeRuleJobSaveDao();

		RuleRunner runner = makeRuleRunner(ruleJobDao, ruleJobSaveDao);
		runner.run();

		ArgumentCaptor<RuleJobSave> captor = ArgumentCaptor.forClass(RuleJobSave.class);
		verify(ruleJobSaveDao, times(1)).persist(captor.capture());

		RuleJobSave captured = captor.getValue();
		assertEquals(RULE_JOB_SAVED_STATE, captured.getContent());
		assertEquals(RULE_JOB_ID_1, (Long) captured.getRuleJobId());
	}

	@Test
	public void run_ConditionInvokesSaveStateRuleDoesntComplete_NewRowNotInserted() {
		List<RuleJob> ruleJobs = Lists.newArrayList();
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_1, RULE_ID, JOB_CONDITIONAL_CALLS_SAVE_STATE_ID));
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_2, RULE_ID, JOB_CONDITIONAL_FAIL_ID));

		RuleJobDao ruleJobDao = makeRuleJobDao();
		when(ruleJobDao.findByRule(RULE_ID)).thenReturn(ruleJobs);

		RuleJobSaveDao ruleJobSaveDao = makeRuleJobSaveDao();

		RuleRunner runner = makeRuleRunner(ruleJobDao, ruleJobSaveDao);
		runner.run();

		verify(ruleJobSaveDao, times(0)).persist(any(RuleJobSave.class));
	}

	@Test
	public void run_ConditionInvokesGetSaveState_ValueReturned() {
		List<RuleJob> ruleJobs = Lists.newArrayList();
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_1, RULE_ID, JOB_CONDITIONAL_CALLS_GET_SAVE_STATE_ID, null));

		RuleJobDao ruleJobDao = makeRuleJobDao();
		when(ruleJobDao.findByRule(RULE_ID)).thenReturn(ruleJobs);

		RuleJobSaveDao ruleJobSaveDao = makeRuleJobSaveDao();

		RuleRunner runner = makeRuleRunner(ruleJobDao, ruleJobSaveDao);
		runner.run();

		assertEquals(RULE_JOB_SAVED_STATE, DummyConditionalCallsGetSave.getLastSaveStateSet());
	}

	@Test
	public void run_ConditionInvokesGetSaveStateNoRow_NullReturned() {
		List<RuleJob> ruleJobs = Lists.newArrayList();
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_1, RULE_ID, JOB_CONDITIONAL_CALLS_GET_SAVE_STATE_ID, null));

		RuleJobDao ruleJobDao = makeRuleJobDao();
		when(ruleJobDao.findByRule(RULE_ID)).thenReturn(ruleJobs);

		RuleJobSaveDao ruleJobSaveDao = makeRuleJobSaveDao();
		when(ruleJobSaveDao.findMostRecentByRuleJobId(RULE_ID)).thenReturn(null);

		RuleRunner runner = makeRuleRunner(ruleJobDao, ruleJobSaveDao);
		runner.run();

		assertNull(DummyConditionalCallsGetSave.getLastSaveStateSet());
	}

	@Test
	public void run_ConditionInvokesGetLastCompleted_ValueReturned() {
		List<RuleJob> ruleJobs = Lists.newArrayList();

		ruleJobs.add(makeRuleJob(RULE_JOB_ID_1, RULE_ID, JOB_CONDITIONAL_CALLS_GET_LAST_COMPLETED_ID, null));

		RuleJobDao ruleJobDao = makeRuleJobDao();
		when(ruleJobDao.findByRule(RULE_ID)).thenReturn(ruleJobs);

		RuleJobSaveDao ruleJobSaveDao = makeRuleJobSaveDao();

		RuleRunner runner = makeRuleRunner(ruleJobDao, ruleJobSaveDao);
		runner.run();

		assertEquals(RULE_LAST_COMPLETED, DummyConditionalCallsGetLastCompleted.getLastCompletedSet());
	}

	@Test(expected = RuntimeException.class)
	public void run_RuleJobMissingRequiredParam_ExceptionThrown() {
		List<RuleJob> ruleJobs = Lists.newArrayList();
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_1, RULE_ID, JOB_WITH_REQUIRED_INPUT_PROP));

		RuleJobDao ruleJobDao = makeRuleJobDao();
		when(ruleJobDao.findByRule(eq(RULE_ID))).thenReturn(ruleJobs);

		RuleRunner runner = makeRuleRunner(ruleJobDao);
		runner.run();
	}

	@Test(expected = RuntimeException.class)
	public void run_RuleJobUnableToParseRequiredParam_ExceptionThrown() {
		List<RuleJob> ruleJobs = Lists.newArrayList();
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_1, RULE_ID, JOB_WITH_REQUIRED_INPUT_PROP));

		List<RuleJobParameter> ruleJobParameters = Lists.newArrayList();
		ruleJobParameters.add(makeRuleJobParameter(RULE_JOB_ID_1, ParameterType.OUTPUT_PARAMETER,
				JOB_PROPERTY_NAME_INPUT, INVALID_JSON));

		RuleJobDao ruleJobDao = makeRuleJobDao();
		when(ruleJobDao.findByRule(eq(RULE_ID))).thenReturn(ruleJobs);

		RuleJobParameterDao ruleJobParameterDao = makeRuleJobParameterDao();
		when(ruleJobParameterDao.findByRuleJobs(anyCollectionOf(Long.class))).thenReturn(ruleJobParameters);

		RuleRunner runner = makeRuleRunner(ruleJobDao, ruleJobParameterDao);
		assertFalse(runner.run());
		assertEquals(0, DummyAction.getInvokeCount());
	}

	@Test
	public void run_RuleJobMissingNonRequiredParameters_ExecutionContinues() {
		List<RuleJob> ruleJobs = Lists.newArrayList();
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_1, RULE_ID, JOB_ACTION_WITH_PRIMITIVE_PROPS_ID, 0L));
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_2, RULE_ID, JOB_ACTION_ID, 1L));

		RuleJobDao ruleJobDao = makeRuleJobDao();
		when(ruleJobDao.findByRule(eq(RULE_ID))).thenReturn(ruleJobs);

		RuleJobParameterDao ruleJobParameterDao = makeRuleJobParameterDao();
		when(ruleJobParameterDao.findByRuleJobs(anyCollectionOf(Long.class))).thenReturn(new ArrayList<>());

		RuleRunner runner = makeRuleRunner(ruleJobDao, ruleJobParameterDao);
		assertTrue(runner.run());
		assertEquals(1, DummyAction.getInvokeCount());
	}

	@Test(expected = RuntimeException.class)
	public void run_JobsMethodDoesntExistRequiredProperty_ExceptionThrown() {

		List<RuleJob> ruleJobs = Lists.newArrayList();
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_1, RULE_ID, JOB_ACTION_WITH_PRIMITIVE_PROPS_ID));

		RuleJobDao ruleJobDao = makeRuleJobDao();
		when(ruleJobDao.findByRule(eq(RULE_ID))).thenReturn(ruleJobs);

		List<JobProperty> jobProperties = Lists.newArrayList();
		jobProperties.add(makeJobProperty(JOB_ACTION_WITH_PRIMITIVE_PROPS_ID, "notARealMethod",
				JOB_PROPERTY_NAME_BOOLEAN, PropertyType.BOOLEAN, true, PropertyIOType.INPUT));

		JobPropertyDao jobPropertyDao = makeJobPropertyDao();
		when(jobPropertyDao.findByJobIds(anyCollectionOf(Long.class))).thenReturn(jobProperties);

		RuleRunner runner = makeRuleRunner(ruleJobDao, jobPropertyDao);
		runner.run();
	}

	@Test
	public void run_JobsMethodDoesntExistNonRequiredProperty_ExecutionContinues() {
		List<RuleJob> ruleJobs = Lists.newArrayList();
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_1, RULE_ID, JOB_ACTION_WITH_PRIMITIVE_PROPS_ID, 0L));
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_2, RULE_ID, JOB_ACTION_ID, 1L));

		RuleJobDao ruleJobDao = makeRuleJobDao();
		when(ruleJobDao.findByRule(eq(RULE_ID))).thenReturn(ruleJobs);

		List<JobProperty> jobProperties = Lists.newArrayList();
		jobProperties.add(makeJobProperty(JOB_ACTION_WITH_PRIMITIVE_PROPS_ID, "notARealMethod",
				JOB_PROPERTY_NAME_BOOLEAN, PropertyType.BOOLEAN, false, PropertyIOType.INPUT));

		JobPropertyDao jobPropertyDao = makeJobPropertyDao();
		when(jobPropertyDao.findByJobIds(anyCollectionOf(Long.class))).thenReturn(jobProperties);

		RuleRunner runner = makeRuleRunner(ruleJobDao, jobPropertyDao);
		assertTrue(runner.run());
		assertEquals(1, DummyAction.getInvokeCount());
	}

	@Test(expected = RuntimeException.class)
	public void run_JobDoesntExist_ExceptionThrown() {
		JobDao jobDao = makeJobDao();
		when(jobDao.findByIds(anyCollectionOf(Long.class))).thenReturn(new ArrayList<>());

		RuleRunner runner = makeRuleRunner(jobDao);
		runner.run();
	}

	@Test(expected = RuntimeException.class)
	public void run_JobClassDoesntExist_ExceptionThrown() {
		List<RuleJob> ruleJobs = Lists.newArrayList();
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_1, RULE_ID, JOB_CLASSDOESNTEXIST_ID));

		RuleJobDao ruleJobDao = makeRuleJobDao();
		when(ruleJobDao.findByRule(eq(RULE_ID))).thenReturn(ruleJobs);

		RuleRunner runner = makeRuleRunner(ruleJobDao);
		runner.run();
	}

	@Test(expected = RuntimeException.class)
	public void run_ConditionalJobClassDoesntExtend_ExceptionThrown() {
		List<Job> jobs = Lists.newArrayList();
		jobs.add(makeJob(JOB_CONDITIONAL_PASS_ID, MODULE_ID_1, JOB_ACTION_CLASSNAME, true));

		JobDao jobDao = makeJobDao();
		when(jobDao.findByIds(anyCollectionOf(Long.class))).thenReturn(jobs);

		List<RuleJob> ruleJobs = Lists.newArrayList();
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_1, RULE_ID, JOB_CONDITIONAL_PASS_ID));

		RuleJobDao ruleJobDao = makeRuleJobDao();
		when(ruleJobDao.findByRule(eq(RULE_ID))).thenReturn(ruleJobs);

		RuleRunner runner = makeRuleRunner(jobDao, ruleJobDao);
		runner.run();
	}

	@Test(expected = RuntimeException.class)
	public void run_ActionJobClassDoesntExtend_ExceptionThrown() {
		List<Job> jobs = Lists.newArrayList();
		jobs.add(makeJob(JOB_ACTION_ID, MODULE_ID_1, JOB_CONDITIONAL_PASS_CLASSNAME, false));

		JobDao jobDao = makeJobDao();
		when(jobDao.findByIds(anyCollectionOf(Long.class))).thenReturn(jobs);

		List<RuleJob> ruleJobs = Lists.newArrayList();
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_1, RULE_ID, JOB_ACTION_ID));

		RuleJobDao ruleJobDao = makeRuleJobDao();
		when(ruleJobDao.findByRule(eq(RULE_ID))).thenReturn(ruleJobs);

		RuleRunner runner = makeRuleRunner(jobDao, ruleJobDao);
		runner.run();
	}

	@Test(expected = RuntimeException.class)
	public void run_MissingModule_ExceptionThrown() {
		RuleRunner.setModuleSpecMap(null);
		RuleRunner runner = makeRuleRunner();
		runner.run();
	}

	@Test
	public void run_NeverRanHasBaselines_BaselineJobsRan() {
		RuleDao ruleDao = makeRuleDao(makeRule(RULE_ID, null));

		List<Job> jobs = Lists.newArrayList();
		jobs.add(makeJob(JOB_BASELINE_ID, MODULE_ID_1, JOB_BASELINE_IMPLEMENTATION_CLASSNAME, true));

		JobDao jobDao = makeJobDao();
		when(jobDao.findByIds(anyCollectionOf(Long.class))).thenReturn(jobs);

		List<RuleJob> ruleJobs = Lists.newArrayList();
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_1, RULE_ID, JOB_BASELINE_ID));

		RuleJobDao ruleJobDao = makeRuleJobDao();
		when(ruleJobDao.findByRule(eq(RULE_ID))).thenReturn(ruleJobs);

		RuleRunner runner = makeRuleRunner(ruleDao, jobDao, ruleJobDao);
		runner.run();
		assertEquals(1, DummyBaselineJob.getBaselineInvokeCount());
		assertEquals(0, DummyBaselineJob.getRunInvokeCount());
	}

	@Test
	public void run_NeverRanHasBaselinesAndNonBaselines_OnlyBaselineJobsRan() {
		RuleDao ruleDao = makeRuleDao(makeRule(RULE_ID, null));

		List<Job> jobs = Lists.newArrayList();
		jobs.add(makeJob(JOB_ACTION_ID, MODULE_ID_1, JOB_ACTION_CLASSNAME, false));
		jobs.add(makeJob(JOB_BASELINE_ID, MODULE_ID_1, JOB_BASELINE_IMPLEMENTATION_CLASSNAME, true));

		JobDao jobDao = makeJobDao();
		when(jobDao.findByIds(anyCollectionOf(Long.class))).thenReturn(jobs);

		List<RuleJob> ruleJobs = Lists.newArrayList();
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_1, RULE_ID, JOB_ACTION_ID));
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_2, RULE_ID, JOB_BASELINE_ID));

		RuleJobDao ruleJobDao = makeRuleJobDao();
		when(ruleJobDao.findByRule(eq(RULE_ID))).thenReturn(ruleJobs);

		RuleRunner runner = makeRuleRunner(ruleDao, jobDao, ruleJobDao);
		runner.run();
		assertEquals(1, DummyBaselineJob.getBaselineInvokeCount());
		assertEquals(0, DummyBaselineJob.getRunInvokeCount());
		assertEquals(0, DummyAction.getInvokeCount());
	}

	@Test
	public void run_HasBeenRanHasBaselines_BaselinesRunCalled() {
		List<Job> jobs = Lists.newArrayList();
		jobs.add(makeJob(JOB_ACTION_ID, MODULE_ID_1, JOB_ACTION_CLASSNAME, false));
		jobs.add(makeJob(JOB_BASELINE_ID, MODULE_ID_1, JOB_BASELINE_IMPLEMENTATION_CLASSNAME, true));

		JobDao jobDao = makeJobDao();
		when(jobDao.findByIds(anyCollectionOf(Long.class))).thenReturn(jobs);

		List<RuleJob> ruleJobs = Lists.newArrayList();
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_1, RULE_ID, JOB_ACTION_ID));
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_2, RULE_ID, JOB_BASELINE_ID));

		RuleJobDao ruleJobDao = makeRuleJobDao();
		when(ruleJobDao.findByRule(eq(RULE_ID))).thenReturn(ruleJobs);

		RuleRunner runner = makeRuleRunner(jobDao, ruleJobDao);
		runner.run();
		assertEquals(0, DummyBaselineJob.getBaselineInvokeCount());
		assertEquals(1, DummyBaselineJob.getRunInvokeCount());
		assertEquals(1, DummyAction.getInvokeCount());
	}

	@Test
	public void run_NeverRanDoesntHaveBaselines_JobsCalledNormally() {
		RuleDao ruleDao = makeRuleDao(makeRule(RULE_ID, null));

		List<Job> jobs = Lists.newArrayList();
		jobs.add(makeJob(JOB_ACTION_ID, MODULE_ID_1, JOB_ACTION_CLASSNAME, false));
		jobs.add(makeJob(JOB_CONDITIONAL_PASS_ID, MODULE_ID_1, JOB_CONDITIONAL_PASS_CLASSNAME, true));

		JobDao jobDao = makeJobDao();
		when(jobDao.findByIds(anyCollectionOf(Long.class))).thenReturn(jobs);

		List<RuleJob> ruleJobs = Lists.newArrayList();
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_1, RULE_ID, JOB_ACTION_ID));
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_2, RULE_ID, JOB_CONDITIONAL_PASS_ID));

		RuleJobDao ruleJobDao = makeRuleJobDao();
		when(ruleJobDao.findByRule(eq(RULE_ID))).thenReturn(ruleJobs);

		RuleRunner runner = makeRuleRunner(ruleDao, jobDao, ruleJobDao);
		runner.run();
		assertEquals(1, DummyConditionalPass.getInvokeCount());
		assertEquals(1, DummyAction.getInvokeCount());
	}

	@Test
	public void run_CompletedJob_CompletedDateAndLastRanUpdated() {
		RuleDao ruleDao = makeRuleDao();
		List<Job> jobs = Lists.newArrayList();
		jobs.add(makeJob(JOB_CONDITIONAL_FAIL_ID, MODULE_ID_1, JOB_CONDITIONAL_FAIL_CLASSNAME, true));

		JobDao jobDao = makeJobDao();
		when(jobDao.findByIds(anyCollectionOf(Long.class))).thenReturn(jobs);

		List<RuleJob> ruleJobs = Lists.newArrayList();
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_1, RULE_ID, JOB_CONDITIONAL_FAIL_ID));

		RuleJobDao ruleJobDao = makeRuleJobDao();
		when(ruleJobDao.findByRule(eq(RULE_ID))).thenReturn(ruleJobs);

		RuleRunner runner = makeRuleRunner(ruleDao, jobDao, ruleJobDao);
		runner.run();

		ArgumentCaptor<Rule> argumentCaptor = ArgumentCaptor.forClass(Rule.class);
		verify(ruleDao, times(1)).persist(argumentCaptor.capture());

		Rule rule = argumentCaptor.getValue();
		assertTrue("Expecting the last completed datetime to be unchanged.",
				rule.getLastCompleted().getTime() == RULE_LAST_COMPLETED.getTime());
		assertTrue("Expecting the last completed datetime to be updated.",
				rule.getLastRan().getTime() > RULE_LAST_COMPLETED.getTime());
	}

	@Test
	public void run_UncompletedJob_OnlyLastRanUpdated() {
		RuleDao ruleDao = makeRuleDao();

		RuleRunner runner = makeRuleRunner(ruleDao);
		runner.run();

		ArgumentCaptor<Rule> argumentCaptor = ArgumentCaptor.forClass(Rule.class);
		verify(ruleDao, times(1)).persist(argumentCaptor.capture());

		Rule rule = argumentCaptor.getValue();
		assertTrue("Expecting the last completed datetime to be updated.",
				rule.getLastCompleted().getTime() > RULE_LAST_COMPLETED.getTime());
		assertTrue("Expecting the last completed datetime to be updated.",
				rule.getLastRan().getTime() > RULE_LAST_COMPLETED.getTime());
	}

	@Test
	public void run_BaselineJobNeverRan_SaveStateSaved() {
		RuleDao ruleDao = makeRuleDao(makeRule(RULE_ID, null));

		List<RuleJob> ruleJobs = Lists.newArrayList();
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_1, RULE_ID, JOB_CONDITIONAL_FAIL_ID));
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_2, RULE_ID, JOB_BASELINE_CALLS_SAVE_ID));

		RuleJobDao ruleJobDao = makeRuleJobDao();
		when(ruleJobDao.findByRule(eq(RULE_ID))).thenReturn(ruleJobs);

		RuleJobSaveDao ruleJobSaveDao = makeRuleJobSaveDao();

		RuleRunner runner = makeRuleRunner(ruleDao, ruleJobDao, ruleJobSaveDao);
		runner.run();

		verify(ruleJobSaveDao, times(1)).persist(any(RuleJobSave.class));
	}

	@Test
	public void run_InputParameterFromPreviousOutput_InputSetWithOutput() {
		List<RuleJob> ruleJobs = Lists.newArrayList();
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_1, RULE_ID, JOB_WITH_OUTPUT_ID, 0L));
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_2, RULE_ID, JOB_WITH_INPUT_ID, 1L));

		List<RuleJobParameter> ruleJobParameters = Lists.newArrayList();
		ruleJobParameters.add(makeRuleJobParameter(RULE_JOB_ID_2, ParameterType.OUTPUT_PARAMETER,
				JOB_PROPERTY_NAME_INPUT, RULE_JOB_PARAMETER_INPUT_FROM_OUTPUT));

		RuleJobDao ruleJobDao = makeRuleJobDao();
		when(ruleJobDao.findByRule(eq(RULE_ID))).thenReturn(ruleJobs);

		RuleJobParameterDao ruleJobParameterDao = makeRuleJobParameterDao();
		when(ruleJobParameterDao.findByRuleJobs(anyCollectionOf(Long.class))).thenReturn(ruleJobParameters);

		RuleRunner runner = makeRuleRunner(ruleJobDao, ruleJobParameterDao);

		Object test = new Object();
		DummyActionWithOutputProperty.setOutput(test);
		runner.run();
		assertEquals(test, DummyActionWithInputProperty.getInput());
	}

	@Test
	public void run_NullInputParameter_SetterStillCalled() {
		List<RuleJob> ruleJobs = Lists.newArrayList();
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_1, RULE_ID, JOB_WITH_OUTPUT_ID, 0L));
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_2, RULE_ID, JOB_WITH_INPUT_ID, 1L));

		List<RuleJobParameter> ruleJobParameters = Lists.newArrayList();
		ruleJobParameters.add(makeRuleJobParameter(RULE_JOB_ID_2, ParameterType.OUTPUT_PARAMETER,
				JOB_PROPERTY_NAME_INPUT, RULE_JOB_PARAMETER_INPUT_FROM_OUTPUT));

		RuleJobDao ruleJobDao = makeRuleJobDao();
		when(ruleJobDao.findByRule(eq(RULE_ID))).thenReturn(ruleJobs);

		RuleJobParameterDao ruleJobParameterDao = makeRuleJobParameterDao();
		when(ruleJobParameterDao.findByRuleJobs(anyCollectionOf(Long.class))).thenReturn(ruleJobParameters);

		RuleRunner runner = makeRuleRunner(ruleJobDao, ruleJobParameterDao);

		Object test = null;
		DummyActionWithOutputProperty.setOutput(test);
		runner.run();
		assertNull(DummyActionWithInputProperty.getInput());
	}

	private RuleRunner makeRuleRunner() {
		return makeRuleRunner(makeJobDao(), makeJobPropertyDao(), makeRuleJobDao());
	}

	private RuleRunner makeRuleRunner(RuleDao ruleDao) {
		return makeRuleRunner(ruleDao, makeJobDao(), makeRuleJobDao());
	}

	private RuleRunner makeRuleRunner(RuleDao ruleDao, JobDao jobDao, RuleJobDao ruleJobDao) {
		return makeRuleRunner(jobDao, makeJobPropertyDao(), ruleDao, ruleJobDao, makeRuleJobSaveDao());
	}

	private RuleRunner makeRuleRunner(JobDao jobDao) {
		return makeRuleRunner(jobDao, makeJobPropertyDao(), makeRuleJobDao());
	}

	private RuleRunner makeRuleRunner(RuleJobDao ruleJobDao) {
		return makeRuleRunner(makeJobDao(), makeJobPropertyDao(), ruleJobDao);
	}

	private RuleRunner makeRuleRunner(JobDao jobDao, RuleJobDao ruleJobDao) {
		return makeRuleRunner(jobDao, makeJobPropertyDao(), ruleJobDao);
	}

	private RuleRunner makeRuleRunner(RuleDao ruleDao, RuleJobDao ruleJobDao, RuleJobSaveDao ruleJobSaveDao) {
		return makeRuleRunner(makeJobDao(), makeJobPropertyDao(), ruleDao, ruleJobDao, ruleJobSaveDao);
	}

	private RuleRunner makeRuleRunner(RuleJobDao ruleJobDao, RuleJobSaveDao ruleJobSaveDao) {
		return makeRuleRunner(makeJobDao(), makeJobPropertyDao(), ruleJobDao, ruleJobSaveDao);
	}

	private RuleRunner makeRuleRunner(RuleJobDao ruleJobDao, RuleJobParameterDao ruleJobParameterDao) {
		return makeRuleRunner(makeJobDao(), makeJobPropertyDao(), makeRuleDao(), ruleJobDao, makeRuleJobSaveDao(),
				ruleJobParameterDao);
	}

	private RuleRunner makeRuleRunner(RuleJobDao ruleJobDao, JobPropertyDao jobPropertyDao) {
		return makeRuleRunner(makeJobDao(), jobPropertyDao, ruleJobDao, makeRuleJobSaveDao());
	}

	private RuleRunner makeRuleRunner(JobDao jobDao, JobPropertyDao jobPropertyDao, RuleJobDao ruleJobDao) {
		return makeRuleRunner(jobDao, jobPropertyDao, ruleJobDao, makeRuleJobSaveDao());
	}

	private RuleRunner makeRuleRunner(JobDao jobDao, JobPropertyDao jobPropertyDao, RuleJobDao ruleJobDao,
			RuleJobSaveDao ruleJobSaveDao) {
		return makeRuleRunner(jobDao, jobPropertyDao, makeRuleDao(), ruleJobDao, ruleJobSaveDao);
	}

	private RuleRunner makeRuleRunner(JobDao jobDao, JobPropertyDao jobPropertyDao, RuleDao ruleDao,
			RuleJobDao ruleJobDao, RuleJobSaveDao ruleJobSaveDao) {
		return makeRuleRunner(jobDao, jobPropertyDao, ruleDao, ruleJobDao, ruleJobSaveDao, makeRuleJobParameterDao());
	}

	private RuleRunner makeRuleRunner(JobDao jobDao, JobPropertyDao jobPropertyDao, RuleDao ruleDao,
			RuleJobDao ruleJobDao, RuleJobSaveDao ruleJobSaveDao, RuleJobParameterDao ruleJobParameterDao) {
		return new RuleRunner(RULE_ID, jobDao, jobPropertyDao, ruleDao, ruleJobDao, ruleJobSaveDao,
				ruleJobParameterDao);
	}

	private Map<Long, ModuleSpec> makeModuleSpecMap() {
		ModuleSpec spec1 = makeModuleSpec();

		Map<Long, ModuleSpec> map = Maps.newHashMap();
		map.put(MODULE_ID_1, spec1);
		return map;
	}

	private JobDao makeJobDao() {
		List<Job> jobs = Lists.newArrayList();
		jobs.add(makeJob(JOB_ACTION_ID, MODULE_ID_1, JOB_ACTION_CLASSNAME, false));
		jobs.add(makeJob(JOB_ACTION_EXCEPTION_ID, MODULE_ID_1, JOB_ACTION_EXCEPTION_CLASSNAME, false));
		jobs.add(makeJob(JOB_ACTION_WITH_PRIMITIVE_PROPS_ID, MODULE_ID_1, JOB_ACTION_WITH_PROPS_CLASSNAME, false));

		jobs.add(makeJob(JOB_CONDITIONAL_PASS_ID, MODULE_ID_1, JOB_CONDITIONAL_PASS_CLASSNAME, true));
		jobs.add(makeJob(JOB_CONDITIONAL_FAIL_ID, MODULE_ID_1, JOB_CONDITIONAL_FAIL_CLASSNAME, true));
		jobs.add(makeJob(JOB_CONDITIONAL_EXCEPTION_ID, MODULE_ID_1, JOB_CONDITIONAL_EXCEPTION_CLASSNAME, true));
		jobs.add(makeJob(JOB_CONDITIONAL_WITH_PRIMITIVE_PROPS_ID, MODULE_ID_1, JOB_CONDITIONAL_WITH_PROPS_CLASSNAME,
				true));

		jobs.add(makeJob(JOB_CONDITIONAL_CALLS_GET_LAST_COMPLETED_ID, MODULE_ID_1,
				JOB_CONDITIONAL_CALLS_GET_LAST_COMPLETED_CLASSNAME, true));
		jobs.add(makeJob(JOB_CONDITIONAL_CALLS_SAVE_STATE_ID, MODULE_ID_1, JOB_CONDITIONAL_CALLS_SAVE_STATE_CLASSNAME,
				true));
		jobs.add(makeJob(JOB_CONDITIONAL_CALLS_GET_SAVE_STATE_ID, MODULE_ID_1,
				JOB_CONDITIONAL_CALLS_GET_SAVE_STATE_CLASSNAME, true));

		jobs.add(makeJob(JOB_BASELINE_ID, MODULE_ID_1, JOB_BASELINE_IMPLEMENTATION_CLASSNAME, true));
		jobs.add(makeJob(JOB_BASELINE_CALLS_SAVE_ID, MODULE_ID_1, JOB_BASELINE_CALLS_SAVE_CLASSNAME, true));

		jobs.add(makeJob(JOB_CLASSDOESNTEXIST_ID, MODULE_ID_1, JOB_DOESNTEXIST_CLASSNAME, false));

		jobs.add(makeJob(JOB_WITH_OUTPUT_ID, MODULE_ID_1, JOB_WITH_OUTPUT_CLASSNAME, false));
		jobs.add(makeJob(JOB_WITH_INPUT_ID, MODULE_ID_1, JOB_WITH_INPUT_CLASSNAME, false));

		jobs.add(makeJob(JOB_WITH_REQUIRED_INPUT_PROP, MODULE_ID_1, JOB_WITH_INPUT_CLASSNAME, false));

		JobDao jobDao = mock(JobDao.class);
		when(jobDao.findByIds(anyCollectionOf(Long.class))).thenReturn(jobs);
		return jobDao;
	}

	private JobPropertyDao makeJobPropertyDao() {
		List<JobProperty> jobProperties = Lists.newArrayList();
		jobProperties.add(makeJobProperty(JOB_ACTION_WITH_PRIMITIVE_PROPS_ID, JOB_PROPERTY_METHOD_BOOLEAN,
				JOB_PROPERTY_NAME_BOOLEAN, PropertyType.BOOLEAN, false, PropertyIOType.INPUT));
		jobProperties.add(makeJobProperty(JOB_ACTION_WITH_PRIMITIVE_PROPS_ID, JOB_PROPERTY_METHOD_INTEGER,
				JOB_PROPERTY_NAME_INTEGER, PropertyType.INT, false, PropertyIOType.INPUT));
		jobProperties.add(makeJobProperty(JOB_ACTION_WITH_PRIMITIVE_PROPS_ID, JOB_PROPERTY_METHOD_LONG,
				JOB_PROPERTY_NAME_LONG, PropertyType.LONG, false, PropertyIOType.INPUT));
		jobProperties.add(makeJobProperty(JOB_ACTION_WITH_PRIMITIVE_PROPS_ID, JOB_PROPERTY_METHOD_STRING,
				JOB_PROPERTY_NAME_STRING, PropertyType.STRING, false, PropertyIOType.INPUT));
		jobProperties.add(makeJobProperty(JOB_ACTION_WITH_PRIMITIVE_PROPS_ID, JOB_PROPERTY_METHOD_DOUBLE,
				JOB_PROPERTY_NAME_DOUBLE, PropertyType.DOUBLE, false, PropertyIOType.INPUT));
		jobProperties.add(makeJobProperty(JOB_ACTION_WITH_PRIMITIVE_PROPS_ID, JOB_PROPERTY_METHOD_FLOAT,
				JOB_PROPERTY_NAME_FLOAT, PropertyType.FLOAT, false, PropertyIOType.INPUT));

		jobProperties.add(makeJobProperty(JOB_CONDITIONAL_WITH_PRIMITIVE_PROPS_ID, JOB_PROPERTY_METHOD_BOOLEAN,
				JOB_PROPERTY_NAME_BOOLEAN, PropertyType.BOOLEAN, false, PropertyIOType.INPUT));
		jobProperties.add(makeJobProperty(JOB_CONDITIONAL_WITH_PRIMITIVE_PROPS_ID, JOB_PROPERTY_METHOD_INTEGER,
				JOB_PROPERTY_NAME_INTEGER, PropertyType.INT, false, PropertyIOType.INPUT));
		jobProperties.add(makeJobProperty(JOB_CONDITIONAL_WITH_PRIMITIVE_PROPS_ID, JOB_PROPERTY_METHOD_LONG,
				JOB_PROPERTY_NAME_LONG, PropertyType.LONG, false, PropertyIOType.INPUT));
		jobProperties.add(makeJobProperty(JOB_CONDITIONAL_WITH_PRIMITIVE_PROPS_ID, JOB_PROPERTY_METHOD_STRING,
				JOB_PROPERTY_NAME_STRING, PropertyType.STRING, false, PropertyIOType.INPUT));
		jobProperties.add(makeJobProperty(JOB_CONDITIONAL_WITH_PRIMITIVE_PROPS_ID, JOB_PROPERTY_METHOD_DOUBLE,
				JOB_PROPERTY_NAME_DOUBLE, PropertyType.DOUBLE, false, PropertyIOType.INPUT));
		jobProperties.add(makeJobProperty(JOB_CONDITIONAL_WITH_PRIMITIVE_PROPS_ID, JOB_PROPERTY_METHOD_FLOAT,
				JOB_PROPERTY_NAME_FLOAT, PropertyType.FLOAT, false, PropertyIOType.INPUT));

		jobProperties.add(makeJobProperty(JOB_WITH_INPUT_ID, JOB_PROPERTY_METHOD_INPUT, JOB_PROPERTY_NAME_INPUT,
				PropertyType.CUSTOM, false, PropertyIOType.INPUT));
		jobProperties.add(makeJobProperty(JOB_WITH_OUTPUT_ID, JOB_PROPERTY_METHOD_OUTPUT, JOB_PROPERTY_NAME_OUTPUT,
				PropertyType.CUSTOM, false, PropertyIOType.OUTPUT));

		jobProperties.add(makeJobProperty(JOB_WITH_REQUIRED_INPUT_PROP, JOB_PROPERTY_METHOD_INPUT,
				JOB_PROPERTY_NAME_INPUT, PropertyType.CUSTOM, true, PropertyIOType.INPUT));

		JobPropertyDao jobPropertyDao = mock(JobPropertyDao.class);
		when(jobPropertyDao.findByJobIds(anyCollectionOf(Long.class))).thenReturn(jobProperties);
		return jobPropertyDao;
	}

	private RuleDao makeRuleDao() {
		return makeRuleDao(makeRule(RULE_ID, RULE_LAST_RAN, RULE_LAST_COMPLETED));
	}

	private RuleDao makeRuleDao(Rule rule) {
		RuleDao ruleDao = mock(RuleDao.class);
		when(ruleDao.findById(RULE_ID)).thenReturn(rule);
		return ruleDao;
	}

	private RuleJobDao makeRuleJobDao() {
		List<RuleJob> ruleJobs = Lists.newArrayList();
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_1, RULE_ID, JOB_ACTION_ID));
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_2, RULE_ID, JOB_CONDITIONAL_PASS_ID));
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_3, RULE_ID, JOB_ACTION_WITH_PRIMITIVE_PROPS_ID));
		ruleJobs.add(makeRuleJob(RULE_JOB_ID_4, RULE_ID, JOB_CONDITIONAL_WITH_PRIMITIVE_PROPS_ID));

		RuleJobDao ruleJobDao = mock(RuleJobDao.class);
		when(ruleJobDao.findByRule(eq(RULE_ID))).thenReturn(ruleJobs);
		return ruleJobDao;
	}

	private RuleJobSaveDao makeRuleJobSaveDao() {
		RuleJobSave ruleJobSave = makeRuleJobSave(RULE_JOB_ID_1, RULE_JOB_SAVED_STATE);

		RuleJobSaveDao ruleJobSaveDao = mock(RuleJobSaveDao.class);
		when(ruleJobSaveDao.findMostRecentByRuleJobId(RULE_JOB_ID_1)).thenReturn(ruleJobSave);
		return ruleJobSaveDao;
	}

	private RuleJobParameterDao makeRuleJobParameterDao() {
		List<RuleJobParameter> parameters = Lists.newArrayList();
		parameters.add(makeRuleJobParameter(RULE_JOB_ID_3, ParameterType.PRIMITIVE, JOB_PROPERTY_NAME_BOOLEAN, "true"));
		parameters.add(makeRuleJobParameter(RULE_JOB_ID_3, ParameterType.PRIMITIVE, JOB_PROPERTY_NAME_INTEGER, "123"));
		parameters.add(
				makeRuleJobParameter(RULE_JOB_ID_3, ParameterType.PRIMITIVE, JOB_PROPERTY_NAME_LONG, "2147483648"));
		parameters
				.add(makeRuleJobParameter(RULE_JOB_ID_3, ParameterType.PRIMITIVE, JOB_PROPERTY_NAME_STRING, "STRING"));
		parameters.add(makeRuleJobParameter(RULE_JOB_ID_3, ParameterType.PRIMITIVE, JOB_PROPERTY_NAME_DOUBLE, "1.1"));
		parameters.add(makeRuleJobParameter(RULE_JOB_ID_3, ParameterType.PRIMITIVE, JOB_PROPERTY_NAME_FLOAT, "1.1"));

		parameters.add(makeRuleJobParameter(RULE_JOB_ID_4, ParameterType.PRIMITIVE, JOB_PROPERTY_NAME_BOOLEAN, "true"));
		parameters.add(makeRuleJobParameter(RULE_JOB_ID_4, ParameterType.PRIMITIVE, JOB_PROPERTY_NAME_INTEGER, "123"));
		parameters.add(
				makeRuleJobParameter(RULE_JOB_ID_4, ParameterType.PRIMITIVE, JOB_PROPERTY_NAME_LONG, "2147483648"));
		parameters
				.add(makeRuleJobParameter(RULE_JOB_ID_4, ParameterType.PRIMITIVE, JOB_PROPERTY_NAME_STRING, "STRING"));
		parameters.add(makeRuleJobParameter(RULE_JOB_ID_4, ParameterType.PRIMITIVE, JOB_PROPERTY_NAME_DOUBLE, "1.1"));
		parameters.add(makeRuleJobParameter(RULE_JOB_ID_4, ParameterType.PRIMITIVE, JOB_PROPERTY_NAME_FLOAT, "1.1"));

		RuleJobParameterDao ruleJobParameterDao = mock(RuleJobParameterDao.class);
		when(ruleJobParameterDao.findByRuleJobs(anyCollectionOf(Long.class))).thenReturn(parameters);
		return ruleJobParameterDao;
	}

	private ModuleSpec makeModuleSpec() {
		return new DummyModule();
	}

	private Job makeJob(Long jobId, Long moduleId, String className, boolean conditionInd) {
		Job job = new Job();
		job.setJobId(jobId);
		job.setClassName(className);
		job.setConditionInd(conditionInd);
		job.setModuleId(moduleId);
		return job;
	}

	private Rule makeRule(Long ruleId, Date lastRan) {
		return makeRule(ruleId, lastRan, null);
	}

	private Rule makeRule(Long ruleId, Date lastRan, Date lastCompleted) {
		Rule rule = new Rule();
		rule.setRuleId(ruleId);
		rule.setLastCompleted(lastCompleted);
		rule.setLastRan(lastRan);
		return rule;
	}

	private RuleJob makeRuleJob(Long ruleJobId, Long ruleId, Long jobId) {
		return makeRuleJob(ruleJobId, ruleId, jobId, null);
	}

	private RuleJob makeRuleJob(Long ruleJobId, Long ruleId, Long jobId, Long collationSeq) {
		RuleJob rj = new RuleJob();
		rj.setRuleJobId(ruleJobId);
		rj.setJobId(jobId);
		rj.setRuleId(ruleId);
		rj.setCollationSeq(collationSeq);
		return rj;
	}

	private JobProperty makeJobProperty(Long jobId, String methodName, String name, PropertyType primitiveType,
			boolean requiredInd, PropertyIOType type) {
		JobProperty jp = new JobProperty();
		jp.setJobId(jobId);
		jp.setMethodName(methodName);
		jp.setName(name);
		jp.setPropertyType(primitiveType);
		jp.setRequiredInd(requiredInd);
		jp.setIOType(type);
		return jp;
	}

	private RuleJobSave makeRuleJobSave(Long ruleJobId, String content) {
		RuleJobSave rjs = new RuleJobSave();
		rjs.setContent(content);
		rjs.setRuleJobId(ruleJobId);
		return rjs;
	}

	private RuleJobParameter makeRuleJobParameter(long ruleJobId, ParameterType type, String propertyName,
			String content) {
		RuleJobParameter rjp = new RuleJobParameter();
		rjp.setContent(content);
		rjp.setPropertyName(propertyName);
		rjp.setType(type);
		rjp.setRuleJobId(ruleJobId);
		return rjp;
	}
}
