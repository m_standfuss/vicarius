package com.cerner.vicarius.dummymodules;

import com.cerner.vicarius.module.ModuleSpec;

public class DummyModuleShutdownThrowsException extends ModuleSpec {
	
	@Override
	public void startup() {
	}
	
	@Override
	public void shutdown() {
		throw new RuntimeException();
	}

}
