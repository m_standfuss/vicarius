package com.cerner.vicarius.dummymodules;

import com.cerner.vicarius.module.BaselineJob;
import com.cerner.vicarius.module.ConditionalJob;

public class DummyBaselineCallsSaveJob extends ConditionalJob implements BaselineJob {
	
	@Override
	public void runBaseline() {
		saveState("");
	}
	
	@Override
	public boolean run() {
		return true;
	}
}
