package com.cerner.vicarius.biz.rulerunner;

import static org.junit.Assert.assertEquals;

import java.io.InvalidObjectException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.google.common.collect.Lists;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

public class EmbeddedInputStringParameterTest {

	private static final String BASE_STRING_VALUE = "BASE_STRING_VALUE";

	private static final String EMBED_PROP_NAME_1 = "@propertyName";
	private static final String EMBED_PROP_NAME_2 = "@propertyName2";

	private static final Long RULE_JOB_ID = 1L;

	private static final String PROPERTY_NAME_1 = "PROPERTY_NAME";
	private static final String PROPERTY_NAME_2 = "PROPERTY_NAME_2";

	private static final Date DATE = new Date();
	private static final Long LONG = 123L;

	@Test(expected = IllegalArgumentException.class)
	public void constructor_NullObject_ExceptionThrown() throws InvalidObjectException {
		new EmbeddedInputStringParameter(null, new HashMap<>());
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructor_NullMap_ExceptionThrown() throws InvalidObjectException {
		new EmbeddedInputStringParameter("", null);
	}

	@Test(expected = IllegalStateException.class)
	public void constructor_MissingEmbeddedMember_ExceptionThrown() throws InvalidObjectException {
		new EmbeddedInputStringParameter("", new HashMap<>());
	}

	@Test(expected = IllegalStateException.class)
	public void constructor_MissingValueMember_ExceptionThrown() throws InvalidObjectException {
		JsonObject obj = new JsonObject();
		obj.add("properties", new JsonObject());
		new EmbeddedInputStringParameter(obj.toString().toString(), new HashMap<>());
	}

	@Test(expected = IllegalStateException.class)
	public void constructor_MissingPropertiesMember_ExceptionThrown() throws InvalidObjectException {
		JsonObject embeddedString = new JsonObject();
		embeddedString.addProperty("value", BASE_STRING_VALUE);
		new EmbeddedInputStringParameter(embeddedString.toString(), new HashMap<>());
	}

	@Test
	public void constructor_PropertyNotAnObject_PropertySkipped() throws InvalidObjectException {
		JsonObject embeddedString = new JsonObject();
		embeddedString.addProperty("value", BASE_STRING_VALUE + EMBED_PROP_NAME_1 + ' ');
		JsonArray properties = new JsonArray();
		properties.add(new JsonPrimitive(""));
		embeddedString.add("properties", properties);
		String value = (String) new EmbeddedInputStringParameter(embeddedString.toString(), new HashMap<>())
				.getParameterValue();
		assertEquals(BASE_STRING_VALUE + null, value);
	}

	@Test
	public void constructor_MissingPropertyName_PropertySkipped() throws InvalidObjectException {
		JsonObject embeddedString = new JsonObject();
		embeddedString.addProperty("value", BASE_STRING_VALUE + EMBED_PROP_NAME_1 + ' ');
		JsonArray properties = new JsonArray();
		JsonObject embeddedObj = new JsonObject();
		embeddedObj.addProperty("ruleJobId", RULE_JOB_ID);
		embeddedObj.addProperty("parameterName", PROPERTY_NAME_1);
		properties.add(embeddedObj);
		embeddedString.add("properties", properties);
		String value = (String) new EmbeddedInputStringParameter(embeddedString.toString(), new HashMap<>())
				.getParameterValue();
		assertEquals(BASE_STRING_VALUE + null, value);
	}

	@Test
	public void constructor_MissingParameterName_PropertySkipped() throws InvalidObjectException {
		JsonObject embeddedString = new JsonObject();
		embeddedString.addProperty("value", BASE_STRING_VALUE + EMBED_PROP_NAME_1 + ' ');
		JsonArray properties = new JsonArray();
		JsonObject embeddedObj = new JsonObject();
		embeddedObj.addProperty("embeddedPropertyName", EMBED_PROP_NAME_1);
		embeddedObj.addProperty("ruleJobId", RULE_JOB_ID);
		properties.add(embeddedObj);
		embeddedString.add("properties", properties);
		String value = (String) new EmbeddedInputStringParameter(embeddedString.toString(), new HashMap<>())
				.getParameterValue();
		assertEquals(BASE_STRING_VALUE + null, value);
	}

	@Test
	public void constructor_MissingPropertyRuleJobId_PropertySkipped() throws InvalidObjectException {
		JsonObject embeddedString = new JsonObject();
		embeddedString.addProperty("value", BASE_STRING_VALUE + EMBED_PROP_NAME_1 + ' ');
		JsonArray properties = new JsonArray();
		JsonObject embeddedObj = new JsonObject();
		embeddedObj.addProperty("embeddedPropertyName", EMBED_PROP_NAME_1);
		embeddedObj.addProperty("parameterName", PROPERTY_NAME_1);
		properties.add(embeddedObj);
		embeddedString.add("properties", properties);
		String value = (String) new EmbeddedInputStringParameter(embeddedString.toString(), new HashMap<>())
				.getParameterValue();
		assertEquals(BASE_STRING_VALUE + null, value);
	}

	@Test
	public void getParameterClass_ReturnsString() throws InvalidObjectException {
		JsonObject obj = makeValidObject();
		Map<RuleJobPropertyKey, Object> outputMap = new HashMap<>();
		assertEquals(String.class, new EmbeddedInputStringParameter(obj.toString(), outputMap).getParameterClass());
	}

	@Test
	public void getParameterValue_NoEmbeddedProperties_SameStringReturned() throws InvalidObjectException {
		JsonObject obj = makeValidObject();
		Map<RuleJobPropertyKey, Object> outputMap = new HashMap<>();
		assertEquals(BASE_STRING_VALUE,
				new EmbeddedInputStringParameter(obj.toString(), outputMap).getParameterValue());
	}

	@Test
	public void getParameterValue_EmbeddedParameter_ValueReplaced() throws InvalidObjectException {
		String value = "pre" + EMBED_PROP_NAME_1 + " post";
		JsonObject obj = makeValidObject(value, EMBED_PROP_NAME_1, RULE_JOB_ID, PROPERTY_NAME_1);
		System.out.println(obj.toString());
		Map<RuleJobPropertyKey, Object> outputMap = new HashMap<>();
		outputMap.put(new RuleJobPropertyKey(RULE_JOB_ID, PROPERTY_NAME_1), LONG);

		String parameter = (String) new EmbeddedInputStringParameter(obj.toString(), outputMap).getParameterValue();
		assertEquals("pre" + LONG + "post", parameter);
	}

	@Test
	public void getParameterValue_MultipleEmbeddedParameter_ValueReplaced() throws InvalidObjectException {
		String value = "pre" + EMBED_PROP_NAME_1 + " post1 pre2" + EMBED_PROP_NAME_2 + " post2";

		List<String> embedPropNames = Lists.newArrayList(EMBED_PROP_NAME_1, EMBED_PROP_NAME_2);
		List<Long> ruleJobIds = Lists.newArrayList(RULE_JOB_ID, RULE_JOB_ID);
		List<String> propertyNames = Lists.newArrayList(PROPERTY_NAME_1, PROPERTY_NAME_2);
		JsonObject obj = makeValidObject(value, embedPropNames, ruleJobIds, propertyNames);

		Map<RuleJobPropertyKey, Object> outputMap = new HashMap<>();
		outputMap.put(new RuleJobPropertyKey(RULE_JOB_ID, PROPERTY_NAME_1), LONG);
		outputMap.put(new RuleJobPropertyKey(RULE_JOB_ID, PROPERTY_NAME_2), DATE);

		String parameter = (String) new EmbeddedInputStringParameter(obj.toString(), outputMap).getParameterValue();
		assertEquals("pre" + LONG + "post1 pre2" + DATE.toString() + "post2", parameter);
	}

	@Test
	public void getParameterValue_ParameterDoesntExist_ReplacedWithNull() throws InvalidObjectException {
		String value = "pre" + EMBED_PROP_NAME_1 + " post";
		JsonObject obj = makeValidObject(value, EMBED_PROP_NAME_1, RULE_JOB_ID, PROPERTY_NAME_1);

		Map<RuleJobPropertyKey, Object> outputMap = new HashMap<>();

		String parameter = (String) new EmbeddedInputStringParameter(obj.toString(), outputMap).getParameterValue();
		assertEquals("prenullpost", parameter);
	}

	@Test
	public void getParameterValue_EscapedAnnotation_NotReplaced() throws InvalidObjectException {
		String value = "pre/" + EMBED_PROP_NAME_1 + " post";
		JsonObject obj = makeValidObject(value, EMBED_PROP_NAME_1, RULE_JOB_ID, PROPERTY_NAME_1);

		Map<RuleJobPropertyKey, Object> outputMap = new HashMap<>();
		outputMap.put(new RuleJobPropertyKey(RULE_JOB_ID, PROPERTY_NAME_1), LONG);

		String parameter = (String) new EmbeddedInputStringParameter(obj.toString(), outputMap).getParameterValue();
		assertEquals("pre" + EMBED_PROP_NAME_1 + " post", parameter);
	}

	private JsonObject makeValidObject() {
		JsonObject embeddedString = new JsonObject();
		embeddedString.addProperty("value", BASE_STRING_VALUE);
		embeddedString.add("properties", new JsonArray());
		return embeddedString;
	}

	private JsonObject makeValidObject(String value, String embeddedName, Long ruleJobId, String parameterName) {
		JsonObject embeddedString = new JsonObject();
		embeddedString.addProperty("value", value);
		JsonArray properties = new JsonArray();
		JsonObject embeddedObj = new JsonObject();
		embeddedObj.addProperty("embeddedPropertyName", embeddedName);
		embeddedObj.addProperty("ruleJobId", ruleJobId);
		embeddedObj.addProperty("parameterName", parameterName);
		properties.add(embeddedObj);
		embeddedString.add("properties", properties);
		return embeddedString;
	}

	private JsonObject makeValidObject(String value, List<String> embeddedNames, List<Long> ruleJobIds,
			List<String> parameterNames) {
		JsonObject embeddedString = new JsonObject();
		embeddedString.addProperty("value", value);
		JsonArray properties = new JsonArray();
		for (int i = 0; i < embeddedNames.size(); i++) {
			String embeddedName = embeddedNames.get(i);
			Long ruleJobId = ruleJobIds.get(i);
			String parameterName = parameterNames.get(i);

			JsonObject embeddedObj = new JsonObject();
			embeddedObj.addProperty("embeddedPropertyName", embeddedName);
			embeddedObj.addProperty("ruleJobId", ruleJobId);
			embeddedObj.addProperty("parameterName", parameterName);
			properties.add(embeddedObj);
		}
		embeddedString.add("properties", properties);
		return embeddedString;
	}
}
