package com.cerner.vicarius.dummymodules;

import com.cerner.vicarius.module.ModuleSpec;

public class DummyModuleStartupThrowsException extends ModuleSpec {
	
	@Override
	public void startup() {
		throw new RuntimeException();
	}
	
	@Override
	public void shutdown() {
	}
	
}
