package com.cerner.vicarius.biz;

import java.io.Serializable;
import java.util.Date;

import com.cerner.vicarius.data.model.DataEntity;

public class DummyEntity implements DataEntity {

	private final Long primaryKey;

	public DummyEntity(Long primaryKey) {
		this.primaryKey = primaryKey;
	}

	@Override
	public Serializable getPrimaryKey() {
		return primaryKey;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setActiveInd(boolean activeInd) {
		// TODO Auto-generated method stub

	}

}
