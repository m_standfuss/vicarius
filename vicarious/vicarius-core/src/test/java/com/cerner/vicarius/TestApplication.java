package com.cerner.vicarius;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.jasypt.util.text.BasicTextEncryptor;
import org.junit.After;
import org.junit.Test;

import com.cerner.vicarius.biz.moduleloader.ModuleLoader;
import com.cerner.vicarius.dummymodules.DummyModule;
import com.cerner.vicarius.module.ModuleSpec;

public class TestApplication {

	private static final String PROP_KEY = "PROP_KEY";
	private static final String PROP_VALUE = "PROP_VALUE";

	/*
	 * This decrypts to PROP_VALUE
	 */
	private static final String PROP_ENCRYPTED_VALUE = getEncryptor().encrypt(PROP_VALUE);

	@After
	public void cleanup() {
		File propFile = new File("vicarius.properties");
		if (propFile.exists()) {
			propFile.delete();
			Application.loadProperties();
		}

		File encryptedFile = new File("vicarius.encrypted.properties");
		if (encryptedFile.exists()) {
			encryptedFile.delete();
		}
		Application.setProperties(null);
		Application.setEncryptedProperties(null);
	}

	@Test
	public void loadProperties_FileExists_PropertiesLoaded() throws IOException {
		makePropertiesFile(PROP_KEY, PROP_VALUE);
		Application.loadProperties();
		assertEquals(PROP_VALUE, Application.getProperty(DummyModule.class, PROP_KEY));
	}

	@Test
	public void loadEncryptedProperties_NoApplicationProperty_LooksInEncryptedProperties() {
		Application.setEncryptedProperties(getEncryptedProperties());
		Application.setProperties(null);

		assertEquals(PROP_VALUE, Application.getProperty(DummyModule.class, PROP_KEY));
	}

	public void loadEncryptedProperties_InBoth_ReturnsUnencryptedFirst() {
		Application.setEncryptedProperties(getEncryptedProperties());

		Properties newProps = new Properties();
		newProps.put(DummyModule.class.getCanonicalName() + '.' + PROP_KEY, "TEST!@#");
		Application.setProperties(newProps);

		assertEquals("TEST!@#", Application.getProperty(DummyModule.class, PROP_KEY));
	}

	@Test
	public void loadEncryptedProperties_InNeitherProperties_ReturnsNull() {
		Application.setEncryptedProperties(new Properties());
		Application.setProperties(new Properties());

		assertNull(Application.getProperty(DummyModule.class, PROP_KEY));
	}

	@Test
	public void getProperty_NoProperties_NullReturned() {
		Application.setProperties(null);
		assertNull(Application.getProperty(DummyModule.class, PROP_KEY));
	}

	@Test(expected = IllegalArgumentException.class)
	public void getProperty_NullModuleClass_ExceptionThrown() {
		Application.getProperty(null, PROP_KEY);
	}

	@Test(expected = IllegalArgumentException.class)
	public void getProperty_NonCanonicalName_ExceptionThrown() {
		Application.getProperty(new ModuleSpec() {

			@Override
			public void startup() {
				// TODO Auto-generated method stub

			}

			@Override
			public void shutdown() {
				// TODO Auto-generated method stub

			}

		}.getClass(), PROP_KEY);
	}

	@Test(expected = IllegalArgumentException.class)
	public void getProperty_NullPropertyName_ExceptionThrown() {
		Application.getProperty(DummyModule.class, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void getProperty_EmptyPropertyName_ExceptionThrown() {
		Application.getProperty(DummyModule.class, "");
	}

	@Test
	public void start_ApplicationTerminated_LockReleased() throws IOException, InterruptedException {
		ApplicationLock lock = makeLock();
		when(lock.isLocked()).thenReturn(false);

		createPropertiesFile();

		Application application = makeApplication(lock);

		application.start();

		verify(lock, times(1)).releaseLock();
	}

	@Test
	public void start_MissingDBHostName_NeverLocked() throws IOException, InterruptedException {
		ApplicationLock lock = makeLock();

		Properties prop = new Properties();
		prop.setProperty("DATABASE_USERNAME", "Test");
		prop.setProperty("DATABASE_PASSWORD", "Test");
		createPropertiesFile(prop);

		Application application = makeApplication(lock);
		application.start();

		verify(lock, never()).obtainLock();
	}

	@Test
	public void start_MissingDBUsername_NeverLocked() throws IOException, InterruptedException {
		ApplicationLock lock = makeLock();

		Properties prop = new Properties();
		prop.setProperty("DATABASE_HOST", "localhost");
		prop.setProperty("DATABASE_PASSWORD", "Test");
		createPropertiesFile(prop);

		Application application = makeApplication(lock);
		application.start();

		verify(lock, never()).obtainLock();
	}

	@Test
	public void start_MissingDBPassword_NeverLocked() throws IOException, InterruptedException {
		ApplicationLock lock = makeLock();

		Properties prop = new Properties();
		prop.setProperty("DATABASE_HOST", "localhost");
		prop.setProperty("DATABASE_USERNAME", "Test");
		createPropertiesFile(prop);

		Application application = makeApplication(lock);
		application.start();

		verify(lock, never()).obtainLock();
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructor_NullLock_ExceptionThrown() {
		new Application(null, makeScheduler(), makeModuleLoader());
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructor_NullScheduler_ExceptionThrown() throws IOException {
		new Application(makeLock(), null, makeModuleLoader());
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructor_NullModuleLoader_ExceptionThrown() throws IOException {
		new Application(makeLock(), makeScheduler(), null);
	}

	@Test
	public void start_LockNotObtained_EarlyReturn() throws IOException {
		ApplicationLock lock = makeLock(false);

		Application application = makeApplication(lock);

		application.start();

		verify(lock, never()).releaseLock();
	}

	@Test(expected = IOException.class)
	public void start_ObtainLockThrowsException_EarlyReturn() throws IOException {
		Properties prop = new Properties();
		prop.setProperty("DATABASE_USERNAME", "Test");
		prop.setProperty("DATABASE_PASSWORD", "Test");
		prop.setProperty("DATABASE_HOST", "localhost");
		createPropertiesFile(prop);

		ApplicationLock lock = makeLock(false);
		when(lock.obtainLock()).thenThrow(new IOException());

		Application application = makeApplication(lock);
		application.start();

		verify(lock, never()).releaseLock();
	}

	@Test
	public void stop_CallsTerminateApplication() throws IOException, InterruptedException {
		ApplicationLock lock = makeLock();

		Application application = makeApplication(lock);
		application.stop();

		verify(lock, times(1)).terminateLockedApplication();
	}

	private Properties getEncryptedProperties() {
		Properties props = new Properties();
		props.put(DummyModule.class.getCanonicalName() + '.' + PROP_KEY, PROP_ENCRYPTED_VALUE);
		return props;
	}

	private Properties getProperties() {
		Properties props = new Properties();
		props.put(DummyModule.class.getCanonicalName() + '.' + PROP_KEY, PROP_VALUE);
		return props;
	}

	private void makeEncryptedPropertiesFile(String key, String value) throws IOException {
		FileUtils.writeStringToFile(new File("vicarius.encrypted.properties"),
				DummyModule.class.getCanonicalName() + '.' + key + '=' + value, Charset.defaultCharset());
	}

	private void makePropertiesFile(String key, String value) throws IOException {
		FileUtils.writeStringToFile(new File("vicarius.properties"),
				DummyModule.class.getCanonicalName() + '.' + key + "=" + value, Charset.defaultCharset());
	}

	private Application makeApplication(ApplicationLock lock) {
		return makeApplication(lock, makeScheduler());
	}

	private Application makeApplication(ApplicationLock lock, Scheduler scheduler) {
		return makeApplication(lock, scheduler, makeModuleLoader());
	}

	private Application makeApplication(ApplicationLock lock, Scheduler scheduler, ModuleLoader moduleLoader) {
		return new Application(lock, scheduler, moduleLoader);
	}

	private ModuleLoader makeModuleLoader() {
		return mock(ModuleLoader.class);
	}

	private Scheduler makeScheduler() {
		return mock(Scheduler.class);
	}

	private ApplicationLock makeLock() throws IOException {
		return makeLock(true);
	}

	private ApplicationLock makeLock(boolean lockObtained) throws IOException {
		ApplicationLock lock = mock(ApplicationLock.class);
		when(lock.obtainLock()).thenReturn(lockObtained);
		when(lock.isLocked()).thenReturn(true);
		return lock;
	}

	private void createPropertiesFile() throws IOException {
		Properties prop = new Properties();
		prop.setProperty("DATABASE_HOST", "localhost");
		prop.setProperty("DATABASE_USERNAME", "Test");
		prop.setProperty("DATABASE_PASSWORD", "Test");
		createPropertiesFile(prop);
	}

	private void createPropertiesFile(Properties props) throws IOException {
		cleanup();
		OutputStream output = new FileOutputStream("vicarius.properties");
		props.store(output, null);
		Application.loadProperties();
	}
		
	private static BasicTextEncryptor getEncryptor() {
		BasicTextEncryptor encryptor = new BasicTextEncryptor();
		encryptor.setPasswordCharArray(Application.DEFAULT_ENCRYPTION_KEY);
		return encryptor;
	}
}
