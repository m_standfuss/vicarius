package com.cerner.vicarius.dummymodules;

import java.util.Date;

import com.cerner.vicarius.module.ConditionalJob;

public class DummyConditionalCallsGetLastCompleted extends ConditionalJob {

	private static Date lastCompleted;

	public static Date getLastCompletedSet() {
		return lastCompleted;
	}
	
	public static void clearLastCompletedSet() {
		lastCompleted = null;
	}
	
	@Override
	public boolean run() {
		lastCompleted = getLastCompleted();
		return true;
	}

}
