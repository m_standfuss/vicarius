package com.cerner.vicarius.biz.rulerunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.InvalidObjectException;

import org.junit.Test;

import com.cerner.vicarius.data.model.PropertyType;

public class PrimitiveInputParameterTest {

	private static final String NON_BOOLEAN = "NotABoolean";
	private static final String NON_NUMBER = "NotANumber";

	private static final Integer INT_1 = 1;
	private static final Long LONG_1 = 1l;
	private static final Double DOUBLE_1 = 1.1d;
	private static final Float FLOAT_1 = 1.1f;
	private static final String STRING_1 = "STRING_1";

	@Test(expected = IllegalArgumentException.class)
	public void isPrimitivePropertyType_Null_ExceptionThrown() {
		PrimitiveInputParameter.isPrimitivePropertyType(null);
	}

	@Test
	public void isPrimitiveInputParameter_BOOLEAN_ReturnsTrue() {
		assertTrue(PrimitiveInputParameter.isPrimitivePropertyType(PropertyType.BOOLEAN));
	}

	@Test
	public void isPrimitiveInputParameter_DOUBLE_ReturnsTrue() {
		assertTrue(PrimitiveInputParameter.isPrimitivePropertyType(PropertyType.DOUBLE));
	}

	@Test
	public void isPrimitiveInputParameter_FLOAT_ReturnsTrue() {
		assertTrue(PrimitiveInputParameter.isPrimitivePropertyType(PropertyType.FLOAT));
	}

	@Test
	public void isPrimitiveInputParameter_INT_ReturnsTrue() {
		assertTrue(PrimitiveInputParameter.isPrimitivePropertyType(PropertyType.INT));
	}

	@Test
	public void isPrimitiveInputParameter_LONG_ReturnsTrue() {
		assertTrue(PrimitiveInputParameter.isPrimitivePropertyType(PropertyType.LONG));
	}

	@Test
	public void isPrimitiveInputParameter_STRING_ReturnsTrue() {
		assertTrue(PrimitiveInputParameter.isPrimitivePropertyType(PropertyType.STRING));
	}

	@Test
	public void isPrimitivieInputParameter_USERCLASS_ReturnsFalse() {
		assertFalse(PrimitiveInputParameter.isPrimitivePropertyType(PropertyType.CUSTOM));
	}

	@Test
	public void getParameterValue_Null_ReturnsNull() throws InvalidObjectException {
		assertNull(new PrimitiveInputParameter(PropertyType.BOOLEAN, null).getParameterValue());
	}

	@Test
	public void getParameterValue_NonBooleanString_FalseBooleanReturned() throws InvalidObjectException {
		assertFalse((Boolean) new PrimitiveInputParameter(PropertyType.BOOLEAN, NON_BOOLEAN).getParameterValue());
	}

	@Test(expected = InvalidObjectException.class)
	public void constructor_NonInt_ExceptionThrown() throws InvalidObjectException {
		new PrimitiveInputParameter(PropertyType.INT, NON_NUMBER);
	}

	@Test(expected = InvalidObjectException.class)
	public void gconstructor_NonDouble_ExceptionThrown() throws InvalidObjectException {
		new PrimitiveInputParameter(PropertyType.DOUBLE, NON_NUMBER);
	}

	@Test(expected = InvalidObjectException.class)
	public void constructor_NonFloat_ExceptionThrown() throws InvalidObjectException {
		new PrimitiveInputParameter(PropertyType.FLOAT, NON_NUMBER);
	}

	@Test(expected = InvalidObjectException.class)
	public void constructor_NonLong_ExceptionThrown() throws InvalidObjectException {
		new PrimitiveInputParameter(PropertyType.LONG, NON_NUMBER);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructor_NullType_ExceptionThrown() throws InvalidObjectException {
		new PrimitiveInputParameter(null, "");
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructor_InvalidType_ExceptionThrown() throws InvalidObjectException {
		new PrimitiveInputParameter(PropertyType.CUSTOM, "");
	}

	@Test
	public void getParameterValue_TrueString_TrueBooleanReturned() throws InvalidObjectException {
		assertTrue((Boolean) new PrimitiveInputParameter(PropertyType.BOOLEAN, Boolean.TRUE.toString())
				.getParameterValue());
	}

	@Test
	public void getParameterValue_FalseString_FalseBooleanReturned() throws InvalidObjectException {
		assertFalse((Boolean) new PrimitiveInputParameter(PropertyType.BOOLEAN, Boolean.FALSE.toString())
				.getParameterValue());
	}

	@Test
	public void getParameterValue_ValidInt_IntValueReturned() throws InvalidObjectException {
		assertEquals(INT_1, new PrimitiveInputParameter(PropertyType.INT, String.valueOf(INT_1)).getParameterValue());
	}

	@Test
	public void getParameterValue_ValidDouble_DoubleValueReturned() throws InvalidObjectException {
		assertEquals(DOUBLE_1,
				new PrimitiveInputParameter(PropertyType.DOUBLE, String.valueOf(DOUBLE_1)).getParameterValue());
	}

	@Test
	public void getParameterValue_ValidLong_LongValueReturned() throws InvalidObjectException {
		assertEquals(LONG_1,
				new PrimitiveInputParameter(PropertyType.LONG, String.valueOf(LONG_1)).getParameterValue());
	}

	@Test
	public void getParameterValue_ValidFloat_FloatValueReturned() throws InvalidObjectException {
		assertEquals(FLOAT_1,
				new PrimitiveInputParameter(PropertyType.FLOAT, String.valueOf(FLOAT_1)).getParameterValue());
	}

	@Test
	public void getParameterValue_StringValue_SameStringReturned() throws InvalidObjectException {
		assertEquals(STRING_1, new PrimitiveInputParameter(PropertyType.STRING, STRING_1).getParameterValue());
	}

	@Test
	public void getParameterClass_BOOLEAN_ReturnsBoolean() throws InvalidObjectException {
		Class<?> clazz = new PrimitiveInputParameter(PropertyType.BOOLEAN, Boolean.TRUE.toString()).getParameterClass();
		assertEquals(Boolean.TYPE, clazz);
	}

	@Test
	public void getParameterClass_INT_ReturnsIntegerType() throws InvalidObjectException {
		Class<?> clazz = new PrimitiveInputParameter(PropertyType.INT, String.valueOf(INT_1)).getParameterClass();
		assertEquals(Integer.TYPE, clazz);
	}

	@Test
	public void getParameterClass_LONG_ReturnsLongType() throws InvalidObjectException {
		Class<?> clazz = new PrimitiveInputParameter(PropertyType.LONG, String.valueOf(LONG_1)).getParameterClass();
		assertEquals(Long.TYPE, clazz);
	}

	@Test
	public void getParameterClass_STRING_ReturnsStringClass() throws InvalidObjectException {
		Class<?> clazz = new PrimitiveInputParameter(PropertyType.STRING, STRING_1).getParameterClass();
		assertEquals(String.class, clazz);
	}

	@Test
	public void getParameterClass_DOUBLE_ReturnsDoubleType() throws InvalidObjectException {
		Class<?> clazz = new PrimitiveInputParameter(PropertyType.DOUBLE, String.valueOf(DOUBLE_1)).getParameterClass();
		assertEquals(Double.TYPE, clazz);
	}

	@Test
	public void getParameterClass_FLOAT_ReturnsFloatType() throws InvalidObjectException {
		Class<?> clazz = new PrimitiveInputParameter(PropertyType.FLOAT, String.valueOf(FLOAT_1)).getParameterClass();
		assertEquals(Float.TYPE, clazz);
	}
}
