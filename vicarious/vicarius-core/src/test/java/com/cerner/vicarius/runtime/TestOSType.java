package com.cerner.vicarius.runtime;

import static org.junit.Assert.assertEquals;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.cerner.vicarius.runtime.OSType;

public class TestOSType {

	private static String trueOSName;

	@BeforeClass
	public static void beforeAll() {
		trueOSName = System.getProperty("os.name");
	}

	@AfterClass
	public static void afterAll() {
		System.setProperty("os.name", trueOSName);
	}

	@Test
	public void getOSType_UnixOS_UNIXReturned() {
		System.setProperty("os.name", "UNIX");
		assertEquals(OSType.UNIX, OSType.getType());
	}

	@Test
	public void getOSType_LinuxOS_UNIXReturned() {
		System.setProperty("os.name", "LINUX");
		assertEquals(OSType.UNIX, OSType.getType());
	}

	@Test
	public void getOSType_AIXOS_UNIXReturned() {
		System.setProperty("os.name", "AIX");
		assertEquals(OSType.UNIX, OSType.getType());
	}

	@Test
	public void getOSType_MacOS_MACReturned() {
		System.setProperty("os.name", "MAC");
		assertEquals(OSType.MAC, OSType.getType());
	}

	@Test
	public void getOSType_WindowsOS_WINReturned() {
		System.setProperty("os.name", "WIN");
		assertEquals(OSType.WIN, OSType.getType());
	}

	@Test(expected = IllegalStateException.class)
	public void getOSType_NotRecognizedOS_ExceptionThrown() {
		System.setProperty("os.name", "UnrecognizedOS");
		OSType.getType();
	}
}
