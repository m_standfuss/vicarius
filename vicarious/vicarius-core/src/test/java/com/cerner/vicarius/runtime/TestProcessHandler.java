package com.cerner.vicarius.runtime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.lang.management.RuntimeMXBean;

import org.junit.Test;

public class TestProcessHandler {

	private static final int PROCESS_ID = 12345;
	private static final String PROCESS_NAME = "PROCESS_NAME";

	private static final String INVALID_PROCESS_NAME = "DOESNT_HAVE_AN_AT_SYMBOL";

	@Test(expected = IllegalArgumentException.class)
	public void constructor_NullOsType_ExceptionThrown() {
		makeProcessHandler(null, makeCommandHandler(), makeRuntimeBean());
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructor_NullCommandHandler_ExceptionThrown() {
		makeProcessHandler(OSType.MAC, null, makeRuntimeBean());
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructor_NullRuntimeBean_ExceptionThrown() {
		makeProcessHandler(OSType.MAC, makeCommandHandler(), null);
	}

	@Test(expected = IllegalStateException.class)
	public void getCurrentProcessId_InvalidFormat_ExceptionThrown() {
		RuntimeMXBean runtimeBean = makeRuntimeBean();
		when(runtimeBean.getName()).thenReturn(INVALID_PROCESS_NAME);

		ProcessHandler processHandler = makeProcessHandler(runtimeBean);
		processHandler.getCurrentProcessId();
	}

	@Test
	public void getCurrentProcessId_ValidFormat_ProcessIdReturned() {
		ProcessHandler processHandler = makeProcessHandler();
		assertEquals(PROCESS_ID, processHandler.getCurrentProcessId());
	}

	@Test
	public void isActiveProcess_WindowsOs_CommandRan() throws IOException, InterruptedException {
		CommandHandler commandHandler = makeCommandHandler();

		ProcessHandler processHandler = makeProcessHandler(OSType.WIN, commandHandler);
		processHandler.isActiveProcess(PROCESS_ID);

		verify(commandHandler).runCommand(eq("tasklist /fi \"pid eq " + PROCESS_ID + "\" | findstr " + PROCESS_ID));
	}

	@Test
	public void isActiveProcess_UnixOs_CommmandRan() throws IOException, InterruptedException {
		CommandHandler commandHandler = makeCommandHandler();

		ProcessHandler processHandler = makeProcessHandler(OSType.UNIX, commandHandler);
		processHandler.isActiveProcess(PROCESS_ID);

		verify(commandHandler).runCommand(eq("ps -p " + PROCESS_ID));
	}

	@Test
	public void isActiveProcess_MacOs_CommmandRan() throws IOException, InterruptedException {
		CommandHandler commandHandler = makeCommandHandler();

		ProcessHandler processHandler = makeProcessHandler(OSType.MAC, commandHandler);
		processHandler.isActiveProcess(PROCESS_ID);

		verify(commandHandler).runCommand(eq("ps -p " + PROCESS_ID));
	}

	@Test
	public void isActiveProcess_0ExitCode_TrueReturned() throws IOException, InterruptedException {
		CommandHandler commandHandler = makeCommandHandler();
		when(commandHandler.runCommand(anyString())).thenReturn(0);

		ProcessHandler processHandler = makeProcessHandler(OSType.WIN, commandHandler);
		assertTrue(processHandler.isActiveProcess(PROCESS_ID));
	}

	@Test
	public void isActiveProcess_1ExitCode_FalseReturned() throws IOException, InterruptedException {
		CommandHandler commandHandler = makeCommandHandler();
		when(commandHandler.runCommand(anyString())).thenReturn(1);

		ProcessHandler processHandler = makeProcessHandler(OSType.WIN, commandHandler);
		assertFalse(processHandler.isActiveProcess(PROCESS_ID));
	}

	@Test
	public void killProcess_WindowsOs_CommandRan() throws IOException, InterruptedException {
		CommandHandler commandHandler = makeCommandHandler();

		ProcessHandler processHandler = makeProcessHandler(OSType.WIN, commandHandler);
		processHandler.killProcess(PROCESS_ID);

		verify(commandHandler).runCommand(eq("taskkill /PID " + PROCESS_ID + " /F"));
	}

	@Test
	public void killProcess_UnixOs_CommmandRan() throws IOException, InterruptedException {
		CommandHandler commandHandler = makeCommandHandler();

		ProcessHandler processHandler = makeProcessHandler(OSType.UNIX, commandHandler);
		processHandler.killProcess(PROCESS_ID);

		verify(commandHandler).runCommand(eq("kill -9 " + PROCESS_ID));
	}

	@Test
	public void killProcess_MacOs_CommmandRan() throws IOException, InterruptedException {
		CommandHandler commandHandler = makeCommandHandler();

		ProcessHandler processHandler = makeProcessHandler(OSType.MAC, commandHandler);
		processHandler.killProcess(PROCESS_ID);

		verify(commandHandler).runCommand(eq("kill -9 " + PROCESS_ID));
	}

	@Test(expected = IllegalStateException.class)
	public void killProcess_1ExitCode_ExceptionThrown() throws IOException, InterruptedException {
		CommandHandler commandHandler = makeCommandHandler();
		when(commandHandler.runCommand(anyString())).thenReturn(1);

		ProcessHandler processHandler = makeProcessHandler(OSType.WIN, commandHandler);
		processHandler.killProcess(PROCESS_ID);
	}

	private ProcessHandler makeProcessHandler() {
		return makeProcessHandler(OSType.MAC, makeCommandHandler(), makeRuntimeBean());
	}

	private ProcessHandler makeProcessHandler(OSType osType, CommandHandler commandHandler) {
		return makeProcessHandler(osType, commandHandler, makeRuntimeBean());
	}

	private ProcessHandler makeProcessHandler(RuntimeMXBean runtimeBean) {
		return makeProcessHandler(OSType.MAC, makeCommandHandler(), runtimeBean);
	}

	private ProcessHandler makeProcessHandler(OSType osType, CommandHandler commandHandler, RuntimeMXBean runtimeBean) {
		return new ProcessHandler(osType, commandHandler, runtimeBean);
	}

	private CommandHandler makeCommandHandler() {
		CommandHandler commandHandler = mock(CommandHandler.class);

		return commandHandler;
	}

	private RuntimeMXBean makeRuntimeBean() {
		RuntimeMXBean bean = mock(RuntimeMXBean.class);
		String processName = PROCESS_ID + "@" + PROCESS_NAME;
		when(bean.getName()).thenReturn(processName);
		return bean;
	}
}
