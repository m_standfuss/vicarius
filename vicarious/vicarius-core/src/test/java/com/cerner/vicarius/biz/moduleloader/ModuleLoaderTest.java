package com.cerner.vicarius.biz.moduleloader;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.junit.Before;
import org.junit.Test;

import com.cerner.vicarius.Application;
import com.cerner.vicarius.data.access.ModuleDao;
import com.cerner.vicarius.data.model.Module;
import com.cerner.vicarius.dummymodules.DummyModule;
import com.cerner.vicarius.module.ModuleSpec;
import com.google.common.collect.Lists;

public class ModuleLoaderTest {
	
	private static final String PROPERTY_NAME = "PROPERTY_NAME";
	private static final String PROPERTY_VALUE = "PROPERTY_VALUE";
	
	private static final Long MODULE_ID_1 = 1L;
	private static final Long MODULE_ID_2 = 2L;
	
	private static final String MODULE_CLASSNAME_1 = "com.cerner.vicarius.dummymodules.DummyModule";
	private static final String MODULE_CLASSNAME_2 = "com.cerner.vicarius.dummymodules.DummyModuleStartupThrowsException";
	private static final String MODULE_CLASSNAME_3 = "com.cerner.vicarius.dummymodules.DummyModuleNoConstructor";
	private static final String MODULE_CLASSNAME_4 = "com.cerner.vicarius.dummymodules.DummyNotAModule";
	private static final String MODULE_CLASSNAME_5 = "com.cerner.vicarius.testmodule.ModuleTest";
	private static final String MODULE_CLASSNAME_6 = "com.cerner.vicarius.dummymodules.DummyModuleShutdownThrowsException";
	
	private static final String JAR_FILE_DOESNT_EXIST = "jarDoesntExist";
	private static final String JAR_FILE_EXTERNAL = "vicarius-moduletest.jar";
	
	@Before
	public void setup() {
		DummyModule.resetShutdownInvokeCount();
		DummyModule.resetStartupInvokeCount();
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void constructor_NullModuleDao_ExceptionThrown() {
		makeLoader(null);
	}
	
	@Test
	public void loadModules_ModuleDaoActiveQueryUsed() {
		ModuleDao moduleDao = makeModuleDao();
		
		ModuleLoader loader = makeLoader(moduleDao);
		loader.loadActiveModules();
		
		verify(moduleDao, times(1)).findActive();
	}
	
	@Test
	public void loadModules_OneActiveModule_Loaded() {
		Module module = makeModule(MODULE_ID_1, null, MODULE_CLASSNAME_1);
		ModuleDao moduleDao = makeModuleDao();
		when(moduleDao.findActive()).thenReturn(Lists.newArrayList(module));
		
		ModuleLoader loader = makeLoader(moduleDao);
		Map<Long, ModuleSpec> moduleMap = loader.loadActiveModules();
		
		assertEquals(1, moduleMap.entrySet().size());
		ModuleSpec spec = moduleMap.get(MODULE_ID_1);
		
		assertEquals(DummyModule.class, spec.getClass());
	}
	
	@Test(expected = IllegalStateException.class)
	public void loadModules_AlreadyLoadedModules_ExceptionThrown() {
		Module module = makeModule(MODULE_ID_1, null, MODULE_CLASSNAME_1);
		ModuleDao moduleDao = makeModuleDao();
		when(moduleDao.findActive()).thenReturn(Lists.newArrayList(module));
		
		ModuleLoader loader = makeLoader(moduleDao);
		
		loader.loadActiveModules();
		loader.loadActiveModules();
	}
	
	@Test
	public void loadModules_SingleModuleLoaded_HasStartupCalled() {
		Module module = makeModule(MODULE_ID_1, null, MODULE_CLASSNAME_1);
		ModuleDao moduleDao = makeModuleDao();
		when(moduleDao.findActive()).thenReturn(Lists.newArrayList(module));
		
		ModuleLoader loader = makeLoader(moduleDao);
		loader.loadActiveModules();
		
		assertEquals(1, DummyModule.getStartupInvokeCount());
	}
	
	@Test
	public void loadModules_SingleModuleLoaded_HasPropertySupplier() {
		Application.setProperties(makeProperties(DummyModule.class, PROPERTY_NAME, PROPERTY_VALUE));
		
		Module module = makeModule(MODULE_ID_1, null, MODULE_CLASSNAME_1);
		ModuleDao moduleDao = makeModuleDao();
		when(moduleDao.findActive()).thenReturn(Lists.newArrayList(module));
		
		ModuleLoader loader = makeLoader(moduleDao);
		Map<Long, ModuleSpec> moduleMap = loader.loadActiveModules();
		
		DummyModule moduleSpec = (DummyModule) moduleMap.get(MODULE_ID_1);
		assertEquals(PROPERTY_VALUE, moduleSpec.getPropertyWrap(PROPERTY_NAME));
	}
	
	@Test
	public void loadModules_ExceptionInStartup_NotLoaded() {
		Module module = makeModule(MODULE_ID_1, null, MODULE_CLASSNAME_2);
		ModuleDao moduleDao = makeModuleDao();
		when(moduleDao.findActive()).thenReturn(Lists.newArrayList(module));
		
		ModuleLoader loader = makeLoader(moduleDao);
		Map<Long, ModuleSpec> moduleMap = loader.loadActiveModules();
		assertFalse("The module was not expected to start up successfully.", moduleMap.containsKey(MODULE_ID_1));
	}
	
	@Test
	public void loadModules_ExternalJarFileDoesntExist_NotLoaded() {
		Module module = makeModule(MODULE_ID_1, JAR_FILE_DOESNT_EXIST, MODULE_CLASSNAME_2);
		ModuleDao moduleDao = makeModuleDao();
		when(moduleDao.findActive()).thenReturn(Lists.newArrayList(module));
		
		ModuleLoader loader = makeLoader(moduleDao);
		Map<Long, ModuleSpec> moduleMap = loader.loadActiveModules();
		assertFalse("The module was not expected to start up successfully.", moduleMap.containsKey(MODULE_ID_1));
	}
	
	@Test
	public void loadModules_NoParameterlessConstructor_NotLoaded() {
		Module module = makeModule(MODULE_ID_1, null, MODULE_CLASSNAME_3);
		ModuleDao moduleDao = makeModuleDao();
		when(moduleDao.findActive()).thenReturn(Lists.newArrayList(module));
		
		ModuleLoader loader = makeLoader(moduleDao);
		Map<Long, ModuleSpec> moduleMap = loader.loadActiveModules();
		assertFalse("The module was not expected to start up successfully.", moduleMap.containsKey(MODULE_ID_1));
	}
	
	@Test
	public void loadModules_ModuleDoesntExtendModuleSpec_NotLoaded() {
		Module module = makeModule(MODULE_ID_1, null, MODULE_CLASSNAME_4);
		ModuleDao moduleDao = makeModuleDao();
		when(moduleDao.findActive()).thenReturn(Lists.newArrayList(module));
		
		ModuleLoader loader = makeLoader(moduleDao);
		Map<Long, ModuleSpec> moduleMap = loader.loadActiveModules();
		assertFalse("The module was not expected to start up successfully.", moduleMap.containsKey(MODULE_ID_1));
	}
	
	@Test
	public void loadModules_ExternalModule_DifferentClassloaderUsed() {
		Module module = makeModule(MODULE_ID_1, JAR_FILE_EXTERNAL, MODULE_CLASSNAME_5);
		ModuleDao moduleDao = makeModuleDao();
		when(moduleDao.findActive()).thenReturn(Lists.newArrayList(module));
		
		ModuleLoader loader = makeLoader(moduleDao);
		loader.setModuleDirectory("target/modules");
		Map<Long, ModuleSpec> moduleMap = loader.loadActiveModules();
		
		ModuleSpec spec = moduleMap.get(MODULE_ID_1);
		assertNotEquals(this.getClass().getClassLoader(), spec.getClass().getClassLoader());
	}
	
	@Test
	public void loadModule_ModuleNotFoundInDb_NullReturned() {
		ModuleLoader loader = makeLoader();
		assertNull(loader.loadModule(MODULE_ID_1));
	}
	
	@Test(expected = IllegalStateException.class)
	public void loadModule_ModuleAlreadyLoaded_ExceptionThrown() {
		Module module = makeModule(MODULE_ID_1, null, MODULE_CLASSNAME_1);
		ModuleDao moduleDao = makeModuleDao();
		when(moduleDao.findActive()).thenReturn(Lists.newArrayList(module));
		
		ModuleLoader loader = makeLoader(moduleDao);
		loader.loadActiveModules();
		loader.loadModule(MODULE_ID_1);
	}
	
	@Test
	public void loadModule_ModuleFound_ModuleLoaded() {
		Module module = makeModule(MODULE_ID_1, null, MODULE_CLASSNAME_1);
		ModuleDao moduleDao = makeModuleDao();
		when(moduleDao.findById(MODULE_ID_1)).thenReturn(module);
		
		ModuleLoader loader = makeLoader(moduleDao);
		assertNotNull(loader.loadModule(MODULE_ID_1));
		assertEquals(1, DummyModule.getStartupInvokeCount());
	}
	
	@Test
	public void stopAllModules_shutdownCalled() {
		Module module = makeModule(MODULE_ID_1, null, MODULE_CLASSNAME_1);
		ModuleDao moduleDao = makeModuleDao();
		when(moduleDao.findActive()).thenReturn(Lists.newArrayList(module));
		
		ModuleLoader loader = makeLoader(moduleDao);
		loader.loadActiveModules();
		loader.stopAllModules();
		
		assertEquals(1, DummyModule.getShutdownInvokeCount());
	}
	
	@Test
	public void stopAllModules_MultipleModulesFirstThrowsException_ShutdownStillCalledOnRest() {
		List<Module> modules = Lists.newArrayList();
		modules.add(makeModule(MODULE_ID_1, null, MODULE_CLASSNAME_6));
		modules.add(makeModule(MODULE_ID_2, null, MODULE_CLASSNAME_1));
		ModuleDao moduleDao = makeModuleDao();
		when(moduleDao.findActive()).thenReturn(modules);
		
		ModuleLoader loader = makeLoader(moduleDao);
		loader.loadActiveModules();
		loader.stopAllModules();
		
		assertEquals(1, DummyModule.getShutdownInvokeCount());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void stopModule_ModuleNotStarted_ExceptionThrown() {
		ModuleLoader loader = makeLoader();
		loader.stopModule(MODULE_ID_1);
	}
	
	@Test
	public void stopModule_shutdownCalled() {
		Module module = makeModule(MODULE_ID_1, null, MODULE_CLASSNAME_1);
		ModuleDao moduleDao = makeModuleDao();
		when(moduleDao.findById(MODULE_ID_1)).thenReturn(module);
		
		ModuleLoader loader = makeLoader(moduleDao);
		loader.loadModule(MODULE_ID_1);
		loader.stopModule(MODULE_ID_1);
		
		assertEquals(1, DummyModule.getShutdownInvokeCount());
	}
	
	private Properties makeProperties(Class<?> moduleClass, String name, String value) {
		Properties props = new Properties();
		props.setProperty(moduleClass.getCanonicalName() + '.' + name, value);
		return props;
	}
	
	private ModuleLoader makeLoader() {
		return makeLoader(makeModuleDao());
	}
	
	private ModuleLoader makeLoader(ModuleDao moduleDao) {
		return new ModuleLoader(moduleDao);
	}
	
	private ModuleDao makeModuleDao() {
		ModuleDao moduleDao = mock(ModuleDao.class);
		return moduleDao;
	}
	
	private Module makeModule(Long moduleId, String jarFile, String className) {
		Module module = new Module();
		module.setClassName(className);
		module.setJarFile(jarFile);
		module.setModuleId(moduleId);
		return module;
	}
}
