package com.cerner.vicarius.biz.rulerunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Test;

import com.google.common.collect.Lists;

public class RuleRunnerFactoryTest {

	private static final Long RULE_ID_1 = 1L;

	@Test(expected = IllegalArgumentException.class)
	public void constructor_NullList_ExceptionThrown() {
		new RuleRunnerFactory(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructor_NullOnList_ExceptionThrown() {
		List<RuleRunner> runners = Lists.newArrayList((RuleRunner) null);
		new RuleRunnerFactory(runners);
	}

	@Test(expected = IllegalArgumentException.class)
	public void getRuleRunner_NullId_ExceptionThrown() {
		makeFactory().getRuleRunner(null);
	}

	@Test
	public void getRuleRunner_RunnerAlreadyExists_SameReturned() {
		RuleRunner runner = makeRunner(RULE_ID_1);
		List<RuleRunner> runners = Lists.newArrayList(runner);

		RuleRunnerFactory factory = new RuleRunnerFactory(runners);
		assertEquals(runner, factory.getRuleRunner(RULE_ID_1));
	}

	private RuleRunnerFactory makeFactory() {
		List<RuleRunner> runners = Lists.newArrayList();
		runners.add(makeRunner());
		return new RuleRunnerFactory(runners);
	}

	private RuleRunner makeRunner() {
		return makeRunner(RULE_ID_1);
	}

	private RuleRunner makeRunner(Long ruleId) {
		RuleRunner runner = mock(RuleRunner.class);
		when(runner.getRuleId()).thenReturn(ruleId);
		return runner;
	}
}
