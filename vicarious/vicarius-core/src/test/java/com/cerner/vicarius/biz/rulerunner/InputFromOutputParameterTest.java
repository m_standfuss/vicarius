package com.cerner.vicarius.biz.rulerunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.HashMap;

import org.junit.Test;

import com.google.gson.JsonObject;

public class InputFromOutputParameterTest {

	private static final Long RULE_JOB_ID = 123L;
	private static final String PARAMETER_NAME = "PARAMETER_NAME";

	private static final String OUTPUT_VALUE = "OUTPUT_VALUE";

	@Test(expected = IllegalArgumentException.class)
	public void constructor_NullJson_ExceptionThrown() {
		new InputFromOutputParameter(null, new HashMap<>());
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructor_NullMap_ExceptionThrown() {
		new InputFromOutputParameter(new JsonObject().toString(), null);
	}

	@Test(expected = IllegalStateException.class)
	public void constructor_MissingOutputProperty_ExceptionThrown() {
		new InputFromOutputParameter(new JsonObject().toString(), new HashMap<>());
	}

	@Test(expected = IllegalStateException.class)
	public void constructor_MissingRuleJobId_ExceptionThrown() {
		JsonObject obj = new JsonObject();
		obj.addProperty("parameterName", PARAMETER_NAME);

		new InputFromOutputParameter(obj.toString(), new HashMap<>());
	}

	@Test(expected = IllegalStateException.class)
	public void constructor_MissingParameterName_ExceptionThrown() {
		JsonObject obj = new JsonObject();
		obj.addProperty("ruleJobId", RULE_JOB_ID);

		new InputFromOutputParameter(obj.toString(), new HashMap<>());
	}

	@Test
	public void getParameterValue_MissingOutputParameter_NullReturned() {
		String parameterString = makeParameterString(RULE_JOB_ID, PARAMETER_NAME);
		InputFromOutputParameter param = new InputFromOutputParameter(parameterString, new HashMap<>());
		assertNull(param.getParameterValue());
	}

	@Test
	public void getParameterClass_MissingOutputParameter_NullReturned() {
		String parameterString = makeParameterString(RULE_JOB_ID, PARAMETER_NAME);
		InputFromOutputParameter param = new InputFromOutputParameter(parameterString, new HashMap<>());
		assertNull(param.getParameterClass());
	}

	@Test
	public void getParameterValue_OutputExists_OutputReturned() {
		RuleJobPropertyKey key = new RuleJobPropertyKey(RULE_JOB_ID, PARAMETER_NAME);
		HashMap<RuleJobPropertyKey, Object> outputMap = new HashMap<>();
		outputMap.put(key, OUTPUT_VALUE);

		String parameterString = makeParameterString(RULE_JOB_ID, PARAMETER_NAME);
		InputFromOutputParameter param = new InputFromOutputParameter(parameterString, outputMap);
		assertEquals(OUTPUT_VALUE, param.getParameterValue());
	}

	@Test
	public void getParameterClass_OutputExists_OutputReturned() {
		RuleJobPropertyKey key = new RuleJobPropertyKey(RULE_JOB_ID, PARAMETER_NAME);
		HashMap<RuleJobPropertyKey, Object> outputMap = new HashMap<>();
		outputMap.put(key, OUTPUT_VALUE);

		String parameterString = makeParameterString(RULE_JOB_ID, PARAMETER_NAME);
		InputFromOutputParameter param = new InputFromOutputParameter(parameterString, outputMap);
		assertEquals(String.class, param.getParameterClass());
	}

	private String makeParameterString(Long ruleJobId, String parameterName) {
		JsonObject obj = new JsonObject();
		obj.addProperty("ruleJobId", ruleJobId);
		obj.addProperty("parameterName", parameterName);
		return obj.toString();
	}
}
