package com.cerner.vicarius.dummymodules;

import com.cerner.vicarius.module.ActionJob;

public class DummyActionWithInputProperty extends ActionJob {

	private static Object input;

	public static Object getInput() {
		return input;
	}

	public static void clearInput() {
		input = null;
	}

	public DummyActionWithInputProperty() {
	}

	public void setInput(Object input) {
		DummyActionWithInputProperty.input = input;
	}

	@Override
	public void run() {

	}

}
