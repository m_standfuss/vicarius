package com.cerner.vicarius;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.cerner.vicarius.biz.rulerunner.RuleRunner;
import com.cerner.vicarius.biz.rulerunner.RuleRunnerFactory;
import com.cerner.vicarius.biz.rules.RuleDelegate;
import com.cerner.vicarius.biz.rules.RuleRecord;
import com.cerner.vicarius.data.model.Priority;
import com.google.common.collect.Lists;

public class TestScheduler {

	private static final Long RULE_ID_1 = 1L;
	private static final Long RULE_ID_2 = 2L;
	private static final Long RULE_ID_3 = 3L;
	private static final Long RULE_ID_4 = 4L;
	private static final Long RULE_ID_5 = 5L;

	@Test(expected = IllegalArgumentException.class)
	public void setLowPrioritySchedule_ZeroUsed_ExceptionThrown() {
		makeScheduler().setLowPrioritySchedule(0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void setLowPrioritySchedule_NegativeUsed_ExceptionThrown() {
		makeScheduler().setLowPrioritySchedule(-1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void setMediumPrioritySchedule_ZeroUsed_ExceptionThrown() {
		makeScheduler().setMediumPrioritySchedule(0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void setMediumPrioritySchedule_NegativeUsed_ExceptionThrown() {
		makeScheduler().setMediumPrioritySchedule(-1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void setHighPrioritySchedule_ZeroUsed_ExceptionThrown() {
		makeScheduler().setHighPrioritySchedule(0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void setHighPrioritySchedule_NegativeUsed_ExceptionThrown() {
		makeScheduler().setHighPrioritySchedule(-1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void setImmediatePrioritySchedule_ZeroUsed_ExceptionThrown() {
		makeScheduler().setImmediatePrioritySchedule(0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void setImmediatePrioritySchedule_NegativeUsed_ExceptionThrown() {
		makeScheduler().setImmediatePrioritySchedule(-1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void setUnknownPrioritySchedule_ZeroUsed_ExceptionThrown() {
		makeScheduler().setUnknownPrioritySchedule(0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void setUnknownPrioritySchedule_NegativeUsed_ExceptionThrown() {
		makeScheduler().setUnknownPrioritySchedule(-1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void setUpdateSchedule_ZeroUsed_ExceptionThrown() {
		makeScheduler().setUpdateSchedule(0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void setUpdateSchedule_NegativeUsed_ExceptionThrown() {
		makeScheduler().setUpdateSchedule(-1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void setRuleJobPoolSize_ZeroUsed_ExceptionThrown() {
		makeScheduler().setRuleJobPoolSize(0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void setRuleJobPoolSize_NegativeUsed_ExceptionThrown() {
		makeScheduler().setRuleJobPoolSize(-1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructor_NullFactory_ExceptionThrown() {
		makeScheduler(null, makeRuleDelegate());
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructor_NullRuleDao_ExceptionThrown() {
		makeScheduler(makeRuleRunnerFactory(), null);
	}

	@Test(expected = IllegalStateException.class)
	public void stop_AlreadyStopped_ExceptionThrown() {
		Scheduler scheduler = makeScheduler();
		scheduler.stop();
	}

	@Test(expected = IllegalStateException.class)
	public void start_AlreadyStarted_ExceptionThrown() {
		Scheduler scheduler = makeScheduler();
		scheduler.start();
		scheduler.start();
	}

	@Test
	public void start_OneRuleCanceled_RemovedFromExecutor() throws InterruptedException {
		RuleRecord rule = makeRuleRecord(RULE_ID_1);

		RuleDelegate ruleDelegate = makeRuleDelegate();
		when(ruleDelegate.getActiveRules()).thenReturn(Lists.newArrayList(rule)).thenReturn(new ArrayList<>());

		RuleRunner ruleRunner = makeRuleRunner();

		RuleRunnerFactory factory = makeRuleRunnerFactory();
		when(factory.getRuleRunner(RULE_ID_1)).thenReturn(ruleRunner);

		Scheduler scheduler = makeScheduler(factory, ruleDelegate);
		scheduler.setUpdateSchedule(100);
		scheduler.setImmediatePrioritySchedule(100);
		scheduler.start();

		Thread.sleep(199);

		scheduler.stop();

		verify(ruleRunner, times(1)).run();
	}

	@Test
	public void start_RulesPriorityChanged_ScheduleUpdated() throws InterruptedException {
		RuleRecord rule = makeRuleRecord(RULE_ID_1, Priority.IMMEDIATE);
		RuleRecord rule2 = makeRuleRecord(RULE_ID_1, Priority.HIGH);

		RuleDelegate ruleDelegate = makeRuleDelegate();
		when(ruleDelegate.getActiveRules()).thenReturn(Lists.newArrayList(rule)).thenReturn(Lists.newArrayList(rule2));

		RuleRunner ruleRunner = makeRuleRunner();

		RuleRunnerFactory factory = makeRuleRunnerFactory();
		when(factory.getRuleRunner(RULE_ID_1)).thenReturn(ruleRunner);

		Scheduler scheduler = makeScheduler(factory, ruleDelegate);
		scheduler.setUpdateSchedule(100);
		scheduler.setImmediatePrioritySchedule(100);
		scheduler.setHighPrioritySchedule(200);
		scheduler.start();

		Thread.sleep(300);

		scheduler.stop();

		verify(ruleRunner, atLeast(2)).run();
	}

	@Test
	public void start_MultipleRules_AllRanOnSchedule() throws InterruptedException {
		RuleRecord rule = makeRuleRecord(RULE_ID_1, Priority.IMMEDIATE);
		RuleRecord rule2 = makeRuleRecord(RULE_ID_2, Priority.HIGH);
		RuleRecord rule3 = makeRuleRecord(RULE_ID_3, Priority.MEDIUM);
		RuleRecord rule4 = makeRuleRecord(RULE_ID_4, Priority.LOW);
		RuleRecord rule5 = makeRuleRecord(RULE_ID_5, 50);

		RuleDelegate ruleDelegate = makeRuleDelegate();
		when(ruleDelegate.getActiveRules()).thenReturn(Lists.newArrayList(rule, rule2, rule3, rule4, rule5));

		RuleRunner ruleRunner1 = makeRuleRunner();
		RuleRunner ruleRunner2 = makeRuleRunner();
		RuleRunner ruleRunner3 = makeRuleRunner();
		RuleRunner ruleRunner4 = makeRuleRunner();
		RuleRunner ruleRunner5 = makeRuleRunner();

		RuleRunnerFactory factory = makeRuleRunnerFactory();
		when(factory.getRuleRunner(RULE_ID_1)).thenReturn(ruleRunner1);
		when(factory.getRuleRunner(RULE_ID_2)).thenReturn(ruleRunner2);
		when(factory.getRuleRunner(RULE_ID_3)).thenReturn(ruleRunner3);
		when(factory.getRuleRunner(RULE_ID_4)).thenReturn(ruleRunner4);
		when(factory.getRuleRunner(RULE_ID_5)).thenReturn(ruleRunner5);

		Scheduler scheduler = makeScheduler(factory, ruleDelegate);
		scheduler.setHighPrioritySchedule(20);
		scheduler.setImmediatePrioritySchedule(10);
		scheduler.setLowPrioritySchedule(40);
		scheduler.setMediumPrioritySchedule(30);
		scheduler.start();

		Thread.sleep(129);

		scheduler.stop();

		verify(ruleRunner1, atLeast(12)).run();
		verify(ruleRunner2, atLeast(6)).run();
		verify(ruleRunner3, atLeast(4)).run();
		verify(ruleRunner4, atLeast(3)).run();
		verify(ruleRunner5, atLeast(2)).run();
	}

	@Test
	public void start_RuleWithOffset_RanAtOffset() throws InterruptedException {
		RuleRecord rule = makeRuleRecord(RULE_ID_1, 100);

		RuleDelegate ruleDelegate = makeRuleDelegate();
		when(ruleDelegate.getActiveRules()).thenReturn(Lists.newArrayList(rule));

		RuleRunner ruleRunner = makeRuleRunner();

		RuleRunnerFactory factory = makeRuleRunnerFactory();
		when(factory.getRuleRunner(RULE_ID_1)).thenReturn(ruleRunner);

		Scheduler scheduler = makeScheduler(factory, ruleDelegate);
		scheduler.start();

		Thread.sleep(250);

		scheduler.stop();

		verify(ruleRunner, atLeast(2)).run();
	}

	@Test
	public void start_RuleWithUnknownPriority_RanAtUnknownOffset() throws InterruptedException {
		RuleRecord rule = makeRuleRecord(RULE_ID_1, (Priority) null);

		RuleDelegate ruleDelegate = makeRuleDelegate();
		when(ruleDelegate.getActiveRules()).thenReturn(Lists.newArrayList(rule));

		RuleRunner ruleRunner = makeRuleRunner();

		RuleRunnerFactory factory = makeRuleRunnerFactory();
		when(factory.getRuleRunner(RULE_ID_1)).thenReturn(ruleRunner);

		Scheduler scheduler = makeScheduler(factory, ruleDelegate);
		scheduler.setUnknownPrioritySchedule(300);
		scheduler.start();

		Thread.sleep(301);

		scheduler.stop();

		verify(ruleRunner, atLeast(1)).run();
	}

	private Scheduler makeScheduler() {
		return makeScheduler(makeRuleRunnerFactory(), makeRuleDelegate());
	}

	private Scheduler makeScheduler(RuleRunnerFactory factory, RuleDelegate ruleDelegate) {
		Scheduler scheduler = new Scheduler(factory, ruleDelegate);

		scheduler.setHighPrioritySchedule(20);
		scheduler.setImmediatePrioritySchedule(10);
		scheduler.setLowPrioritySchedule(40);
		scheduler.setMediumPrioritySchedule(30);
		scheduler.setUpdateSchedule(10);

		return scheduler;
	}

	private RuleDelegate makeRuleDelegate() {
		RuleDelegate ruleDelegate = mock(RuleDelegate.class);

		List<RuleRecord> rules = Lists.newArrayList();

		when(ruleDelegate.getActiveRules()).thenReturn(rules);
		return ruleDelegate;
	}

	private RuleRunnerFactory makeRuleRunnerFactory() {
		RuleRunnerFactory factory = mock(RuleRunnerFactory.class);

		RuleRunner runner = makeRuleRunner();

		when(factory.getRuleRunner(anyLong())).thenReturn(runner);
		return factory;
	}

	private RuleRunner makeRuleRunner() {
		RuleRunner runner = mock(RuleRunner.class);
		when(runner.run()).thenReturn(true);
		return runner;
	}

	private RuleRecord makeRuleRecord(Long ruleId) {
		return makeRuleRecord(ruleId, Priority.IMMEDIATE);
	}

	private RuleRecord makeRuleRecord(Long ruleId, Priority priority) {
		RuleRecord rule = new RuleRecord();
		rule.setRuleId(ruleId);
		rule.setPriority(priority);
		return rule;
	}

	private RuleRecord makeRuleRecord(Long ruleId, long offset) {
		RuleRecord rule = makeRuleRecord(ruleId, Priority.OFFSET);
		rule.setOffset(offset);
		return rule;
	}
}
