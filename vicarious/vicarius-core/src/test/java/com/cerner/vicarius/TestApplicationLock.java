package com.cerner.vicarius;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.cerner.vicarius.runtime.ProcessHandler;

public class TestApplicationLock {

	private static final int PROCESS_ID_ACTIVE = 1;
	private static final int PROCESS_ID_INACTIVE = 2;
	private static final int PROCESS_ID_CURRENT = 3;

	@Before
	public void setup() {
		ApplicationLock.setGracefulShutdownPeriod(1);
		ApplicationLock.LOCK_FILE.delete();
	}

	@After
	public void teardown() {
		ApplicationLock.setGracefulShutdownPeriod(ApplicationLock.DEFAULT_GRACEFUL_SHUTDOWN);
		ApplicationLock.LOCK_FILE.delete();
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructor_NullProcessHandler_ExceptionThrown() {
		new ApplicationLock(null);
	}

	@Test
	public void obtainLock_LockFileDoesntExist_LockFileCreated() throws IOException, InterruptedException {
		ApplicationLock lock = makeApplicationLock();
		lock.obtainLock();
		assertLockFileExists(PROCESS_ID_CURRENT);
	}

	@Test
	public void obtainLock_LockFileExistsActiveProcess_FalseReturned() throws IOException, InterruptedException {
		createLockFile(PROCESS_ID_ACTIVE);
		ApplicationLock lock = makeApplicationLock();
		assertFalse("The lock file should not be obtained successfully.", lock.obtainLock());
	}

	@Test
	public void obtainLock_LockFileExistNotActiveProcess_TrueReturned() throws IOException, InterruptedException {
		createLockFile(PROCESS_ID_INACTIVE);
		ApplicationLock lock = makeApplicationLock();
		assertTrue("The application should be locked.", lock.obtainLock());
		assertLockFileExists(PROCESS_ID_CURRENT);
	}

	@Test
	public void obtainLock_InterruptedException_FalseReturned() throws IOException, InterruptedException {
		createLockFile(PROCESS_ID_ACTIVE);
		ProcessHandler processHandler = makeProcessHandler();
		when(processHandler.isActiveProcess(eq(PROCESS_ID_ACTIVE))).thenThrow(new InterruptedException());

		ApplicationLock lock = makeApplicationLock(processHandler);
		assertFalse(lock.obtainLock());
	}

	@Test
	public void terminateLockedApplication_NoLockFile_NothingHappens() throws IOException, InterruptedException {
		ProcessHandler processHandler = makeProcessHandler();

		ApplicationLock lock = makeApplicationLock(processHandler);
		lock.terminateLockedApplication();

		verifyZeroInteractions(processHandler);
	}

	@Test
	public void terminateLockedApplication_ActiveProcessNotKilledGracefully_ProcessKilledManuallyFileDeleted()
			throws IOException, InterruptedException {
		createLockFile(PROCESS_ID_ACTIVE);
		ProcessHandler processHandler = makeProcessHandler();

		ApplicationLock lock = makeApplicationLock(processHandler);
		lock.terminateLockedApplication();

		verify(processHandler).killProcess(PROCESS_ID_ACTIVE);
		assertFalse("The lock file should be deleted.", ApplicationLock.LOCK_FILE.exists());
	}

	@Test
	public void terminateLockedApplication_ActiveProcessKilledGracefully_ProcessNotKilledFileDeleted()
			throws IOException, InterruptedException {
		ApplicationLock.setGracefulShutdownPeriod(100);

		createLockFile(PROCESS_ID_ACTIVE);
		ProcessHandler processHandler = makeProcessHandler();
		when(processHandler.isActiveProcess(PROCESS_ID_ACTIVE)).thenReturn(true, false);

		ApplicationLock lock = makeApplicationLock(processHandler);
		lock.terminateLockedApplication();

		verify(processHandler, times(0)).killProcess(PROCESS_ID_ACTIVE);
		assertFalse("The lock file should be deleted.", ApplicationLock.LOCK_FILE.exists());
	}

	@Test
	public void terminateLockedApplication_InactiveProcess_LockFileDeleted() throws IOException, InterruptedException {
		createLockFile(PROCESS_ID_INACTIVE);
		ProcessHandler processHandler = makeProcessHandler();

		ApplicationLock lock = makeApplicationLock(processHandler);
		lock.terminateLockedApplication();

		verify(processHandler, times(0)).killProcess(PROCESS_ID_INACTIVE);
		assertFalse("The lock file should be deleted.", ApplicationLock.LOCK_FILE.exists());
	}

	@Test
	public void releaseLock_FileExists_FileRemoved() throws IOException, InterruptedException {
		createLockFile(PROCESS_ID_CURRENT);
		ApplicationLock lock = makeApplicationLock();
		lock.releaseLock();
		assertFalse("The lock file should be deleted.", ApplicationLock.LOCK_FILE.exists());
	}

	@Test
	public void releaseLock_FileDoesntExists_FileDoesntExist() throws IOException, InterruptedException {
		ApplicationLock lock = makeApplicationLock();
		lock.releaseLock();
		assertFalse("The lock file should be deleted.", ApplicationLock.LOCK_FILE.exists());
	}

	@Test
	public void isLocked_MultipleRequests_ProcessIdCached() throws IOException, InterruptedException {
		createLockFile(PROCESS_ID_CURRENT);
		ProcessHandler processHandler = makeProcessHandler();

		ApplicationLock lock = makeApplicationLock(processHandler);
		lock.isLocked();
		lock.isLocked();

		verify(processHandler, times(1)).getCurrentProcessId();
	}

	@Test
	public void isLocked_LockFileDoenstExist_FalseReturned() throws IOException, InterruptedException {
		ApplicationLock lock = makeApplicationLock();
		assertFalse(lock.isLocked());
	}

	@Test
	public void isLocked_LockExistsDifferentProcessId_FalseReturned() throws IOException, InterruptedException {
		createLockFile(PROCESS_ID_ACTIVE);
		ApplicationLock lock = makeApplicationLock();
		assertFalse(lock.isLocked());
	}

	@Test
	public void isLocked_LockExistsSameProcessId_TrueReturned() throws IOException, InterruptedException {
		createLockFile(PROCESS_ID_CURRENT);
		ApplicationLock lock = makeApplicationLock();
		assertTrue(lock.isLocked());
	}

	@Test
	public void getGracefulShutdownPeriod_ValueSet_ValueReturned() {
		ApplicationLock.setGracefulShutdownPeriod(1);
		assertEquals(1, ApplicationLock.getGracefulShutdownPeriod());
	}

	private void assertLockFileExists(int processId) throws IOException {
		assertTrue("The application lock file does not exist.", ApplicationLock.LOCK_FILE.exists());
		String fileContents = FileUtils.readFileToString(ApplicationLock.LOCK_FILE, StandardCharsets.UTF_8);
		assertEquals(processId, Integer.parseInt(fileContents));
	}

	private void createLockFile(int processId) throws IOException {
		FileUtils.writeStringToFile(ApplicationLock.LOCK_FILE, String.valueOf(processId), StandardCharsets.UTF_8);
	}

	private ApplicationLock makeApplicationLock() throws IOException, InterruptedException {
		return makeApplicationLock(makeProcessHandler());
	}

	private ApplicationLock makeApplicationLock(ProcessHandler processHandler) {
		return new ApplicationLock(processHandler);
	}

	private ProcessHandler makeProcessHandler() throws IOException, InterruptedException {
		ProcessHandler processHandler = mock(ProcessHandler.class);

		when(processHandler.isActiveProcess(eq(PROCESS_ID_ACTIVE))).thenReturn(true);
		when(processHandler.isActiveProcess(eq(PROCESS_ID_CURRENT))).thenReturn(true);
		when(processHandler.isActiveProcess(eq(PROCESS_ID_INACTIVE))).thenReturn(false);

		doNothing().when(processHandler).killProcess(anyInt());

		when(processHandler.getCurrentProcessId()).thenReturn(PROCESS_ID_CURRENT);

		return processHandler;
	}
}
