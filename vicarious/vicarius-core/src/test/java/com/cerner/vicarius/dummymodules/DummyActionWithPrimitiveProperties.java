package com.cerner.vicarius.dummymodules;

import com.cerner.vicarius.module.ActionJob;

public class DummyActionWithPrimitiveProperties extends ActionJob {

	private static int invokeCount = 0;

	public static int getInvokeCount() {
		return invokeCount;
	}
	
	public static void resetInvokeCount() {
		invokeCount = 0;
	}

	private Boolean boolean_;
	private Integer integer_;
	private Long long_;
	private String string_;
	private Double double_;
	private Float float_;

	public void setBoolean(boolean value) {
		boolean_ = value;
	}
	
	public void setInteger(int value) {
		integer_ = value;
	}
	
	public void setLong(long value) {
		long_ = value;
	}
	
	public void setString(String value) {
		string_ = value;
	}
	
	public void setDouble(double value) {
		double_ = value;
	}
	
	public void setFloat(float value) {
		float_ = value;
	}
	
	@Override
	public void run() {
		invokeCount++;
	}
}
