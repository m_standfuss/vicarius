package com.cerner.vicarius.dummymodules;

import com.cerner.vicarius.module.ConditionalJob;

public class DummyConditionalCallsSaveState extends ConditionalJob {
	
	private static String saveState;

	public static void setSaveState(String saveState) {
		DummyConditionalCallsSaveState.saveState = saveState;
	}

	@Override
	public boolean run() {
		saveState(saveState);
		return true;
	}

}