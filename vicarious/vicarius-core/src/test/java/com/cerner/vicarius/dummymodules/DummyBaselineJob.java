package com.cerner.vicarius.dummymodules;

import com.cerner.vicarius.module.BaselineJob;
import com.cerner.vicarius.module.ConditionalJob;

public class DummyBaselineJob extends ConditionalJob implements BaselineJob {
	
	private static int baselineCount;
	private static int runCount;
	
	static {
		resetInvokeCounts();
	}
	
	public static int getBaselineInvokeCount() {
		return baselineCount;
	}
	
	public static int getRunInvokeCount() {
		return runCount;
	}
	
	public static void resetInvokeCounts() {
		baselineCount = 0;
		runCount = 0;
	}
	
	@Override
	public void runBaseline() {
		baselineCount++;
	}
	
	@Override
	public boolean run() {
		runCount++;
		return true;
	}
}
