package com.cerner.vicarius.runtime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.Lists;

public class TestCommandHandler {

	private static final String VALID_COMMAND_1 = "VALID_COMMAND_1";

	private static final String LINE_1 = "LINE_1";
	private static final String LINE_2 = "LINE_2";
	private static final String LINE_3 = "LINE_3";

	private InputStream emptyStream;

	@Before
	public void setUp() {
		List<String> emptyList = Lists.newArrayList();
		emptyStream = makeInputStream(emptyList);
	}

	@Test(expected = IllegalArgumentException.class)
	public void runCommand_NullCommand_ExceptionThrown() throws IOException, InterruptedException {
		CommandHandler test = new CommandHandler(makeRuntime(makeProcess(emptyStream, emptyStream)));
		test.runCommand(null);
	}

	@Test
	public void getErrorStrings_NoCommandRan_EmptyListReturned() throws IOException {
		CommandHandler test = new CommandHandler(makeRuntime(makeProcess(emptyStream, emptyStream)));

		assertTrue(test.getErrorStrings() != null);
		assertTrue(test.getErrorStrings().size() == 0);
	}

	@Test
	public void getOutputStrings_NoCommandRan_EmptyListReturned() throws IOException {
		CommandHandler test = new CommandHandler(makeRuntime(makeProcess(emptyStream, emptyStream)));

		assertTrue(test.getOutputStrings() != null);
		assertTrue(test.getOutputStrings().size() == 0);
	}

	@Test
	public void runCommand_CommandRanAgainstRuntime() throws IOException, InterruptedException {
		Runtime runtime = makeRuntime(makeProcess(emptyStream, emptyStream));

		CommandHandler test = new CommandHandler(runtime);
		test.runCommand(VALID_COMMAND_1);

		verify(runtime).exec(VALID_COMMAND_1);
	}

	@Test
	public void getOutputStrings_BufferWithThreeLine_ThreeLinesSet() throws IOException, InterruptedException {
		List<String> lines = Lists.newArrayList();
		lines.add(LINE_1);
		lines.add(LINE_2);
		lines.add(LINE_3);
		InputStream inputStream = makeInputStream(lines);
		Process p = makeProcess(inputStream, emptyStream);

		CommandHandler test = new CommandHandler(makeRuntime(p));
		test.runCommand(VALID_COMMAND_1);

		assertTrue(test.getOutputStrings().size() == 3);
		assertEquals(LINE_1, test.getOutputStrings().get(0));
		assertEquals(LINE_2, test.getOutputStrings().get(1));
		assertEquals(LINE_3, test.getOutputStrings().get(2));
	}

	@Test
	public void getErrorStrings_BufferWithThreeLine_ThreeLinesSet() throws IOException, InterruptedException {
		List<String> lines = Lists.newArrayList();
		lines.add(LINE_1);
		lines.add(LINE_2);
		lines.add(LINE_3);
		InputStream errorStream = makeInputStream(lines);
		Process p = makeProcess(emptyStream, errorStream);

		CommandHandler test = new CommandHandler(makeRuntime(p));
		test.runCommand(VALID_COMMAND_1);

		assertTrue(test.getErrorStrings().size() == 3);
		assertEquals(LINE_1, test.getErrorStrings().get(0));
		assertEquals(LINE_2, test.getErrorStrings().get(1));
		assertEquals(LINE_3, test.getErrorStrings().get(2));
	}

	private Runtime makeRuntime(Process process) throws IOException {
		Runtime runtime = mock(Runtime.class);
		when(runtime.exec(anyString())).thenReturn(process);
		return runtime;
	}

	private Process makeProcess(InputStream inputStream, InputStream errorStream) {
		Process p = mock(Process.class);
		when(p.getInputStream()).thenReturn(inputStream);
		when(p.getErrorStream()).thenReturn(errorStream);
		return p;
	}

	private InputStream makeInputStream(List<String> lines) {
		StringBuilder b = new StringBuilder();
		if (lines != null) {
			for (String line : lines) {
				b.append(line).append(System.getProperty("line.separator"));
			}
		}
		return IOUtils.toInputStream(b, StandardCharsets.UTF_8);
	}
}
