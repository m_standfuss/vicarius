Vicarius is an automation tool that can be used to trigger actions based on a set of conditions. The core frameworks is reusable, 
so anyone can write a "module" that can implement "conditions" or "actions" that can be ran in the framework. 

A combination of a set of conditions and actions is called a job. Users can create jobs to automatically identify situations when a condition
is met and therefore actions need to be taken. The framework allows for "parameters" to be passed into conditions or actions that can accept them.
These parameters can come from user defined details at creation of the job, or dynamically passed from another condition or action. The state of 
each condition can be saved upon running it, which allows for conditions such as "The number of FindBugs issues increased in the code base" to 
automatically update themselves each time they run, instead of manual intervention to update.

Some default modules have been implemented and pulled into the code base, along with a standard set of conditions and actions these include:

Email
JIRA
HP Fortify
Git

If you are interested in learning more about Vicarius, want to contribute to the codebase or want help implementing it for yourself feel free to reachout
to m_standfuss@hotmail.com. 